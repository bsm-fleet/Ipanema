import pycuda.cumath
from timeit import default_timer as timer
from tools import plt
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
from math import pi
import pycuda.gpuarray as gpuarray
from toygen import poissonLL_b as poissonLL
from iminuit import Minuit
from ModelBricks import Parameter, Free, Cat
import numpy as np
from PhisModel6 import mod, Badjanak as Model
from phisParams import CSP
from scipy import random as rnd

import cPickle
BLOCK_SIZE = 70 #(1 - 1024) For complex the max no. smaller

integraB = mod.get_function("binnedTimeIntegralB")
integraBbar = mod.get_function("binnedTimeIntegralBbar")
    
def manipulate_parameter(par, factor):
    rand1 = rnd.uniform()
    rand2 = rnd.uniform()
       
    if rand1 > 0.5:
        par = par + factor*rand2
    else:
        par = par - factor*rand2
            
    return par  

def getGrid(thiscat, BLOCK_SIZE):
    Nbunch = thiscat.Nevts *1. / BLOCK_SIZE
    if Nbunch > int(Nbunch): 
        Nbunch = int(Nbunch) +1
    else : 
        Nbunch = int(Nbunch)
    return  (Nbunch,1,1)
   
cats, Params = [], []
from math import pi
initial_fL = 0.5#0.5241
initial_fpe = 0.25#0.2504
initial_phis_0 = -0.03#0.8#
initial_phis_S = 0.#-0.03
initial_phis_pa = -0.03#0.8#
initial_phis_pe = -0.03#0.8#
initial_dpa = 1.57#3.2
initial_dpe = -1.57#-0.1

Params.append(Free("fL",initial_fL, limits=(0.48,0.52)))
Params.append(Free("fpe",initial_fpe, limits=(0.23,0.27)))
Params.append(Free("phis_0",initial_phis_0, limits=(-0.2,0.2)))#limits=(-pi,pi))#0.9 for phi only
Params.append(Parameter("phis_S",initial_phis_S, limits=(-0.2,0.2)))
Params.append(Free("phis_pa",initial_phis_pa, limits=(-0.2,0.2)))##limits=(-pi,pi)#0.8 for phi only
Params.append(Free("phis_pe",initial_phis_pe, limits=(-0.2,0.2)))## limits=(-pi,pi)#0.7 for phi only
Params.append(Free("dpa",initial_dpa, limits=(-pi,pi)))#1.57 for phi only
Params.append(Free("dpe",initial_dpe, limits=(-pi,pi)))#-1.57 for phi only
Params.append(Parameter("lambda_0_abs",1., limits=(0.8,1.2)))
Params.append(Parameter("lambda_S_abs",1., limits=(0.8,1.2)))
Params.append(Parameter("lambda_pa_abs",1., limits=(0.8,1.2)))
Params.append(Parameter("lambda_pe_abs",1., limits=(0.8,1.2)))

Params.append(Parameter("G",0.6603, limits=(0.6,0.7)))
Params.append(Parameter("DG",0.0805, limits=(0.06,0.10)))
Params.append(Parameter("Dm",17.7, limits=(16.,19.)))
  
#a = cPickle.load(file("./data/data_bbar_phi_0.5pi_res_tag_per_evt_3.ext","r"))
#a = np.float64(a)
#datahist = np.histogram(a[:,7], 100)
#plt.plot(datahist[1][:-1],datahist[0])
#plt.show()
#EXIT
        
for i in range(1,7):
#for i in range(3,4):
    ibin = str(i)
    Bname = "B_"+ ibin
    Bbarname = "Bbar_"+ ibin
    if i != 2: continue

    Params.append(Parameter("CSP_" + ibin ,CSP[i]))
    #Params.append(Free("Fs_" + ibin,0.5, limits=(0.0005,0.8)))
    #Params.append(Free("ds_" + ibin,0., limits=(-10,10.)))
    Params.append(Parameter("Fs_" + ibin,0, limits=(0.,.5)))
    Params.append(Parameter("ds_" + ibin,0., limits=(-10,10.)))

    ## Define category for i-th bin and flavour = B
    #thiscat = Cat(Bname, "./data/data_b_m0.03_0.5pi_0.5M_time_acc_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bname, "./data/data_b_m0.03_0.5pi_5M_time_acc_"+ibin +".ext", getN = True)
    thiscat = Cat(Bname, "/home3/veronika.chobanova/GPU/data/data_b_m0.03_0.5pi_time_res_RunII_"+ibin +".ext", getN = True)
    thiscat.weights = gpuarray.to_gpu(np.float64(thiscat.Nevts*[1.])) ## all weights to 1.
    thiscat.wNorm = np.float64(gpuarray.sum(thiscat.weights).get())
    
    thiscat.integra = integraB
    thiscat.bin = i
    thiscat.ibin = str(i)
    thiscat.block = (BLOCK_SIZE,1,1)
    thiscat.grid = getGrid(thiscat, BLOCK_SIZE)
    cats.append(thiscat)
    
    ## Define category for i-th bin and flavour = Bbar
    #thiscat = Cat(Bbarname, "./data/data_bbar_m0.03_0.5pi_0.5M_time_acc_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bbarname, "./data/data_bbar_m0.03_0.5pi_5M_time_acc_"+ibin +".ext", getN = True)
    thiscat = Cat(Bbarname, "/home3/veronika.chobanova/GPU/data/data_bbar_m0.03_0.5pi_time_res_RunII_"+ibin +".ext", getN = True)
    thiscat.weights = gpuarray.to_gpu(np.float64(thiscat.Nevts*[1.])) ## all weights to 1.
    thiscat.wNorm = np.float64(gpuarray.sum(thiscat.weights).get())
   
    thiscat.integra = integraBbar
    thiscat.bin = i
    thiscat.ibin = str(i)
    thiscat.block = (BLOCK_SIZE,1,1)
    thiscat.grid = getGrid(thiscat, BLOCK_SIZE)
    cats.append(thiscat) 
 
start = timer()
manager = Model(Params, cats, weighted = 0)
manager.createFit()
manager.fit.set_strategy(2)
manager.fit.migrad()
print timer() - start

while manager.fit.get_fmin()['edm'] > 1. or manager.fit.get_fmin()["is_valid"] == False:
    print "Edm still too high. Trying again."
    for i in range(len(Params)):
        if Params[i].name == "fL": Params[i].setVal(manipulate_parameter(initial_fL, 0.01))
        if Params[i].name == "fpe": Params[i].setVal(manipulate_parameter(initial_fpe, 0.01))
        if Params[i].name == "phis_0": Params[i].setVal(manipulate_parameter(initial_phis_0, 0.01))
        #if Params[i].name == "phis_S": Params[i].setVal(manipulate_parameter(initial_phis_S, 0.01))
        if Params[i].name == "phis_pa": Params[i].setVal(manipulate_parameter(initial_phis_pa, 0.01))
        if Params[i].name == "phis_pe": Params[i].setVal(manipulate_parameter(initial_phis_pe, 0.01))
        #if Params[i].name == "dpa": Params[i].setVal(manipulate_parameter(initial_dpa, 1.))
        #if Params[i].name == "dpe": Params[i].setVal(manipulate_parameter(initial_dpe, 1.))
    manager = Model(Params, cats)
    manager.createFit()
    manager.fit.set_strategy(2)
    manager.fit.migrad()
    
manager.fit.hesse()
#manager.fit.minos()
print manager.fit.get_fmin()['fval']
manager.plotcat(cats[6])
plt.show()
EXIT
manager.createMultinest("mnest_party", reset = False)
#manager.createMultinest("mnest_party")
values_dic = manager.mnest_vals()
values_list = len(manager.params)*[0.]
for i in xrange(len(manager.params)): 
    values_list[i] = values_dic[manager.params[i]]
manager(*values_list)
    
        
