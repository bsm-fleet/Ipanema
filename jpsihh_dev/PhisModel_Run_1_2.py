import numpy as np
#import pycuda.autoinit
import pycuda.cumath
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
import psIpatia as ipatia_module
import pycuda.gpuarray as gpuarray
from toygen import poissonLL_b as poissonLL
from iminuit import Minuit
from ModelBricks import ParamBox, cuRead, getGrid, Cat
from tools import getSumLL_w_large, getSumLL_w_short,getSumLL_large, getSumLL_short, plt, get_pulls, plot1D
from timeit import default_timer as timer
from poisson_intervals import *
from math import pi
from scipy import random as rnd
from RTuple import RTuple

from fitParameters_2011 import *
from fitParameters_2012 import *
from fitParameters_2015 import *
from fitParameters_2016 import *

mod = cuRead("cuda_b2cc_Run_1_2.c", no_extern_c = True)
#generator = mod.get_function("generate")
generator = mod.get_function("generate_perfect_tag")
t_ll =  np.float64(tcut)#np.float64(args[N["t_ll"]])

pdf = mod.get_function("DiffRate")

_timebins = np.arange(0.3,15,14.7/10000)
_timebins_gpu = gpuarray.to_gpu(_timebins)
_pdf_time = _timebins_gpu.copy()*0

THR = 1e06

def mySum1(thing): 
    return np.float64(sum(thing))

def mySum2(thing): 
    return np.float64(sum(thing).get())

class Badjanak(ParamBox):
    def __init__(self, pars, cats, weighted = 1, blind = 1, poldep = 0, cartesian = 0):
        ParamBox.__init__(self, pars, cats)
        sizes = []
        self.weighted = weighted
        
        for k in cats: 
            sizes.append(k.Nevts)
            
        if max(sizes) > THR:
            if weighted: 
                self.getSumLL = getSumLL_w_large
            else : 
                self.getSumLL = getSumLL_large
            self.mySum = mySum2
        else:
            if weighted: 
                self.getSumLL = getSumLL_w_short
            else: 
                self.getSumLL = getSumLL_short
            self.mySum = mySum1
            
        self.pdf = pdf
        self.time_bins = _timebins_gpu
        self.pdf_time = _pdf_time
        self.N_tbins = self.time_bins.size
        self.blind = blind
        self.setPolDep(poldep)
        self.cartesian = cartesian
        
    def setPolDep(self, poldep):
        print "Setting polarization dependence to " , poldep
        if "fit" in dir(self): print "Remember that minuit fit needs to be reinitialized to see the changes"
        self.poldep = poldep
        if not poldep:
            self.Params["delta_phis_S_0" ].setVal(0)
            self.Params["delta_phis_pa_0"].setVal(0)
            self.Params["delta_phis_pe_0"].setVal(0)
            self.Params["lambda_S_0_abs" ].setVal(1)
            self.Params["lambda_pa_0_abs"].setVal(1)
            self.Params["lambda_pe_0_abs"].setVal(1)

        #self.Params["delta_phis_S_0" ].constant = (not poldep)
        #self.Params["delta_phis_pa_0"].constant = (not poldep)
        #self.Params["delta_phis_pe_0"].constant = (not poldep)
        #self.Params["lambda_S_0_abs" ].constant = (not poldep)
        #self.Params["lambda_pa_0_abs"].constant = (not poldep)
        #self.Params["lambda_pe_0_abs"].constant = (not poldep)


    def run_cat(self, cat, CSP, Fs, fL, fpe, phis_0, phis_S, phis_pa, phis_pe, dS_m_dpe, dpa, dpe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, G, DG, Dm, p0_OS, dp0_OS, p1_OS, dp1_OS, p2_OS, dp2_OS, eta_bar_OS, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, eta_bar_SSK, sigma_t_a, sigma_t_b, sigma_t_c, t_ll, sigma_t_mu_a, sigma_t_mu_b, sigma_t_mu_c, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar, t_res_scale, t_mu):
        
        
        As = np.sqrt(Fs)
        Fp = 1.-Fs
        #As = Fs
        #Fp = 1. - As*As
        A0 = np.sqrt(Fp*fL)
        Ape = np.sqrt(Fp*fpe)
        Apa = np.sqrt(Fp*(1-fL-fpe))
        
        self.pdf(cat.data, cat.Probs, CSP, A0, As, Apa, Ape, phis_0, phis_S, phis_pa, phis_pe, dS_m_dpe, dpa, dpe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, G, DG, Dm, p0_OS, dp0_OS, p1_OS, dp1_OS, p2_OS, dp2_OS, eta_bar_OS, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, eta_bar_SSK, sigma_t_a, sigma_t_b, sigma_t_c, t_ll, sigma_t_mu_a, sigma_t_mu_b, sigma_t_mu_c, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar, t_res_scale, t_mu, np.int32(len(cat.knots)), cat.tristan, cat.knots, cat.spline_coeffs, cat.low_time_acc_bins_ul, cat.low_time_acc, cat.Nevts, block = cat.block, grid = cat.grid)
                
        if self.weighted: 
            return cat.wNorm  
        else: 
            return np.float64(cat.Nevts)
    
    def __call__(self,*args):
            
        chi2 = np.float64(0.)
        N = self.dc
            
        fL = np.float64(args[N["fL"]])
        fpe = np.float64(args[N["fpe"]])
        phis_0 = np.float64(args[N["phis_0"]] + self.blind*((self.poldep < .5)*self.params[N["phis_0"]].BlindOffset()
                                                            + (self.poldep > .5)*self.params[N["phis_0"]].blindpoldep))
        phis_S =  phis_0 + self.poldep*np.float64(args[N["delta_phis_S_0" ]] + self.blind*self.params[N["delta_phis_S_0" ]].BlindOffset())
        phis_pa = phis_0 + self.poldep*np.float64(args[N["delta_phis_pa_0"]] + self.blind*self.params[N["delta_phis_pa_0"]].BlindOffset())
        phis_pe = phis_0 + self.poldep*np.float64(args[N["delta_phis_pe_0"]] + self.blind*self.params[N["delta_phis_pe_0"]].BlindOffset())
        dpa = np.float64(args[N["dpa"]])
        dpe = np.float64(args[N["dpe"]]+pi)
        lambda_0_abs = np.float64(args[N["lambda_0_abs"]])
        lambda_S_abs = np.float64(args[N["lambda_S_0_abs"]]*args[N["lambda_0_abs"]]) 
        lambda_pa_abs = np.float64(args[N["lambda_pa_0_abs"]]*args[N["lambda_0_abs"]])
        lambda_pe_abs = np.float64(args[N["lambda_pe_0_abs"]]*args[N["lambda_0_abs"]])
        #G = np.float64(args[N["Gs_m_Gd"]]+0.65876)#OLD, tauBd = 1.518
        G = np.float64(args[N["Gs_m_Gd"]]+0.65789)
        #G = np.float64(args[N["Gs_m_Gd"]])
        DG = np.float64(args[N["DG"]] + self.blind*self.params[N["DG"]].BlindOffset())
        Dm = np.float64(args[N["Dm"]])
        
        p0_OS = np.float64(args[N["p0_OS"]])
        dp0_OS = np.float64(args[N["dp0_OS"]])
        p1_OS = np.float64(args[N["p1_OS"]])
        dp1_OS = np.float64(args[N["dp1_OS"]])
        p2_OS = np.float64(args[N["p2_OS"]])
        dp2_OS = np.float64(args[N["dp2_OS"]])
        eta_bar_OS = np.float64(args[N["eta_bar_OS"]])
        p0_SSK = np.float64(args[N["p0_SSK"]])
        dp0_SSK = np.float64(args[N["dp0_SSK"]])
        p1_SSK = np.float64(args[N["p1_SSK"]])
        dp1_SSK = np.float64(args[N["dp1_SSK"]])
        eta_bar_SSK = np.float64(args[N["eta_bar_SSK"]])
        
        p0_OS_RunI = np.float64(args[N["p0_OS_RunI"]])
        dp0_OS_RunI = np.float64(args[N["dp0_OS_RunI"]])
        p1_OS_RunI = np.float64(args[N["p1_OS_RunI"]])
        dp1_OS_RunI = np.float64(args[N["dp1_OS_RunI"]])
        eta_bar_OS_RunI = np.float64(args[N["eta_bar_OS_RunI"]])
        p0_SSK_RunI = np.float64(args[N["p0_SSK_RunI"]])
        dp0_SSK_RunI = np.float64(args[N["dp0_SSK_RunI"]])
        p1_SSK_RunI = np.float64(args[N["p1_SSK_RunI"]])
        dp1_SSK_RunI = np.float64(args[N["dp1_SSK_RunI"]])
        eta_bar_SSK_RunI = np.float64(args[N["eta_bar_SSK_RunI"]])
        
        sigma_t_a = np.float64(args[N["sigma_t_a"]])
        sigma_t_b = np.float64(args[N["sigma_t_b"]])
        sigma_t_c = np.float64(args[N["sigma_t_c"]])
        
        sigma_t_mu_a = np.float64(args[N["sigma_t_mu_a"]])
        sigma_t_mu_b = np.float64(args[N["sigma_t_mu_b"]])
        sigma_t_mu_c = np.float64(args[N["sigma_t_mu_c"]])
   
        norm = np.float64(0.)

        for cat in self.cats:
            ibin = cat.ibin
            if(self.cartesian == 0):
                Fs = np.float64(args[N["Fs_" + ibin]])
                dS_m_dpe = np.float64(args[N["ds_m_dpe_" + ibin]]+args[N["dpe"]])
            else:
                Fs = np.float64(args[N["xs_" + ibin]]**2 + args[N["ys_" + ibin]]**2)
                dS_m_dpe = np.float64(np.arctan2(args[N["ys_" + ibin]],args[N["xs_" + ibin]]) + args[N["dpe"]])
                
            #fL = np.float64(args[N["fL_" + ibin]])
            #fpe = np.float64(args[N["fpe_" + ibin]])
            #CSP = np.float64(args[N["CSP_" + ibin]])
            if("2011" not in cat.name and "2012" not in cat.name and "2015" not in cat.name and "2016" not in cat.name):
                print "ATTENTION: Invalid cat name"
              
            if("2011" in cat.name):
                f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar, t_res_scale, t_mu = np.float64(timeResFraction_2011), np.float64(sfBarOffset_2011), np.float64(sfSigmaOffset_2011), np.float64(sfBarSlope_2011), np.float64(sfSigmaSlope_2011), np.float64(sigmaBar_2011), np.float64(timeResolutionScale_2011), np.float64(timeResMu_2011)
            elif("2012" in cat.name):  
                f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar, t_res_scale, t_mu = np.float64(timeResFraction_2012), np.float64(sfBarOffset_2012), np.float64(sfSigmaOffset_2012), np.float64(sfBarSlope_2012), np.float64(sfSigmaSlope_2012), np.float64(sigmaBar_2012), np.float64(timeResolutionScale_2012), np.float64(timeResMu_2012)
            else:
                f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar, t_res_scale, t_mu = np.float64(0.), np.float64(0.), np.float64(0.), np.float64(0.), np.float64(0.), np.float64(0.), np.float64(0.), np.float64(0.)
            
            if("2011" in cat.name or "2012" in cat.name ):
                CSP = np.float64(CSP_factors_RunI[int(ibin)])
                #print ibin, CSP
                
                p2_OS, dp2_OS = np.float64(0.), np.float64(0.)
                
                norm += self.run_cat(cat, CSP, Fs, fL, fpe, phis_0, phis_S, phis_pa, phis_pe, dS_m_dpe, dpa, dpe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, G, DG, Dm, p0_OS_RunI, dp0_OS_RunI, p1_OS_RunI, dp1_OS_RunI, p2_OS, dp2_OS, eta_bar_OS_RunI, p0_SSK_RunI, dp0_SSK_RunI, p1_SSK_RunI, dp1_SSK_RunI, eta_bar_SSK_RunI, sigma_t_a, sigma_t_b, sigma_t_c, t_ll, sigma_t_mu_a, sigma_t_mu_b, sigma_t_mu_c, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar, t_res_scale, t_mu)
            else:
                CSP = np.float64(args[N["CSP_f0_" + ibin]] + (args[N["CSP_spl_" + ibin]]-args[N["CSP_f0_" + ibin]])*args[N["sigma_0"]])

                norm +=  self.run_cat(cat, CSP, Fs, fL, fpe, phis_0, phis_S, phis_pa, phis_pe, dS_m_dpe, dpa, dpe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, G, DG, Dm, p0_OS, dp0_OS, p1_OS, dp1_OS, p2_OS, dp2_OS, eta_bar_OS, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, eta_bar_SSK, sigma_t_a, sigma_t_b, sigma_t_c, t_ll, sigma_t_mu_a, sigma_t_mu_b, sigma_t_mu_c, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar, t_res_scale, t_mu)
            #print f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar, t_res_scale, t_mu
            
            #print cat.name, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar, t_mu
        LL = map(self.getSumLL, self.cats)
        LLsum = self.mySum(LL)
        out = -2*LLsum + 2*norm
        
        ################
        # CONSTRAINTS ##
        ################
        #Gaussian constraints on tagging parameters: Run II
        if(self.Params["p2_OS"].constant == 1):
            out += ((dp0_OS-dp0_OS_cv)/dp0_OS_err)**2
            out += ((dp1_OS-dp1_OS_cv)/dp1_OS_err)**2
            
            m_tagging_cv_OS = np.matrix(tagging_cv_OS)
            m_tagging_cv_OS = np.matrix([p0_OS, p1_OS]) - m_tagging_cv_OS
            
            m_tagging_cov_inv_OS = np.matrix(tagging_cov_inv_OS)
        else:
            print "Tagging systematics with p2"
            out += ((dp0_OS-dp0_OS_cv_syst)/dp0_OS_err_syst)**2
            out += ((dp1_OS-dp1_OS_cv_syst)/dp1_OS_err_syst)**2
            out += ((dp2_OS-dp2_OS_cv_syst)/dp2_OS_err_syst)**2
            
            m_tagging_cv_OS = np.matrix(tagging_cv_OS_syst)
            m_tagging_cv_OS = np.matrix([p0_OS, p1_OS, p2_OS]) - m_tagging_cv_OS
            
            m_tagging_cov_inv_OS = np.matrix(tagging_cov_inv_OS_syst)
        
        Y_OS = np.dot(m_tagging_cv_OS, m_tagging_cov_inv_OS)
        chi2_OS = np.dot(Y_OS, m_tagging_cv_OS.T)
        
        #chi2_OS = ((p0_OS-p0_OS_cv)/p0_OS_err)**2 + ((p1_OS-p1_OS_cv)/p1_OS_err)**2 #ATTENTION
        
        out += chi2_OS
        
        out += ((dp0_SSK-dp0_SSK_cv)/dp0_SSK_err)**2
        out += ((dp1_SSK-dp1_SSK_cv)/dp1_SSK_err)**2
        
        m_tagging_cv_SSK = np.matrix(tagging_cv_SSK)
        m_tagging_cv_SSK =  np.matrix([p0_SSK, p1_SSK]) - m_tagging_cv_SSK
        m_tagging_cov_inv_SSK = np.matrix(tagging_cov_inv_SSK)
        
        Y_SSK = np.dot(m_tagging_cv_SSK, m_tagging_cov_inv_SSK)
        chi2_SSK = np.dot(Y_SSK, m_tagging_cv_SSK.T)
                
        #chi2_SSK = ((p0_SSK-p0_SSK_cv)/p0_SSK_err)**2 + ((p1_SSK-p1_SSK_cv)/p1_SSK_err)**2 #ATTENTION

        out += chi2_SSK
        
        #print chi2_OS, chi2_SSK
        
        #Gaussian constraints on tagging parameters: Run I
        out += ((p0_OS_RunI-p0_OS_RunI_cv)/p0_OS_RunI_err)**2
        out += ((p1_OS_RunI-p1_OS_RunI_cv)/p1_OS_RunI_err)**2
        out += ((dp0_OS_RunI-dp0_OS_RunI_cv)/dp0_OS_RunI_err)**2
        out += ((dp1_OS_RunI-dp1_OS_RunI_cv)/dp1_OS_RunI_err)**2
        
        out += ((p0_SSK_RunI-p0_SSK_RunI_cv)/p0_SSK_RunI_err)**2
        out += ((p1_SSK_RunI-p1_SSK_RunI_cv)/p1_SSK_RunI_err)**2
        out += ((dp0_SSK_RunI-dp0_SSK_RunI_cv)/dp0_SSK_RunI_err)**2
        out += ((dp1_SSK_RunI-dp1_SSK_RunI_cv)/dp1_SSK_RunI_err)**2
        
        #Gaussian constraints to determine time resolution calibration systematics
        #if(self.Params["sigma_t_a"].constant == 1 and self.Params["sigma_t_b"].constant == 0 and self.Params["sigma_t_c"].constant == 0):
            #m_tr_cv = np.matrix(tr_cv_syst) 
            #m_tr_cv =  np.matrix([sigma_t_c, sigma_t_b]) - m_tr_cv
            #m_tr_cov_inv = np.matrix(tr_cov_inv_syst)
            
            #Y_tr = np.dot(m_tr_cv, m_tr_cov_inv)
            #chi2_tr = np.dot(Y_tr, m_tr_cv.T)
            
            #out += chi2_tr
        
        if math.isnan(out):
            return 10e12

        #print out
        return out
    
    def find_minimum(x, y):
        y_min = y[0]
        i_min = 0
        for i in range(1,len(y)):
            #print i, i_min
            #print y[i], y_min
            if y_min > y[i]:
                y_min = y[i]
                i_min = i
                
        return x[i_min], y[i_min], i_min

    def find_one_sigma_interval(x, y):
        x_start, y_start, i_start = find_minimum(x, y)
        dL_left = 0
        dL_right = 0
        i_right = i_start
        i_left = i_start
        
        #print x_start, y_start, i_start
            
        while dL_left < 1.:
            i_left -= 1
            if(i_left) <= 0:
                break
            dL_left = y[i_left] - y_start
        
        while dL_right < 1.:
            i_right += 1
            if(i_right) > len(y)-1:
                break
            dL_right = y[i_right] - y_start
            
        print x_start, "-", x_start-x[i_left], "+", x[i_right]-x_start
        print y_start , "-", dL_left, "+", dL_right 

    def corr_to_cov_matrix(corr_matrix, uncertainties):
        cov_matrix = [[0 for i in range(len(uncertainties))] for j in range(len(uncertainties))]
        for i in range(len(uncertainties)):
            for j in range(len(uncertainties)):
                cov_matrix[j][i] = corr_matrix[j][i]*uncertainties[i]*uncertainties[j]
        
        return cov_matrix

    def cov_to_corr_matrix(cov_matrix):
        size = len(cov_matrix[0])
        corr_matrix = [[0 for i in range(size)] for j in range(size)]
        uncertainties = []
        
        for i in range(size):
            sigma_i = np.sqrt(cov_matrix[i][i])
            uncertainties.append(sigma_i)
            for j in range(size):
                sigma_j = np.sqrt(cov_matrix[j][j])
                corr_matrix[j][i] = cov_matrix[j][i]/sigma_i/sigma_j
        print uncertainties
        return corr_matrix
    
    def ArgandPlot(self, thetaSP = [-0.0551307075480883, -0.31253947595101383, -1.1512187028439975, -2.1923049689849727, -2.7526032410206884, -2.9358782732618813], MKK = [990., 1008., 1016., 1020., 1024., 1032., 1050.] ):
        Ns = {}
        Ns_eff = {}
        for i in xrange(len(MKK)-1): 
            Ns[i] = 0.
            Ns_eff[i] = 0.
        for cat in self.cats:
            Ns[cat.bin-1] += cat.sumW_raw
            Ns_eff[cat.bin-1] += cat.wNorm

	#rho = {}                                                                                                                                                                                                  
	x, y = [], []      
        x_err, y_err = [], []                                                                                                                                                                                     
	x_p_d0, y_p_d0 = [], []
	x_p_d0_err, y_p_d0_err = [], []

	for i in xrange(6):
            rho = np.sqrt(Ns[i] / (MKK[i+1]-MKK[i]))  ## sqrt of events per unit of mkk     
            deltaS = np.arctan2(self.fit.values["ys_"+ str(i+1)], self.fit.values["xs_"+ str(i+1)]) - thetaSP[i]
            AmpS = np.sqrt(self.fit.values["xs_"+ str(i+1)]**2 + self.fit.values["ys_"+ str(i+1)]**2)                                                                                                                                        
            x.append(rho*self.fit.values["xs_"+ str(i+1)])
            y.append(rho*self.fit.values["ys_"+ str(i+1)])                                                                                                                                       
            x_p_d0.append(rho*AmpS*np.cos(deltaS))
            y_p_d0.append(rho*AmpS*np.sin(deltaS))
            #TODO
            x_err.append(rho*np.sqrt(self.fit.errors["xs_" + str(i+1)]**2 + self.fit.values["xs_" + str(i+1)]**2/(4.*Ns_eff[i])))                                                                                                                                                                                 
            y_err.append(rho*np.sqrt(self.fit.errors["ys_" + str(i+1)]**2 + self.fit.values["ys_" + str(i+1)]**2/(4.*Ns_eff[i]))) 
            x_p_d0_err.append(np.sqrt((np.cos(-thetaSP[i])*x_err[i])**2+(np.sin(-thetaSP[i])*y_err[i])**2))                                                                                                                                                                                 
            y_p_d0_err.append(np.sqrt((np.sin(-thetaSP[i])*x_err[i])**2+(np.cos(-thetaSP[i])*y_err[i])**2))  
                    
	return x, y, x_err, y_err, x_p_d0, y_p_d0, x_p_d0_err, y_p_d0_err

    def plotcat(self,cat, obs = 3, rebinning = 50, outfile='test_fit.pdf', yscale='log', interval = poisson_Linterval, plotf = plot1D):
        #rebin                                                                                                                                                                                                      
       data = cat.data.get()[:,obs]  ## only the decaytime
       sigmat = cat.data.get()[:,4]  ## only sigmat
       q_OS = cat.data.get()[:,5]
       q_SSK = cat.data.get()[:,6]
       omega_OS = cat.data.get()[:,7]
       omega_SSK = cat.data.get()[:,8]
       Nsigmat = np.int32(len(sigmat))
       sigmat_gpu = gpuarray.to_gpu(sigmat)
       q_OS_gpu = gpuarray.to_gpu(q_OS)
       q_SSK_gpu = gpuarray.to_gpu(q_SSK)
       omega_OS_gpu = gpuarray.to_gpu(omega_OS)
       omega_SSK_gpu = gpuarray.to_gpu(omega_SSK)
       bins = self.time_bins.get()
       if obs != 3: 
           bins = np.arange(min(data),max(data), (max(data)-min(data))*1./100)
       #print bins
       dt = bins[1]-bins[0]
       bins2 = bins -np.float64(len(bins)*[-dt*0.5])
       bins2 = list(bins2)
       bins2.append(bins[-1]+ dt)
       bins2 = np.float64(bins2)
       datahist = np.histogram(data, bins2)[0]
       #print bins2, datahist
       N = self.fit.values
       #print N
       fL = np.float64(N["fL"])
       fpe = np.float64(N["fpe"])
       phis_0 = np.float64(N["phis_0"])
       phis_S = np.float64([N["phis_0"]])
       #phis_S = np.float64(N["phis_S"])
       phis_pa = np.float64([N["phis_0"]])
       #phis_pa = np.float64(N["phis_pa"])
       phis_pe = np.float64([N["phis_0"]])
       #phis_pe = np.float64([N["phis_pe"]])
       dpa = np.float64(N["dpa"])
       dpe = np.float64(N["dpe"])
       lambda_0_abs = np.float64(N["lambda_0_abs"])
       lambda_S_abs = np.float64(N["lambda_S_abs"])
       lambda_pa_abs = np.float64(N["lambda_pa_abs"])
       lambda_pe_abs = np.float64(N["lambda_pe_abs"])

       G = np.float64(N["G"])
       DG = np.float64(N["DG"])
       Dm = np.float64(N["Dm"])
       
       ibin = cat.ibin
       Fs = np.float64(N["Fs_" + ibin])
       dS = np.float64(N["ds_m_dpe" + ibin] - N["dpe"])
       CSP = np.float64(N["CSP_" + ibin])
       As = np.sqrt(Fs)
       Fp = 1.-Fs
       A0 = np.sqrt(Fp*fL)
       Ape = np.sqrt(Fp*fpe)
       Apa = np.sqrt(Fp*(1-fL-fpe))
       self.pdf_time *= 0
       yblock = 1
       if obs == 3:
           #cat.integra(self.time_bins,self.pdf_time,sigmat_gpu, A0,As,Apa,Ape,G,DG,Dm,phis_0, phis_S, phis_pa, phis_pe, block = (10,yblock,1),grid = (self.N_tbins/10,int(len(sigmat)*1./yblock),1))
           cat.integra(self.time_bins, self.pdf_time, sigmat_gpu, q_OS_gpu, q_SSK_gpu, omega_OS_gpu, omega_SSK_gpu, A0, As, Apa, Ape, G, DG, Dm, phis_0, phis_S, phis_pa, phis_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, dS, dpa, dpe, CSP, Nsigmat, block = (1,1,1),grid = (self.N_tbins/1,1,1))#(10,1,1),grid = (self.N_tbins/10,1,1))

           pdf_ = self.pdf_time.get()
           integral = sum(pdf_)#*dt                                                                                                                                                                                    
           pdf = (len(data)*1./integral)*pdf_
       else: pdf = 0*datahist
       #print pdf
       fig , pltfit, pltpulls = plotf (datahist,pdf,bins,rebinning = rebinning, yscale = yscale, interval = interval)
       fig.tight_layout()
       fig.savefig(outfile)
       print Nsigmat
  
    def generate_cats(self, scale_factor = 2.):
        for key in self.Params.keys():
            print key, self.Params[key].val
        fL = np.float64(self.Params["fL"].val)
        fpe = np.float64(self.Params["fpe"].val)
        phis_0 = np.float64(self.Params["phis_0"].val + self.blind*((self.poldep < .5)*self.Params["phis_0"].BlindOffset()
                                                                    + (self.poldep > .5)*self.Params["phis_0"].blindpoldep))
        phis_S =  phis_0 + self.poldep*np.float64(self.Params["delta_phis_S_0" ].val + self.blind*self.Params["delta_phis_S_0" ].BlindOffset())
        phis_pa = phis_0 + self.poldep*np.float64(self.Params["delta_phis_pa_0"].val + self.blind*self.Params["delta_phis_pa_0"].BlindOffset())
        phis_pe = phis_0 + self.poldep*np.float64(self.Params["delta_phis_pe_0"].val + self.blind*self.Params["delta_phis_pe_0"].BlindOffset())
        
        dpa = np.float64(self.Params["dpa"].val)
        dpe = np.float64(self.Params["dpe"].val+pi)
        lambda_0_abs = np.float64(self.Params["lambda_0_abs"].val)
        lambda_S_abs = np.float64(self.Params["lambda_S_0_abs"].val*self.Params["lambda_0_abs"].val) 
        lambda_pa_abs = np.float64(self.Params["lambda_pa_0_abs"].val*self.Params["lambda_0_abs"].val)
        lambda_pe_abs = np.float64(self.Params["lambda_pe_0_abs"].val*self.Params["lambda_0_abs"].val)

        #G = np.float64(self.Params["Gs_m_Gd"].val+0.65876)#OLD
        G = np.float64(self.Params["Gs_m_Gd"].val+0.65789)
        DG = np.float64(self.Params["DG"].val + self.blind*self.Params["DG"].BlindOffset())
        Dm = np.float64(self.Params["Dm"].val)
        
        p0_OS = np.float64(self.Params["p0_OS"].val)
        dp0_OS = np.float64(self.Params["dp0_OS"].val)
        p1_OS = np.float64(self.Params["p1_OS"].val)
        dp1_OS = np.float64(self.Params["dp1_OS"].val)
        eta_bar_OS = np.float64(self.Params["eta_bar_OS"].val)
        p0_SSK = np.float64(self.Params["p0_SSK"].val)
        dp0_SSK = np.float64(self.Params["dp0_SSK"].val)
        p1_SSK = np.float64(self.Params["p1_SSK"].val)
        dp1_SSK = np.float64(self.Params["dp1_SSK"].val)
        eta_bar_SSK = np.float64(self.Params["eta_bar_SSK"].val)
        
        sigma_t_a = np.float64(self.Params["sigma_t_a"].val)
        sigma_t_b = np.float64(self.Params["sigma_t_b"].val)
        sigma_t_c = np.float64(self.Params["sigma_t_c"].val)
                       
        #t_ll = np.float64(self.Params["t_ll"].val)
        newcats = []
        for cat in self.cats:
            ibin = cat.ibin
            Probmax = scale_factor*np.float64(gpuarray.max(cat.Probs).get())
            if not Probmax:
                print "PROBMAX is 0, what you are doing is dangerous and I don't want to get involved in it"
                print "exiting..."
                BREAK
            Fs = np.float64(self.Params["Fs_" + ibin].val)
            dS_m_dpe = np.float64(self.Params["ds_m_dpe_" + ibin].val + self.Params["dpe"].val)
            CSP = np.float64(self.Params["CSP_f0_" + ibin].val + (self.Params["CSP_spl_" + ibin].val
                                                                  - self.Params["CSP_f0_" + ibin].val)*self.Params["sigma_0"].val)
            
            As = np.sqrt(Fs)
            Fp = 1.-Fs
       
            A0 = np.sqrt(Fp*fL)
            Ape = np.sqrt(Fp*fpe)
            Apa = np.sqrt(Fp*(1-fL-fpe))
            #Nevts = np.int32(rnd.poisson(cat.wNorm))
            Nevts = np.int32((cat.wNorm))#ATTENTION this is just for the plot

            out = gpuarray.to_gpu(np.float64(Nevts*[13*[0.]]))
            
            mygrid = getGrid(Nevts, cat.block[0])
            
            #normweights_2015_unbiased = [1.0, 1.0446, 1.0453, -0.0011, -0.00142, 0.001, 1.0139, -0.0002, -0.0003, -0.0047]
            #normweights_2015_biased = [1.0, 1.0462, 1.0469, -0.0102, 0.004, 0.0046, 1.0251, -0.0024, -0.0004, -0.0402]
            #normweights_2016_unbiased = [1.0, 1.04126, 1.04024, -0.0001, 0.00017, 4e-05, 1.01071, 0.00013, -0.0005, -0.0038]
            #normweights_2016_biased = [1.0, 1.0327, 1.0346, 0.0014, 0.00268, -0.00076, 1.021, 0.0002, 0.0, 0.0022]

            #Katya's e-mail from 08/03/19
            moments_2015_unbiased = [1.0288667, 0.030559596, 0.0010327956, 0.0016105348, 0.00080526739, 0.0029692872, -0.015155445, 0.003812355, -0.0038433498, -0.038]
            moments_2015_biased =   [1.0255333, 0.0094660211, -0.00232379, 0.0059052942, -0.0010736899, 0.013684541, -0.030397492, -0.01146806, -0.007128794, -0.05325]
            moments_2016_unbiased = [1.0249967, 0.033444123, -0.00029692872, -9.3947863e-05, 0.00012079011, 0.0010327956, -0.0033601786, -0.0007128794, -0.00074387415, -0.0251]  
            moments_2016_biased = [1.0218, 0.0062609903, 0., -0.00016105348, 0.00013421123, -0.0037438839, 0.0046765372, -0.0095773797, 0.00074387415, -0.0475]
            
            if("2015" in cat.name and "unbiased" in cat.name):
                #normweights = normweights_2015_unbiased
                normweights = normweights_unbiased_2015
                moments = moments_2015_unbiased
                year = np.int32(2015)
                hltB = np.int32(0)
            elif("2015" in cat.name and "biased" in cat.name):
                #normweights = normweights_2015_biased
                normweights = normweights_biased_2015
                moments = moments_2015_biased
                year = np.int32(2015)
                hltB = np.int32(1)
            elif("2016" in cat.name and "unbiased" in cat.name):
                #normweights = normweights_2016_unbiased
                normweights = normweights_unbiased_2016
                moments = moments_2016_unbiased
                year = np.int32(2016)
                hltB = np.int32(0)
            else:
                #normweights = normweights_2016_biased
                normweights = normweights_biased_2016
                moments = moments_2016_biased
                year = np.int32(2016)
                hltB = np.int32(1)
            print hltB, year, Nevts

            generator(out, CSP, A0, As, Apa, Ape, phis_0, phis_S, phis_pa, phis_pe, dS_m_dpe, dpa, dpe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, G, DG, Dm, p0_OS, dp0_OS, p1_OS, dp1_OS, eta_bar_OS, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, eta_bar_SSK, sigma_t_a, sigma_t_b, sigma_t_c,t_ll,np.int32(len(cat.knots)), gpuarray.to_gpu(np.float64(normweights)), gpuarray.to_gpu(np.float64(moments)), cat.knots, cat.spline_coeffs, year, hltB, Probmax, Nevts, block = cat.block, grid = mygrid)
            
            toycat = Cat(cat.name, out, N = Nevts)
            out = []

            toycat.ibin = cat.ibin
            toycat.weights = gpuarray.to_gpu(np.float64(Nevts*[1.]))
            toycat.wNorm = Nevts
            toycat.knots = cat.knots.copy()
            toycat.spline_coeffs  = cat.spline_coeffs.copy()
            toycat.tristan = gpuarray.to_gpu(np.float64(normweights))
            #toycat.tristan = gpuarray.to_gpu(np.float64([1, 1, 1, 0, 0, 0, 1, 0, 0, 0]))
            toycat.block = cat.block
            toycat.grid = mygrid
            
            newcats.append(toycat)
       
        #labels = ["sigma_t_gen/F"] 
        #tup = RTuple("data_gen" + str(rnd.random()), labels)    
        #for c in newcats:
            #sigmat = c.data.get()[:,4]
            #for s in sigmat:
                #tup.fillItem("sigma_t_gen", s)
                #tup.fill()
                
        #tup.close()

        return newcats

