#include <stdio.h>
//#include <math.h>
// #include <thrust/complex.h>
#include <pycuda-complex.hpp>

#define errf_const 1.12837916709551
#define xLim 5.33
#define yLim 4.29 

#define eta_bar_OS 0.384
#define eta_bar_SSK 0.42
#define p0_OS 0.389
#define p0_SSK 0.4314
#define dp0_hf_OS 0.00435
#define dp0_hf_SSK 0.
#define p1_OS 0.9093
#define p1_SSK 0.8874
#define dp1_hf_OS -0.00315
#define dp1_hf_SSK 0.
#define sigma_t_a_RunII -6.
#define sigma_t_b_RunII 1.539
#define sigma_t_c_RunII -0.004133
#define f_sigma_t 0.244
#define r_offset_pr 1.4207
#define r_offset_sc 0.3778
#define r_slope_pr -2.85
#define r_slope_sc -1.55
#define sigma_t_bar 0.0350 
#define t_ll 0.30//ATTENTION
#define t_ul 15.//ATTENTION

#define year_const 2016//ATTENTION
#define sigma_t_threshold 0.//ATTENTION
#define time_acc_bins 40
#define spline_num_bins 6//ATTENTION
#define spline_num_bins_2015 5//ATTENTION

__device__ double spline_t[spline_num_bins+1] = {0.20, 0.22, 0.25, 0.50, 1.00, 8.00, t_ul};//ATTENTION
__device__ double spline_t_2015[spline_num_bins_2015+1] = {0.3, 0.5, 1.0, 4.0, 8.0, t_ul};//ATTENTION

__device__ double spline_c[spline_num_bins+2] = {0.362400, 0.595944, 0.782916, 0.934225, 0.986995, 0.993854, 1.02765, 1.0};
__device__ double spline_c_2015[spline_num_bins_2015+2] = {0.972050, 0.980177, 1.03515, 1.03868, 1.07465, 1.00229, 1.0};

//Calculated with the K*K* code
// __device__ double spline_pol_c[spline_num_bins][4] = {{-187.14506667, 2504.41600016, -11156.25800073, 16609.32333332},{-32.70014145, 398.34883791, -1583.22544505, 2104.72855311}, {0.0943265, 4.81522, -9.09099, 5.88261}, {0.807275, 0.53753, -0.535599, 0.179016}, {0.986533, -0.000242206, 0.00217308, -0.000241548}, {1.094801032, -0.011850142, 0, 0}}; 

//Calculated with general_spline.py
__device__ double spline_pol_c[spline_num_bins][4] = {{- 187.145066666667, 2504.41600000001, - 11156.258, 16609.3233333334},
 {-32.7001414468865, 398.348837912088, -1583.22544505495, 2104.72855311355}, {0.0943264670920469, 4.81522294434598, -9.09098518397732, 5.88260661892946}, {0.807275319790461, 0.537529828155501, -0.53559895159636, 0.179015797342156}, {0.986532664636091, -0.000242206381391268, 0.00217308294053105, -0.000241547503474508}, {1.0948, -0.0118499999999999, 0, 0}};//ATTENTION

__device__ double spline_pol_c_2015[spline_num_bins_2015][4] = {{1.02718328702427, -0.624602513099816, 1.91989051985657, -1.50158073634859}, {0.811504554881413, 0.669469879757309, -0.668254265857693, 0.223849120794264}, {1.03644020113992, -0.00533705901820179, 0.00655267291781575, -0.00108652546423975}, {0.893253959183673, 0.10205262244898, -0.0202947474489796, 0.00115075956632654}, {1.01374, -0.00171749999999987, 0., 0.}};//ATTENTION

// __device__ double spline_pol_c[spline_num_bins][4] = {{0.0943265, 4.81522, -9.09099, 5.88261}, {0.807275, 0.53753, -0.535599, 0.179016}, {0.986533, -0.000242206, 0.00217308, -0.000241548}, {1.094801032, -0.011850142, 0, 0}};//ATTENTION

//For t>0.2ps
// __device__ double normweights[10] = {1., 1.0322, 1.0311, -0.00032, 0.00338, -0.0009, 1.01090, 0.00058, 0.00034, -0.0010};//ATTENTION
// __device__ double normweights[10] = {1., 1., 1., 0., 0., 0., 1., 0., 0., 0.};

//For t>0.25ps
// __device__ double normweights[10] = {1.0, 1.031813, 1.03065, -0.00037, 0.00346, -0.00085, 1.01069, 0.00054, 0.00022, -0.0010};

//For t>0.30ps
__device__ double normweights[10] = {1.0, 1.03140, 1.03023, -0.00025, 0.00343, -0.00078, 1.01049, 0.00053, 0.00014, -0.0006};


__device__ double low_time_acc_bins_ll[time_acc_bins] = {0.2, 0.23797471677166174, 0.27693584616386924, 0.31693600305260117, 0.35803212761855635, 0.40028597282970174, 0.4437646625969446, 0.48854133326607896, 0.5346958738379816, 0.5823157837313284, 0.6314971712226232, 0.6823459211913355, 0.7349790678339851, 0.7895264170955251, 0.8461324753957553, 0.9049587567724324, 0.966186561187383, 1.0300203443952365, 1.0966918372716514, 1.1664651239654924, 1.2396429598202376, 1.3165747110027106, 1.3976664425174057, 1.483393892317744, 1.5743193827952986, 1.6711141967271264, 1.7745886837810803, 1.8857335419899877, 2.0057776537213834, 2.1362711425364616, 2.279208120728858, 2.43721431642301, 2.613845648787718, 2.814087169543123, 3.045239101577547, 3.3186177201267117, 3.653177485580561, 4.084441360908032, 4.692120690735037, 5.730287538377153};

__device__ double low_time_acc_bins_ul[time_acc_bins] = 
{0.23797471677166174, 0.27693584616386924, 0.31693600305260117, 0.35803212761855635, 0.40028597282970174, 0.4437646625969446, 0.48854133326607896, 0.5346958738379816, 0.5823157837313284, 0.6314971712226232, 0.6823459211913355, 0.7349790678339851, 0.7895264170955251, 0.8461324753957553, 0.9049587567724324, 0.966186561187383, 1.0300203443952365, 1.0966918372716514, 1.1664651239654924, 1.2396429598202376, 1.3165747110027106, 1.3976664425174057, 1.483393892317744, 1.5743193827952986, 1.6711141967271264, 1.7745886837810803, 1.8857335419899877, 2.0057776537213834, 2.1362711425364616, 2.279208120728858, 2.43721431642301, 2.613845648787718, 2.814087169543123, 3.045239101577547, 3.3186177201267117, 3.653177485580561, 4.084441360908032, 4.692120690735037, 5.730287538377153, 15.0};

__device__ double low_time_acc_all[time_acc_bins] = {0.2634384036064148, 0.3077799677848816, 0.3243389427661896, 0.3367297947406769, 0.3454984724521637, 0.351309597492218, 0.3549683690071106, 0.35744380950927734, 0.35949477553367615, 0.36121028661727905, 0.3626016676425934, 0.36368557810783386, 0.3644851744174957, 0.3650318384170532, 0.36536726355552673, 0.36554619669914246, 0.365639865398407, 0.365723580121994, 0.36581581830978394, 0.36591747403144836, 0.3660294711589813, 0.3661530315876007, 0.3662893772125244, 0.3664401173591614, 0.36660704016685486, 0.3667922616004944, 0.3669983744621277, 0.3672284483909607, 0.36748626828193665, 0.3677765130996704, 0.36810508370399475, 0.36847949028015137, 0.3689095377922058, 0.3694082498550415, 0.36999329924583435, 0.37068918347358704, 0.37152960896492004, 0.3725546896457672, 0.373737096786499, 0.3596302568912506};


__device__ double S_matrix_coeff[4][4] = {{1., 0.5, 0.25, 0.125}, {0.5, 0.5, 0.375, 0}, {0.25, 0.375, 0., 0.}, {0.125, 0., 0., 0.}};

__device__ double get_ta (double t,double G,double DG) { return exp(-G*t)*cosh(DG*t/2); }
__device__ double get_tb (double t,double G,double DG) { return exp(-G*t)*sinh(DG*t/2); }
__device__ double get_tc (double t,double G,double DM) { return exp(-G*t)*cos(DM*t); }
__device__ double get_td (double t,double G,double DM) { return exp(-G*t)*sin(DM*t); }

__device__ double get_int_ta(double t_0, double t_1, double G,double DG) 
{ 
    return (2*(DG*sinh(.5*DG*t_0) + 2*G*cosh(.5*DG*t_0))*exp(G*t_1) - 2*(DG*sinh(.5*DG*t_1) + 2*G*cosh(.5*DG*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(-pow(DG, 2) + 4 *pow(G, 2));
}
__device__ double get_int_tb(double t_0, double t_1,double G,double DG) 
{
    return (2*(DG*cosh(.5*DG*t_0) + 2*G*sinh(.5*DG*t_0))*exp(G*t_1) - 2*(DG*cosh(.5*DG*t_1) + 2*G*sinh(.5*DG*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(-pow(DG, 2) + 4*pow(G, 2));
}
__device__ double get_int_tc(double t_0, double t_1,double G,double DM)
{
    return ((-DM*sin(DM*t_0) + G*cos(DM*t_0))*exp(G*t_1) + (DM*sin(DM*t_1) - G*cos(DM*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(pow(DM, 2) + pow(G, 2));
}

__device__ double get_int_td(double t_0, double t_1,double G,double DM)
{
    return ((DM*cos(DM*t_0) + G*sin(DM*t_0))*exp(G*t_1) - (DM*cos(DM*t_1) + G*sin(DM*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(pow(DM, 2) + pow(G, 2));
}

__device__ double get_int_ta_spline(double delta_t,double G,double DM,double DG,double a,double b,double c,double d,double t_0,double t_1)
{
    double G_sq = G*G;
    double G_cub = G_sq*G;
    double DG_sq = DG*DG;
    double DG_cub = DG_sq*DG;
    double delta_t_sq = delta_t*delta_t;
    double t_0_sq = t_0*t_0;
    double t_0_cub = t_0_sq*t_0;
    double t_1_sq = t_1*t_1;
    double t_1_cub = t_1_sq*t_1;
    
    return -0.5*sqrt(2.)*sqrt(delta_t)
    *(-2*a*pow(DG - 2*G, 4.)*pow(DG + 2*G, 3.)*(-exp(-0.5*t_1*(DG + 2*G)) + exp(-0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq) 
     + 2*a*pow(DG - 2*G, 3.)*pow(DG + 2*G, 4.)*(exp(0.5*t_0*(DG - 2*G)) - exp(0.5*t_1*(DG - 2*G))) 
     + b*pow(DG - 2*G, 4.)*pow(DG + 2*G, 2.)*(-2*(DG*t_0 + 2*G*t_0 + 2.)*exp(0.5*t_1*(DG + 2*G)) + 2*(DG*t_1 + 2*G*t_1 + 2.)*exp(0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq - 0.5*(DG + 2*G)*(t_0 + t_1)) 
     - 2*b*pow(DG - 2*G, 2.)*pow(DG + 2*G, 4.)*((-DG*t_0 + 2*G*t_0 + 2.)*exp(0.5*DG*t_0 + G*t_1) + (DG*t_1 - 2*G*t_1 - 2.)*exp(0.5*DG*t_1 + G*t_0))*exp(-G*(t_0 + t_1)) 
     + 2*c*pow(DG - 2*G, 4.)*(DG + 2*G)*(-(DG_sq*t_0_sq + 4*DG*t_0*(G*t_0 + 1) + 4*G_sq*t_0_sq + 8*G*t_0 + 8)*exp(0.5*t_1*(DG + 2*G)) + (DG_sq*t_1_sq + 4*DG*t_1*(G*t_1 + 1) + 4*G_sq*t_1_sq + 8*G*t_1 + 8)*exp(0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq - 0.5*(DG + 2*G)*(t_0 + t_1)) 
     - 2*c*(DG - 2*G)*pow(DG + 2*G, 4.)*(-(DG_sq*t_0_sq - 4*DG*t_0*(G*t_0 + 1) + 4*G_sq*t_0_sq + 8*G*t_0 + 8)*exp(0.5*DG*t_0 + G*t_1) + (DG_sq*t_1_sq - 4*DG*t_1*(G*t_1 + 1) + 4*G_sq*t_1_sq + 8*G*t_1 + 8)*exp(0.5*DG*t_1 + G*t_0))*exp(-G*(t_0 + t_1)) 
     + 2*d*pow(DG - 2*G, 4.)*((-DG_cub*t_0_cub - 6*DG_sq*t_0_sq*(G*t_0 + 1) - 12*DG*t_0*(G_sq*t_0_sq + 2*G*t_0 + 2.) - 8*G_cub*t_0_cub - 24*G_sq*t_0_sq - 48*G*t_0 + 48*exp(0.5*t_0*(DG + 2*G)) - 48)*exp(-0.5*t_0*(DG + 2*G)) + (DG_cub*t_1_cub + 6*DG_sq*t_1_sq*(G*t_1 + 1) + 12*DG*t_1*(G_sq*t_1_sq + 2*G*t_1 + 2.) + 8*G_cub*t_1_cub + 24*G_sq*t_1_sq + 48*G*t_1 - 48*exp(0.5*t_1*(DG + 2*G)) + 48)*exp(-0.5*t_1*(DG + 2*G)))*exp(DG*G*delta_t_sq) 
     + 2*d*pow(DG + 2*G, 4.)*(((DG_cub*t_0_cub - 6*DG_sq*t_0_sq*(G*t_0 + 1) + 12*DG*t_0*(G_sq*t_0_sq + 2*G*t_0 + 2.) - 8*G_cub*t_0_cub - 24*G_sq*t_0_sq - 48*G*t_0 - 48)*exp(0.5*DG*t_0) + 48*exp(G*t_0))*exp(-G*t_0) - ((DG_cub*t_1_cub - 6*DG_sq*t_1_sq*(G*t_1 + 1) + 12*DG*t_1*(G_sq*t_1_sq + 2*G*t_1 + 2.) - 8*G_cub*t_1_cub - 24*G_sq*t_1_sq - 48*G*t_1 - 48)*exp(0.5*DG*t_1) + 48*exp(G*t_1))*exp(-G*t_1)))
    *exp(0.125*delta_t_sq*pow(DG - 2*G, 2.))/pow(DG_sq - 4*G_sq, 4.);
}

__device__ double get_int_tb_spline(double delta_t,double G,double DM,double DG,double a,double b,double c,double d,double t_0,double t_1)
{
    double G_sq = G*G;
    double G_cub = G_sq*G;
    double DG_sq = DG*DG;
    double DG_cub = DG_sq*DG;
    double delta_t_sq = delta_t*delta_t;
    double t_0_sq = t_0*t_0;
    double t_0_cub = t_0_sq*t_0;
    double t_1_sq = t_1*t_1;
    double t_1_cub = t_1_sq*t_1;
    
    return 0.5*sqrt(2.)*sqrt(delta_t)
    *(-2*a*pow(DG - 2*G, 4.)*pow(DG + 2*G, 3.)*(-exp(-0.5*t_1*(DG + 2*G)) + exp(-0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq) 
    - 2*a*pow(DG - 2*G, 3.)*pow(DG + 2*G, 4.)*(exp(0.5*t_0*(DG - 2*G)) - exp(0.5*t_1*(DG - 2*G))) + b*pow(DG - 2*G, 4.)*pow(DG + 2*G, 2.)*(-2*(DG*t_0 + 2*G*t_0 + 2.)*exp(0.5*t_1*(DG + 2*G)) + 2*(DG*t_1 + 2*G*t_1 + 2.)*exp(0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq - 0.5*(DG + 2*G)*(t_0 + t_1)) 
    + 2*b*pow(DG - 2*G, 2.)*pow(DG + 2*G, 4.)*((-DG*t_0 + 2*G*t_0 + 2.)*exp(0.5*DG*t_0 + G*t_1) + (DG*t_1 - 2*G*t_1 - 2.)*exp(0.5*DG*t_1 + G*t_0))*exp(-G*(t_0 + t_1)) 
    + 2*c*pow(DG - 2*G, 4.)*(DG + 2*G)*(-(DG_sq*t_0_sq + 4*DG*t_0*(G*t_0 + 1) + 4*G_sq*t_0_sq + 8*G*t_0 + 8)*exp(0.5*t_1*(DG + 2*G)) + (DG_sq*t_1_sq + 4*DG*t_1*(G*t_1 + 1) + 4*G_sq*t_1_sq + 8*G*t_1 + 8)*exp(0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq - 0.5*(DG + 2*G)*(t_0 + t_1)) 
    + 2*c*(DG - 2*G)*pow(DG + 2*G, 4.)*(-(DG_sq*t_0_sq - 4*DG*t_0*(G*t_0 + 1) + 4*G_sq*t_0_sq + 8*G*t_0 + 8)*exp(0.5*DG*t_0 + G*t_1) + (DG_sq*t_1_sq - 4*DG*t_1*(G*t_1 + 1) + 4*G_sq*t_1_sq + 8*G*t_1 + 8)*exp(0.5*DG*t_1 + G*t_0))*exp(-G*(t_0 + t_1)) 
    + 2*d*pow(DG - 2*G, 4.)*((-DG_cub*t_0_cub - 6*DG_sq*t_0_sq*(G*t_0 + 1) - 12*DG*t_0*(G_sq*t_0_sq + 2*G*t_0 + 2.) - 8*G_cub*t_0_cub - 24*G_sq*t_0_sq - 48*G*t_0 + 48*exp(0.5*t_0*(DG + 2*G)) - 48)*exp(-0.5*t_0*(DG + 2*G)) + (DG_cub*t_1_cub + 6*DG_sq*t_1_sq*(G*t_1 + 1) + 12*DG*t_1*(G_sq*t_1_sq + 2*G*t_1 + 2.) + 8*G_cub*t_1_cub + 24*G_sq*t_1_sq + 48*G*t_1 - 48*exp(0.5*t_1*(DG + 2*G)) + 48)*exp(-0.5*t_1*(DG + 2*G)))*exp(DG*G*delta_t_sq) 
    - 2*d*pow(DG + 2*G, 4.)*(((DG_cub*t_0_cub - 6*DG_sq*t_0_sq*(G*t_0 + 1) + 12*DG*t_0*(G_sq*t_0_sq + 2*G*t_0 + 2.) - 8*G_cub*t_0_cub - 24*G_sq*t_0_sq - 48*G*t_0 - 48)*exp(0.5*DG*t_0) + 48*exp(G*t_0))*exp(-G*t_0) - ((DG_cub*t_1_cub - 6*DG_sq*t_1_sq*(G*t_1 + 1) + 12*DG*t_1*(G_sq*t_1_sq + 2*G*t_1 + 2.) - 8*G_cub*t_1_cub - 24*G_sq*t_1_sq - 48*G*t_1 - 48)*exp(0.5*DG*t_1) + 48*exp(G*t_1))*exp(-G*t_1)))
    *exp(0.125*delta_t_sq*pow(DG - 2*G, 2.))/pow(DG_sq - 4*G_sq, 4.);
}

__device__ double get_int_tc_spline(double delta_t,double G,double DM,double DG,double a,double b,double c,double d,double t_0,double t_1)
{
    double G_sq = G*G;
    double G_cub = G_sq*G;
    double DM_sq = DM*DM;
    double DM_fr = DM_sq*DM_sq;
    double DM_sx = DM_fr*DM_sq;
    double G_fr = G_sq*G_sq;
    double delta_t_sq = delta_t*delta_t;
    double t_0_sq = t_0*t_0;
    double t_0_cub = t_0_sq*t_0;
    double t_1_sq = t_1*t_1;
    double t_1_cub = t_1_sq*t_1;
    double exp_0_sin_1_term = exp(G*t_0)*sin(DM*G*delta_t_sq - DM*t_1);
    double exp_1_sin_0_term = exp(G*t_1)*sin(DM*G*delta_t_sq - DM*t_0);
    double exp_0_cos_1_term = exp(G*t_0)*cos(DM*G*delta_t_sq - DM*t_1);
    double exp_1_cos_0_term = exp(G*t_1)*cos(DM*G*delta_t_sq - DM*t_0);
    
    return (a*pow(DM_sq + G_sq, 3.)*(-DM*exp_0_sin_1_term + DM*exp_1_sin_0_term - G*exp_0_cos_1_term + G*exp_1_cos_0_term) 
    + b*pow(DM_sq + G_sq, 2.)*(DM*((DM_sq*t_0 + G_sq*t_0 + 2*G)*exp_1_sin_0_term - (DM_sq*t_1 + G_sq*t_1 + 2*G)*exp_0_sin_1_term) + (DM_sq*(G*t_0 - 1) + G_sq*(G*t_0 + 1))*exp_1_cos_0_term - (DM_sq*(G*t_1 - 1) + G_sq*(G*t_1 + 1))*exp_0_cos_1_term) 
    + c*(DM_sq + G_sq)*(DM*((DM_fr*t_0_sq + 2*DM_sq*(G_sq*t_0_sq + 2*G*t_0 - 1) + G_sq*(G_sq*t_0_sq + 4*G*t_0 + 6))*exp_1_sin_0_term - (DM_fr*t_1_sq + 2*DM_sq*(G_sq*t_1_sq + 2*G*t_1 - 1) + G_sq*(G_sq*t_1_sq + 4*G*t_1 + 6))*exp_0_sin_1_term) + (DM_fr*t_0*(G*t_0 - 2.) + 2*DM_sq*G*(G_sq*t_0_sq - 3.) + G_cub*(G_sq*t_0_sq + 2*G*t_0 + 2.))*exp_1_cos_0_term - (DM_fr*t_1*(G*t_1 - 2.) + 2*DM_sq*G*(G_sq*t_1_sq - 3.) + G_cub*(G_sq*t_1_sq + 2*G*t_1 + 2.))*exp_0_cos_1_term) 
    + d*(DM*((DM_sx*t_0_cub + 3*DM_fr*t_0*(G_sq*t_0_sq + 2*G*t_0 - 2.) + 3*DM_sq*G*(G_cub*t_0_cub + 4*G_sq*t_0_sq + 4*G*t_0 - 8) + G_cub*(G_cub*t_0_cub + 6*G_sq*t_0_sq + 18*G*t_0 + 24.))*exp_1_sin_0_term - (DM_sx*t_1_cub + 3*DM_fr*t_1*(G_sq*t_1_sq + 2*G*t_1 - 2.) + 3*DM_sq*G*(G_cub*t_1_cub + 4*G_sq*t_1_sq + 4*G*t_1 - 8) + G_cub*(G_cub*t_1_cub + 6*G_sq*t_1_sq + 18*G*t_1 + 24.))*exp_0_sin_1_term) + (DM_sx*t_0_sq*(G*t_0 - 3.) + 3*DM_fr*(G_cub*t_0_cub - G_sq*t_0_sq - 6*G*t_0 + 2.) + 3*DM_sq*G_sq*(G_cub*t_0_cub + G_sq*t_0_sq - 4*G*t_0 - 12.) + G_fr*(G_cub*t_0_cub + 3*G_sq*t_0_sq + 6*G*t_0 + 6))*exp_1_cos_0_term - (DM_sx*t_1_sq*(G*t_1 - 3.) + 3*DM_fr*(G_cub*t_1_cub - G_sq*t_1_sq - 6*G*t_1 + 2.) + 3*DM_sq*G_sq*(G_cub*t_1_cub + G_sq*t_1_sq - 4*G*t_1 - 12.) + G_fr*(G_cub*t_1_cub + 3*G_sq*t_1_sq + 6*G*t_1 + 6))*exp_0_cos_1_term))*sqrt(2.)*sqrt(delta_t)*exp(-G*(t_0 + t_1) + 0.5*delta_t_sq*(-DM_sq + G_sq))/pow(DM_sq + G_sq, 4.);
}

__device__ double get_int_td_spline(double delta_t,double G,double DM,double DG,double a,double b,double c,double d,double t_0,double t_1)
{
    double G_sq = G*G;
    double G_cub = G_sq*G;
    double G_fr = G_sq*G_sq;
    double G_fv = G_cub*G_sq;
    double DM_sq = DM*DM;
    double DM_fr = DM_sq*DM_sq;
    double DM_sx = DM_fr*DM_sq;
    double delta_t_sq = delta_t*delta_t;
    double t_0_sq = t_0*t_0;
    double t_0_cub = t_0_sq*t_0;
    double t_1_sq = t_1*t_1;
    double t_1_cub = t_1_sq*t_1;
    double exp_0_sin_1_term = exp(G*t_0)*sin(DM*G*delta_t_sq - DM*t_1);
    double exp_1_sin_0_term = exp(G*t_1)*sin(DM*G*delta_t_sq - DM*t_0);
    double exp_0_cos_1_term = exp(G*t_0)*cos(DM*G*delta_t_sq - DM*t_1);
    double exp_1_cos_0_term = exp(G*t_1)*cos(DM*G*delta_t_sq - DM*t_0);
    
    
    return -(a*pow(DM_sq + G_sq, 3.)*(DM*exp_0_cos_1_term - DM*exp_1_cos_0_term - G*exp_0_sin_1_term + G*exp_1_sin_0_term) 
    + b*pow(DM_sq + G_sq, 2.)*(DM_sq*G*t_0*exp_1_sin_0_term - DM_sq*G*t_1*exp_0_sin_1_term + DM_sq*exp_0_sin_1_term - DM_sq*exp_1_sin_0_term - DM*(DM_sq*t_0 + G_sq*t_0 + 2*G)*exp_1_cos_0_term + DM*(DM_sq*t_1 + G_sq*t_1 + 2*G)*exp_0_cos_1_term + G_cub*t_0*exp_1_sin_0_term - G_cub*t_1*exp_0_sin_1_term - G_sq*exp_0_sin_1_term + G_sq*exp_1_sin_0_term) 
    + c*(DM_sq + G_sq)*(DM_fr*G*t_0_sq*exp_1_sin_0_term - DM_fr*G*t_1_sq*exp_0_sin_1_term - 2*DM_fr*t_0*exp_1_sin_0_term + 2*DM_fr*t_1*exp_0_sin_1_term + 2*DM_sq*G_cub*t_0_sq*exp_1_sin_0_term - 2*DM_sq*G_cub*t_1_sq*exp_0_sin_1_term + 6*DM_sq*G*exp_0_sin_1_term - 6*DM_sq*G*exp_1_sin_0_term - DM*(DM_fr*t_0_sq + 2*DM_sq*(G_sq*t_0_sq + 2*G*t_0 - 1) + G_sq*(G_sq*t_0_sq + 4*G*t_0 + 6))*exp_1_cos_0_term + DM*(DM_fr*t_1_sq + 2*DM_sq*(G_sq*t_1_sq + 2*G*t_1 - 1) + G_sq*(G_sq*t_1_sq + 4*G*t_1 + 6))*exp_0_cos_1_term + G_fv*t_0_sq*exp_1_sin_0_term - G_fv*t_1_sq*exp_0_sin_1_term + 2*G_fr*t_0*exp_1_sin_0_term - 2*G_fr*t_1*exp_0_sin_1_term - 2*G_cub*exp_0_sin_1_term + 2*G_cub*exp_1_sin_0_term) 
    + d*(DM_sx*G*t_0_cub*exp_1_sin_0_term - DM_sx*G*t_1_cub*exp_0_sin_1_term - 3*DM_sx*t_0_sq*exp_1_sin_0_term + 3*DM_sx*t_1_sq*exp_0_sin_1_term + 3*DM_fr*G_cub*t_0_cub*exp_1_sin_0_term - 3*DM_fr*G_cub*t_1_cub*exp_0_sin_1_term - 3*DM_fr*G_sq*t_0_sq*exp_1_sin_0_term + 3*DM_fr*G_sq*t_1_sq*exp_0_sin_1_term - 18*DM_fr*G*t_0*exp_1_sin_0_term + 18*DM_fr*G*t_1*exp_0_sin_1_term - 6*DM_fr*exp_0_sin_1_term + 6*DM_fr*exp_1_sin_0_term + 3*DM_sq*G_fv*t_0_cub*exp_1_sin_0_term - 3*DM_sq*G_fv*t_1_cub*exp_0_sin_1_term + 3*DM_sq*G_fr*t_0_sq*exp_1_sin_0_term - 3*DM_sq*G_fr*t_1_sq*exp_0_sin_1_term - 12*DM_sq*G_cub*t_0*exp_1_sin_0_term + 12*DM_sq*G_cub*t_1*exp_0_sin_1_term + 36*DM_sq*G_sq*exp_0_sin_1_term - 36*DM_sq*G_sq*exp_1_sin_0_term - DM*(DM_sx*t_0_cub + 3*DM_fr*t_0*(G_sq*t_0_sq + 2*G*t_0 - 2.) + 3*DM_sq*G*(G_cub*t_0_cub + 4*G_sq*t_0_sq + 4*G*t_0 - 8) + G_cub*(G_cub*t_0_cub + 6*G_sq*t_0_sq + 18*G*t_0 + 24.))*exp_1_cos_0_term + DM*(DM_sx*t_1_cub + 3*DM_fr*t_1*(G_sq*t_1_sq + 2*G*t_1 - 2.) + 3*DM_sq*G*(G_cub*t_1_cub + 4*G_sq*t_1_sq + 4*G*t_1 - 8) + G_cub*(G_cub*t_1_cub + 6*G_sq*t_1_sq + 18*G*t_1 + 24.))*exp_0_cos_1_term + pow(G, 7)*t_0_cub*exp_1_sin_0_term - pow(G, 7)*t_1_cub*exp_0_sin_1_term + 3*pow(G, 6)*t_0_sq*exp_1_sin_0_term - 3*pow(G, 6)*t_1_sq*exp_0_sin_1_term + 6*G_fv*t_0*exp_1_sin_0_term - 6*G_fv*t_1*exp_0_sin_1_term - 6*G_fr*exp_0_sin_1_term + 6*G_fr*exp_1_sin_0_term))
    *sqrt(2.)*sqrt(delta_t)*exp(-G*(t_0 + t_1) + 0.5*delta_t_sq*(-DM_sq + G_sq))/pow(DM_sq + G_sq, 4.);
}

__device__ pycuda::complex<double> faddeeva(pycuda::complex<double> z)//, double t) 
{
   double in_real = pycuda::real(z);
   double in_imag = pycuda::imag(z);
   int n, nc, nu;
   double h, q, Saux, Sx, Sy, Tn, Tx, Ty, Wx, Wy, xh, xl, x, yh, y;
   double Rx [33];
   double Ry [33];

   x = fabs(in_real);
   y = fabs(in_imag);

   if (y < yLim && x < xLim) {
      q = (1.0 - y / yLim) * sqrt(1.0 - (x / xLim) * (x / xLim));
      h  = 1.0 / (3.2 * q);
      nc = 7 + int(23.0 * q);
//       xl = pow(h, double(1 - nc));
      double h_inv = 1./h;
      xl = h_inv;
      for(int i = 1; i < nc-1; i++)
          xl *= h_inv;
      
      xh = y + 0.5 / h;
      yh = x;
      nu = 10 + int(21.0 * q);
      Rx[nu] = 0.;
      Ry[nu] = 0.;
      for (n = nu; n > 0; n--){
         Tx = xh + n * Rx[n];
         Ty = yh - n * Ry[n];
         Tn = Tx*Tx + Ty*Ty;
         Rx[n-1] = 0.5 * Tx / Tn;
         Ry[n-1] = 0.5 * Ty / Tn;
         }
      Sx = 0.;
      Sy = 0.;
      for (n = nc; n>0; n--){
         Saux = Sx + xl;
         Sx = Rx[n-1] * Saux - Ry[n-1] * Sy;
         Sy = Rx[n-1] * Sy + Ry[n-1] * Saux;
         xl = h * xl;
      };
      Wx = errf_const * Sx;
      Wy = errf_const * Sy;
   }
   else {
      xh = y;
      yh = x;
      Rx[0] = 0.;
      Ry[0] = 0.;
      for (n = 9; n>0; n--){
         Tx = xh + n * Rx[0];
         Ty = yh - n * Ry[0];
         Tn = Tx * Tx + Ty * Ty;
         Rx[0] = 0.5 * Tx / Tn;
         Ry[0] = 0.5 * Ty / Tn;
      };
      Wx = errf_const * Rx[0];
      Wy = errf_const * Ry[0];
   }

   if (y == 0.) {
      Wx = exp(-x * x);
   }
   if (in_imag < 0.) {

      double exp_x2_y2 = exp(y * y - x * x);
      Wx =   2.0 * exp_x2_y2 * cos(2.0 * x * y) - Wx;
      Wy = - 2.0 * exp_x2_y2 * sin(2.0 * x * y) - Wy;
      if (in_real > 0.) {
         Wy = -Wy;
      }
   }
   else if (in_real < 0.) {
      Wy = -Wy;
   }

   return pycuda::complex<double>(Wx,Wy);
}

__device__ pycuda::complex<double> conv_exp_VC(double t, double Gamma, double omega, double sigma_t) 
{
    pycuda::complex<double> I(0,1);
    double sigma_t_sq = sigma_t*sigma_t;
    
    if(t>sigma_t_threshold*sigma_t)                   
        return 2.*(sqrt(0.5*M_PI))*exp(-Gamma*t+0.5*Gamma*Gamma*sigma_t_sq-0.5*omega*omega*sigma_t_sq)
                                  *(cos(omega*(t-Gamma*sigma_t_sq)) + I*sin(omega*(t-Gamma*sigma_t_sq)));
    else
    {
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*Gamma) - omega*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        pycuda::complex<double> fad = faddeeva(z);
        
        return sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*(pycuda::real(fad) - I*pycuda::imag(fad));
    }
}

__device__ int get_spline_bin(double time, int year)
{
    int i = 0;
//     while(i < get_spline_num_bins())
     if(year == 2015)
     {
        while(i < (sizeof(spline_t_2015)/sizeof(*spline_t_2015)))
        {
            if(time < spline_t_2015[i])
                break;
            i++;
        }
     }
     else
     {
        while(i < (sizeof(spline_t)/sizeof(*spline_t)))
        {
            if(time < spline_t[i])
                break;
            i++;
        }
     } 
    
    return i-1;
}

__device__ double spline_bin_deg(int ibin, int deg, int year) 
{ 
     if(year == 2015)
        return spline_pol_c_2015[ibin][deg];
     else
        return spline_pol_c[ibin][deg];
}

__device__ double calculate_time_acc(double time, int year)
{
   int bin = get_spline_bin(time, year);
    
   double a = spline_bin_deg(bin, 0, year);
   double b = spline_bin_deg(bin, 1, year);
   double c = spline_bin_deg(bin, 2, year);
   double d = spline_bin_deg(bin, 3, year);
    
   double acc = d*time*time*time + c*time*time + b*time + a;
   
   return acc;
}

__device__ double get_time_acc(double time)
{    
    int i;
    for(i = 0; i < time_acc_bins; i++)
    {
        if(time < low_time_acc_bins_ul[i])
            break;
    }
    
    double acc = low_time_acc_all[i];
    
    return acc;
}

// The functions Factorial, Kn, Mn_x and calculate_integral_terms are from an attempt to implement the procedure in https://arxiv.org/pdf/1407.0748.pdf
/*
__device__ double Factorial(int n) 
{
   if(n <= 0) 
    return 1.;
   
   double x = 1;
   int b = 0;
   do {
      b++;
      x *= b;
   } while(b!=n);
   
   return x;
}

__device__ pycuda::complex<double> Kn(pycuda::complex<double> z, int n) 
{
   if (n == 0) 
   {
       return 1./(2.*z);
   }
   else if (n == 1) 
   {
       return 1./(2.*z*z);
   }
   else if (n == 2) 
   {
       return 1./z*(1.+1./(z*z));
   }
   else if (n == 3) 
   {
       return 3./(z*z)*(1.+1./(z*z));
   }
    
   return pycuda::complex<double>(0.,0.);

 }

__device__ pycuda::complex<double> Mn_x(double x, int n, double t, double delta_t, double Gamma, double omega) 
{
   pycuda::complex<double> conv_term = conv_exp_VC(t, Gamma, omega, delta_t)/(sqrt(0.5*M_PI));
    
   if (n == 0) 
   {
       return pycuda::complex<double>(erf(x),0.)-conv_term;
   }
   else if (n == 1) 
   {
       return 2.*(-pycuda::complex<double>(sqrt(1./M_PI)*exp(-x*x),0.)-x*conv_term);
   }
   else if (n == 2) 
   {
       return 2.*(-2.*x*exp(-x*x)*pycuda::complex<double>(sqrt(1./M_PI),0.)-(2.*x*x-1.)*conv_term);
   }
   else if (n == 3) 
   {
       return 4.*(-(2.*x*x-1.)*exp(-x*x)*pycuda::complex<double>(sqrt(1./M_PI),0.)-x*(2.*x*x-3.)*conv_term);
   }
   
   return pycuda::complex<double>(0.,0.);
}

__device__ void calculate_integral_terms(double time_terms[4], double delta_t, double G, double DG, double DM, int year, const int spline_bins) 
{
    double* spline; 
    
    if(year == 2015)
    {
        spline = spline_t_2015;
    }
    else
    {
        spline = spline_t;
    }
    
   // Convert spline knots to x coordinates
   double spline_x[spline_bins+1];
   
   double sigma_t_sqrt2_inv = 1./(sqrt(2.)*delta_t);
      
   for(int i = 0; i < spline_bins+1; i++)
   {
       spline_x[i] = spline[i]*sigma_t_sqrt2_inv;
   }
   
   // Fill Sjk matrix
   //TODO speed to be gained here - Sjk is constant
   double S_matrix[spline_bins][4][4];
    
   for (int ibin=0; ibin < spline_bins; ++ibin) 
   {
       for (int i=0; i<4; ++i) 
        {
            for (int j=0; j<4; ++j) 
            {               
                if(i+j < 4)
                {
                    S_matrix[ibin][i][j] = spline_bin_deg(ibin,i+j,year)*Factorial(i+j)/Factorial(j)/Factorial(i)/pow(2.,i+j);
                }
                else
                {
                    S_matrix[ibin][i][j] = 0.;
                }
                
            }
        }
   }
   
   // Fill Kn
   //TODO speed to be gained here - only need to calculate this once per minimization
   pycuda::complex<double> z1_hyper_m = delta_t*pycuda::complex<double>(G-0.5*DG,0.)/sqrt(2.);
   pycuda::complex<double> z1_hyper_p = delta_t*pycuda::complex<double>(G+0.5*DG,0.)/sqrt(2.);
   pycuda::complex<double> z1_trigo   = delta_t*pycuda::complex<double>(G,-DM)/sqrt(2.);
   
   pycuda::complex<double> Kn_hyper_m_vector[4];
   pycuda::complex<double> Kn_hyper_p_vector[4];
   pycuda::complex<double> Kn_trigo_vector[4];
    
   for (int j=0; j<4; ++j) 
   {
       Kn_hyper_m_vector[j] = Kn(z1_hyper_m,j);
       Kn_hyper_p_vector[j] = Kn(z1_hyper_p,j);
       Kn_trigo_vector[j]   = Kn(z1_trigo,j);
   } 
   
   // Fill Mn
   pycuda::complex<double> Mn_hyper_m_matrix[spline_bins+1][4];
   pycuda::complex<double> Mn_hyper_p_matrix[spline_bins+1][4];
   pycuda::complex<double> Mn_trigo_matrix[spline_bins+1][4];
   
   for (int j=0; j<4; ++j) 
    {
        for(int ibin=0; ibin < spline_bins+1; ++ibin) 
            {               
                Mn_hyper_m_matrix[ibin][j]  = Mn_x(spline_x[ibin], j, spline[ibin], delta_t, G-0.5*DG, 0.);
                Mn_hyper_p_matrix[ibin][j]  = Mn_x(spline_x[ibin], j, spline[ibin], delta_t, G+0.5*DG, 0.);
                Mn_trigo_matrix[ibin][j]    = Mn_x(spline_x[ibin], j, spline[ibin], delta_t, G, DM);
//                 printf("ibin %d spline_t[ibin] %lf spline_x[ibin] %lf \n", ibin, spline_t[ibin], spline_x[ibin]);
            }
    }
  
  // Fill the delta factors to multiply by the integrals
  double delta_t_fact[4];
  
  for (int i=0; i<4; ++i) 
  {
      delta_t_fact[i] = pow(delta_t*sqrt(2.), i+1)/sqrt(2.);
  }
  
  // Integral calculation for cosh, sinh, cos, sin terms
   double integral_conv_exp_hyper_m = 0;
   double integral_conv_exp_hyper_p = 0;
   pycuda::complex<double> integral_conv_exp_trigo = pycuda::complex<double>(0.,0.);
   
   for (int ibin=0; ibin < spline_bins; ++ibin) 
    {
       for (int j=0; j<=3; ++j) 
         {  
             for (int k=0; k<=3-j; ++k) 
//              for (int k=0; k<=3; ++k) 
            {                
                integral_conv_exp_hyper_m += pycuda::real(S_matrix[ibin][j][k]
                                                           *(Mn_hyper_m_matrix[ibin+1][j] - Mn_hyper_m_matrix[ibin][j])
                                                           *Kn_hyper_m_vector[k])
                                                           *delta_t_fact[j+k];
                
                integral_conv_exp_hyper_p += pycuda::real(S_matrix[ibin][j][k]
                                                          *(Mn_hyper_p_matrix[ibin+1][j] - Mn_hyper_p_matrix[ibin][j])
                                                          *Kn_hyper_p_vector[k])
                                                          *delta_t_fact[j+k];
                
                integral_conv_exp_trigo   +=  S_matrix[ibin][j][k]
                                                  *(Mn_trigo_matrix[ibin+1][j] - Mn_trigo_matrix[ibin][j])
                                                  *Kn_trigo_vector[k]
                                                  *delta_t_fact[j+k];
            }
        }
//                 
   }
   
   //0:cosh, 1:sinh, 2:cos, 3:sin
   time_terms[0] = 0.5*(integral_conv_exp_hyper_m + integral_conv_exp_hyper_p);
   time_terms[1] = 0.5*(integral_conv_exp_hyper_m - integral_conv_exp_hyper_p);
   time_terms[2] = pycuda::real(integral_conv_exp_trigo);
   time_terms[3] = pycuda::imag(integral_conv_exp_trigo);
}*/

// Flavour tagging calibration B
__device__ double omega(double eta, int isOS)
{
    double om;
    
    if(isOS == 1)
        om = (p0_OS + dp0_hf_OS) + (p1_OS + dp1_hf_OS)*(eta - eta_bar_OS);
    else
        om = (p0_SSK + dp0_hf_SSK) + (p1_SSK + dp1_hf_SSK)*(eta - eta_bar_SSK);
    
    return om;    
}

// Flavour tagging calibration Bbar
__device__ double omega_bar(double eta, int isOS)
{
    double om_bar;
    
    if(isOS == 1)
        om_bar = (p0_OS - dp0_hf_OS) + (p1_OS - dp1_hf_OS)*(eta - eta_bar_OS);
    else
        om_bar = (p0_SSK - dp0_hf_SSK) + (p1_SSK - dp1_hf_SSK)*(eta - eta_bar_SSK);
    
    return om_bar;  
}

// Decay time resolution calibration
__device__ double delta_RunII(double sigma_t)
{
    return sigma_t_a_RunII*sigma_t*sigma_t + sigma_t_b_RunII*sigma_t + sigma_t_c_RunII;
}

__device__ double Nk(double A_0_abs, 
                     double A_S_abs,
                     double A_pa_abs, 
                     double A_pe_abs,
                     double CSP,
                     int k)
{
    double nk;
    switch(k) {
        case 1:  nk = A_0_abs*A_0_abs;
                 break;
        case 2:  nk = A_pa_abs*A_pa_abs;
                 break;
        case 3:  nk = A_pe_abs*A_pe_abs;
                 break;
        case 4:  nk = A_pe_abs*A_pa_abs;
                 break;
        case 5:  nk = A_0_abs*A_pa_abs;
                 break;
        case 6:  nk = A_0_abs*A_pe_abs;
                 break;
        case 7:  nk = A_S_abs*A_S_abs;
                 break;
        case 8:  nk = CSP*A_S_abs*A_pa_abs;
                 break;
        case 9:  nk = CSP*A_S_abs*A_pe_abs;
                 break;
        case 10: nk = CSP*A_S_abs*A_0_abs;
                 break;
        default: printf("Wrong k index in nk, please check code %d\\n", k);
                 return 0.;
    }
    return nk;
}

__device__ double fk(double helcosthetaK, 
                     double helcosthetaL, 
                     double helphi,
                     int k)
{
    double helsinthetaK = sqrt(1. - helcosthetaK*helcosthetaK);
    double helsinthetaL = sqrt(1. - helcosthetaL*helcosthetaL);
//     helphi -= M_PI;
    double helsinphi = sin(-helphi);
    double helcosphi = cos(-helphi);
    
    double fk;
    switch(k) {
        case 1:  fk = helcosthetaK*helcosthetaK*helsinthetaL*helsinthetaL;
                 break;
        case 2:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helcosphi*helcosphi*helsinthetaL*helsinthetaL);
                 break;
        case 3:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helsinphi*helsinphi*helsinthetaL*helsinthetaL);
                 break;
        case 4:  fk = helsinthetaK*helsinthetaK*helsinthetaL*helsinthetaL*helsinphi*helcosphi;
                 break;
        case 5:  fk = sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helcosphi;
                 break;
        case 6:  fk = -sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helsinphi;
                 break;
        case 7:  fk = helsinthetaL*helsinthetaL/3.;
                 break;
        case 8:  fk = 2.*helsinthetaK*helsinthetaL*helcosthetaL*helcosphi/sqrt(6.);
                 break;
        case 9:  fk = -2.*helsinthetaK*helsinthetaL*helcosthetaL*helsinphi/sqrt(6.);
                 break;
        case 10: fk = 2.*helcosthetaK*helsinthetaL*helsinthetaL/sqrt(3.);
                 break;
        default: printf("Wrong k index in fk, please check code %d\\n", k);
                 return 0.;
    }
    return fk;
}

__device__ double ak(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double ak;
    switch(k) {
        case 1:  ak = 0.5*(1.+lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ak = 0.5*(1.+lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ak = 0.5*(1.+lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ak = 0.5*(sin(delta_pe-delta_pa) - lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ak = 0.5*(cos(delta_0-delta_pa) + lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ak = -0.5*(sin(delta_0-delta_pe) - lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ak = 0.5*(1.+lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ak = 0.5*(cos(delta_S-delta_pa) - lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ak = -0.5*(sin(delta_S-delta_pe) + lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ak = 0.5*(cos(delta_S-delta_0) - lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ak, please check code %d\\n", k);
                 return 0.;
    }
    return ak;
}

__device__ double bk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double bk;
    switch(k) {
        case 1:  bk = -lambda_0_abs*cos(phis_0);
                 break;
        case 2:  bk = -lambda_pa_abs*cos(phis_pa);
                 break;
        case 3:  bk = lambda_pe_abs*cos(phis_pe);
                 break;
        case 4:  bk = 0.5*(lambda_pe_abs*sin(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*sin(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  bk = -0.5*(lambda_0_abs*cos(delta_0-delta_pa-phis_0) + lambda_pa_abs*cos(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  bk = 0.5*(lambda_0_abs*sin(delta_0-delta_pe-phis_0) + lambda_pe_abs*sin(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  bk = lambda_S_abs*cos(phis_S);
                 break;
        case 8:  bk = 0.5*(lambda_S_abs*cos(delta_S-delta_pa-phis_S) - lambda_pa_abs*cos(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  bk = -0.5*(lambda_S_abs*sin(delta_S-delta_pe-phis_S) - lambda_pe_abs*sin(delta_pe-delta_S-phis_pe));
                 break;
        case 10: bk = 0.5*(lambda_S_abs*cos(delta_S-delta_0-phis_S) - lambda_0_abs*cos(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in bk, please check code %d\\n", k);
                 return 0.;
    }
    return bk;
}
    
__device__ double ck(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double ck;
    switch(k) {
        case 1:  ck = 0.5*(1.-lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ck = 0.5*(1.-lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ck = 0.5*(1.-lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ck = 0.5*(sin(delta_pe-delta_pa) + lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ck = 0.5*(cos(delta_0-delta_pa) - lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ck = -0.5*(sin(delta_0-delta_pe) + lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ck = 0.5*(1.-lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ck = 0.5*(cos(delta_S-delta_pa) + lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ck = -0.5*(sin(delta_S-delta_pe) - lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ck = 0.5*(cos(delta_S-delta_0) + lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ck, please check code %d\\n", k);
                 return 0.;
    }
    return ck;
}

__device__ double dk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double dk;
    switch(k) {
        case 1:  dk = lambda_0_abs*sin(phis_0);
                 break;
        case 2:  dk = lambda_pa_abs*sin(phis_pa);
                 break;
        case 3:  dk = -lambda_pe_abs*sin(phis_pe);
                 break;
        case 4:  dk = -0.5*(lambda_pe_abs*cos(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*cos(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  dk = -0.5*(lambda_0_abs*sin(delta_0-delta_pa-phis_0) + lambda_pa_abs*sin(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  dk = -0.5*(lambda_0_abs*cos(delta_0-delta_pe-phis_0) + lambda_pe_abs*cos(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  dk = -lambda_S_abs*sin(phis_S);
                 break;
        case 8:  dk = 0.5*(lambda_S_abs*sin(delta_S-delta_pa-phis_S) - lambda_pa_abs*sin(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  dk = -0.5*(-lambda_S_abs*cos(delta_S-delta_pe-phis_S) + lambda_pe_abs*cos(delta_pe-delta_S-phis_pe));
                 break;
        case 10: dk = 0.5*(lambda_S_abs*sin(delta_S-delta_0-phis_S) - lambda_0_abs*sin(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in dk, please check code %d\\n", k);
    
             return 0.;
    }
    return dk;
}


__device__ void integral4pitime(double integral[2], double vNk[10], double vak[10],double vbk[10],double vck[10],double vdk[10], 
                                double G, double DG, double DM, double delta_t)
{
  //double Nk, ak, bk, ck, dk;
  
  double low_time_acc;
  double t_1;
  double conv_sin_term_t_0, conv_cos_term_t_0, conv_sinh_term_t_0, conv_cosh_term_t_0;
  double conv_sin_term_t_1, conv_cos_term_t_1, conv_sinh_term_t_1, conv_cosh_term_t_1;
  double int_ta, int_tb, int_tc, int_td;

  pycuda::complex<double> hyper_p = conv_exp_VC(t_ll, G + 0.5*DG, 0., delta_t);
  pycuda::complex<double> hyper_m = conv_exp_VC(t_ll, G - 0.5*DG, 0., delta_t);
  pycuda::complex<double> trig    = conv_exp_VC(t_ll, G, DM, delta_t);

  conv_cosh_term_t_0 = pycuda::real(0.5*(hyper_m + hyper_p));
  conv_sinh_term_t_0 = pycuda::real(0.5*(hyper_m - hyper_p));
  conv_cos_term_t_0  = pycuda::real(trig);
  conv_sin_term_t_0  = pycuda::imag(trig);
  
  integral[0] = 0.;
  integral[1] = 0.;
  double den1 = -DG*DG + 4*G*G;
  double den2 = DM*DM + G*G;
  
  for(int i=0; i<time_acc_bins; i++)
  {
    t_1 = low_time_acc_bins_ul[i];
 
    hyper_p = conv_exp_VC(t_1, G + 0.5*DG, 0., delta_t);
    hyper_m = conv_exp_VC(t_1, G - 0.5*DG, 0., delta_t);
    trig    = conv_exp_VC(t_1, G, DM, delta_t);

    conv_cosh_term_t_1 = pycuda::real(0.5*(hyper_m + hyper_p));
    conv_sinh_term_t_1 = pycuda::real(0.5*(hyper_m - hyper_p));
    conv_cos_term_t_1  = pycuda::real(trig);
    conv_sin_term_t_1  = pycuda::imag(trig);
    
    low_time_acc = low_time_acc_all[i];
            
    int_ta = 2*(DG*conv_sinh_term_t_0 + 2*G*conv_cosh_term_t_0 - DG*conv_sinh_term_t_1 - 2*G*conv_cosh_term_t_1)/den1;
    int_tb = 2*(DG*conv_cosh_term_t_0 + 2*G*conv_sinh_term_t_0 - DG*conv_cosh_term_t_1 - 2*G*conv_sinh_term_t_1)/den1;
    int_tc =   (-DM*conv_sin_term_t_0 + G*conv_cos_term_t_0 + DM*conv_sin_term_t_1 - G*conv_cos_term_t_1)/den2;
    int_td =   (DM*conv_cos_term_t_0 + G*conv_sin_term_t_0 - DM*conv_cos_term_t_1 - G*conv_sin_term_t_1)/den2;
    
    for(int k=0; k<10; k++)
    {
      integral[0] += low_time_acc*vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb + vck[k]*int_tc + vdk[k]*int_td);
      integral[1] += low_time_acc*vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb - vck[k]*int_tc - vdk[k]*int_td); 
    }
        
    conv_sin_term_t_0  = conv_sin_term_t_1;
    conv_cos_term_t_0  = conv_cos_term_t_1;
    conv_sinh_term_t_0 = conv_sinh_term_t_1;
    conv_cosh_term_t_0 = conv_cosh_term_t_1;
    }       
} 

//This integral works in the limit t>>sigma_t, e.g. t>0.3ps, and should not be used for low times.
__device__ void integral4pitime_spline( double integral[2], double vNk[10], double vak[10],double vbk[10],
                                        double vck[10],double vdk[10], 
                                        double G, double DG, double DM, double delta_t, int year)
{  
  double t_0 = t_ll;//ATTENTION
  double t_1;
  int bin0;
  int spline_bins;
  
  if(year == 2015)
  {
      bin0 = 0;
      t_1 = spline_t_2015[bin0 + 1];
      spline_bins = spline_num_bins_2015;
  }
  else
  {  
      if (t_0 == 0.2) 
        bin0 = 0;//ATTENTION
      else if (t_0 == 0.25) 
        bin0 = 2;
      else if (t_0 == 0.3) 
        bin0 = 2;
  
      t_1 = spline_t[bin0 + 1];
      spline_bins = spline_num_bins;
  }
  
  for(int bin = bin0; bin < spline_bins; bin++)//ATTENTION
  {
    double a = spline_bin_deg(bin,0,year);
    double b = spline_bin_deg(bin,1,year);
    double c = spline_bin_deg(bin,2,year);
    double d = spline_bin_deg(bin,3,year);
    
    double int_ta = get_int_ta_spline( delta_t, G, DM, DG, a, b, c, d, t_0, t_1);
    double int_tb = get_int_tb_spline( delta_t, G, DM, DG, a, b, c, d, t_0, t_1);
    double int_tc = get_int_tc_spline( delta_t, G, DM, DG, a, b, c, d, t_0, t_1);
    double int_td = get_int_td_spline( delta_t, G, DM, DG, a, b, c, d, t_0, t_1);
        
    for(int k=0; k<10; k++)
    {
        integral[0] += vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb + vck[k]*int_tc + vdk[k]*int_td);
        integral[1] += vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb - vck[k]*int_tc - vdk[k]*int_td); 
    }   
    
    if(year == 2015)
    {
        t_0 = spline_t_2015[bin+1];
        t_1 = spline_t_2015[bin+2];
    }
    else
    {
        t_0 = spline_t[bin+1];
        t_1 = spline_t[bin+2];
    }
  }
} 

__global__ void DiffRate(double *data, 
                         double *out, 
                         double CSP,
                         double A_0_abs,
                         double A_S_abs,
                         double A_pa_abs,
                         double A_pe_abs,
                         double phis_0,
                         double phis_S,
                         double phis_pa,
                         double phis_pe,
                         double delta_S,
                         double delta_pa,
                         double delta_pe,
                         double l_0_abs,
                         double l_S_abs,
                         double l_pa_abs,
                         double l_pe_abs,
                         double G, 
                         double DG, 
                         double DM, 
                         int Nevt)
{
int row = threadIdx.x + blockDim.x * blockIdx.x; //ntuple entry

if (row >= Nevt) { 
    return;
}

int i0 = row*10;// general rule for cuda matrices : index = col + row*N; 
int idx = 0 + i0; 
int idy = 1 + i0;
int idz = 2 + i0;
int idt = 3 + i0;
int idsigma_t = 4 + i0;
int idq_OS = 5 + i0;
int idq_SSK = 6 + i0;
int ideta_OS = 7 + i0;
int ideta_SSK = 8 + i0;
//int sweights = 9 + i0;

double helcosthetaK = data[idx];
double helcosthetaL = data[idy];
double helphi = data[idz];
double t = data[idt];
double sigma_t = data[idsigma_t];
double q_OS = data[idq_OS];
double q_SSK = data[idq_SSK];
double eta_OS = data[ideta_OS];
double eta_SSK = data[ideta_SSK];

double delta_0 = 0.;

double pdf_B = 0.;
double pdf_Bbar = 0.;  

double delta_t =  delta_RunII(sigma_t);

double omega_OS = omega(eta_OS, 1);
double omega_bar_OS = omega_bar(eta_OS, 1);
double omega_SSK = omega(eta_SSK, 0);
double omega_bar_SSK = omega_bar(eta_SSK, 0);

pycuda::complex<double> hyper_p = conv_exp_VC(t, G + 0.5*DG, 0., delta_t);
pycuda::complex<double> hyper_m = conv_exp_VC(t, G - 0.5*DG, 0., delta_t);
pycuda::complex<double> trig    = conv_exp_VC(t, G, DM, delta_t);

double ta = pycuda::real(0.5*(hyper_m + hyper_p));
double tb = pycuda::real(0.5*(hyper_m - hyper_p));
double tc = pycuda::real(trig);
double td = pycuda::imag(trig);

double vNk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vak[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vbk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vck[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vdk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};

for(int k = 1; k <= 10; k++)
{
    double Nk_term = Nk(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,CSP,k);
    double fk_term = fk(helcosthetaK,helcosthetaL,helphi,k); 
    
    double ak_term = ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);    
    double bk_term = bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    double ck_term = ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    double dk_term = dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    
    double hk_B    = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb + ck_term*tc + dk_term*td);
    double hk_Bbar = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb - ck_term*tc - dk_term*td);
    
    pdf_B    += Nk_term*hk_B*fk_term;            
    pdf_Bbar += Nk_term*hk_Bbar*fk_term;

    vNk[k-1] = 1.*Nk_term;
    vak[k-1] = 1.*ak_term;
    vbk[k-1] = 1.*bk_term;
    vck[k-1] = 1.*ck_term;
    vdk[k-1] = 1.*dk_term;
}

double integral[2] = {0.,0.};

// Time acceptance with a histogram
// double time_acc = get_time_acc(t);
// Integral time acceptance with a histogram
// integral4pitime(integral, vNk,vak,vbk,vck,vdk, G, DG,DM, delta_t);

// Time acceptance with a spline
double time_acc = calculate_time_acc(t, year_const); 
// Integral time acceptance with a spline
integral4pitime_spline(integral, vNk,vak,vbk,vck,vdk, G, DG,DM, delta_t, year_const);

// printf("time_acc = %lf integral[0] %lf integral[1] %lf \n", time_acc, integral[0], integral[1]);
    
double int_B = integral[0];
double int_Bbar = integral[1];

// Total PDF
double num = time_acc*((1.+ q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*pdf_B 
                     + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*pdf_Bbar);

// Total PDF integral
double den =  ((1.+q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*int_B 
             + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*int_Bbar);

 out[row] = num/den;
}       
