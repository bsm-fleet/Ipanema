#include <stdio.h>
//#include <math.h>
// #include <thrust/complex.h>
#include <pycuda-complex.hpp>
#include<curand.h>
#include<curand_kernel.h>
#include "/scratch15/diego/gitcrap4/cuda/tag_gen.c"
#define errf_const 1.12837916709551
#define xLim 5.33
#define yLim 4.29 
#define sigma_t_threshold 5
#define time_acc_bins 40
#define spl_bins 7

extern "C" 
{
__device__ double flat_normweights[10] = {1., 1.0, 1.0, 0.00, 0.00, 0.000, 1.0, 0.00, 0.00, 0.000};

// __device__ double low_time_acc_bins_ul[time_acc_bins] = 
// {0.23797471677166174, 0.27693584616386924, 0.31693600305260117, 0.35803212761855635, 0.40028597282970174, 0.4437646625969446, 0.48854133326607896, 0.5346958738379816, 0.5823157837313284, 0.6314971712226232, 0.6823459211913355, 0.7349790678339851, 0.7895264170955251, 0.8461324753957553, 0.9049587567724324, 0.966186561187383, 1.0300203443952365, 1.0966918372716514, 1.1664651239654924, 1.2396429598202376, 1.3165747110027106, 1.3976664425174057, 1.483393892317744, 1.5743193827952986, 1.6711141967271264, 1.7745886837810803, 1.8857335419899877, 2.0057776537213834, 2.1362711425364616, 2.279208120728858, 2.43721431642301, 2.613845648787718, 2.814087169543123, 3.045239101577547, 3.3186177201267117, 3.653177485580561, 4.084441360908032, 4.692120690735037, 5.730287538377153, 15.0};

// __device__ double low_time_acc[time_acc_bins] = {0.2634384036064148, 0.3077799677848816, 0.3243389427661896, 0.3367297947406769, 0.3454984724521637, 0.351309597492218, 0.3549683690071106, 0.35744380950927734, 0.35949477553367615, 0.36121028661727905, 0.3626016676425934, 0.36368557810783386, 0.3644851744174957, 0.3650318384170532, 0.36536726355552673, 0.36554619669914246, 0.365639865398407, 0.365723580121994, 0.36581581830978394, 0.36591747403144836, 0.3660294711589813, 0.3661530315876007, 0.3662893772125244, 0.3664401173591614, 0.36660704016685486, 0.3667922616004944, 0.3669983744621277, 0.3672284483909607, 0.36748626828193665, 0.3677765130996704, 0.36810508370399475, 0.36847949028015137, 0.3689095377922058, 0.3694082498550415, 0.36999329924583435, 0.37068918347358704, 0.37152960896492004, 0.3725546896457672, 0.373737096786499, 0.3596302568912506};

__device__ double get_ta (double t,double G,double DG) { return exp(-G*t)*cosh(DG*t/2); }
__device__ double get_tb (double t,double G,double DG) { return exp(-G*t)*sinh(DG*t/2); }
__device__ double get_tc (double t,double G,double DM) { return exp(-G*t)*cos(DM*t); }
__device__ double get_td (double t,double G,double DM) { return exp(-G*t)*sin(DM*t); }

__device__ double get_int_ta(double t_0, double t_1, double G,double DG) 
{ 
    return (2*(DG*sinh(.5*DG*t_0) + 2*G*cosh(.5*DG*t_0))*exp(G*t_1) - 2*(DG*sinh(.5*DG*t_1) + 2*G*cosh(.5*DG*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(-pow(DG, 2) + 4 *pow(G, 2));
}
__device__ double get_int_tb(double t_0, double t_1,double G,double DG) 
{
    return (2*(DG*cosh(.5*DG*t_0) + 2*G*sinh(.5*DG*t_0))*exp(G*t_1) - 2*(DG*cosh(.5*DG*t_1) + 2*G*sinh(.5*DG*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(-pow(DG, 2) + 4*pow(G, 2));
}
__device__ double get_int_tc(double t_0, double t_1,double G,double DM)
{
    return ((-DM*sin(DM*t_0) + G*cos(DM*t_0))*exp(G*t_1) + (DM*sin(DM*t_1) - G*cos(DM*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(pow(DM, 2) + pow(G, 2));
}

__device__ double get_int_td(double t_0, double t_1,double G,double DM)
{
    return ((DM*cos(DM*t_0) + G*sin(DM*t_0))*exp(G*t_1) - (DM*cos(DM*t_1) + G*sin(DM*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(pow(DM, 2) + pow(G, 2));
}

__device__ double get_int_ta_spline(double delta_t,double G,double DM,double DG,double a,double b,double c,double d,double t_0,double t_1)
{
    double G_sq = G*G;
    double G_cub = G_sq*G;
    double DG_sq = DG*DG;
    double DG_cub = DG_sq*DG;
    double delta_t_sq = delta_t*delta_t;
    double t_0_sq = t_0*t_0;
    double t_0_cub = t_0_sq*t_0;
    double t_1_sq = t_1*t_1;
    double t_1_cub = t_1_sq*t_1;
    
    return -0.5*sqrt(2.)*sqrt(delta_t)
    *(-2*a*pow(DG - 2*G, 4.)*pow(DG + 2*G, 3.)*(-exp(-0.5*t_1*(DG + 2*G)) + exp(-0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq) 
     + 2*a*pow(DG - 2*G, 3.)*pow(DG + 2*G, 4.)*(exp(0.5*t_0*(DG - 2*G)) - exp(0.5*t_1*(DG - 2*G))) 
     + b*pow(DG - 2*G, 4.)*pow(DG + 2*G, 2.)*(-2*(DG*t_0 + 2*G*t_0 + 2.)*exp(0.5*t_1*(DG + 2*G)) + 2*(DG*t_1 + 2*G*t_1 + 2.)*exp(0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq - 0.5*(DG + 2*G)*(t_0 + t_1)) 
     - 2*b*pow(DG - 2*G, 2.)*pow(DG + 2*G, 4.)*((-DG*t_0 + 2*G*t_0 + 2.)*exp(0.5*DG*t_0 + G*t_1) + (DG*t_1 - 2*G*t_1 - 2.)*exp(0.5*DG*t_1 + G*t_0))*exp(-G*(t_0 + t_1)) 
     + 2*c*pow(DG - 2*G, 4.)*(DG + 2*G)*(-(DG_sq*t_0_sq + 4*DG*t_0*(G*t_0 + 1) + 4*G_sq*t_0_sq + 8*G*t_0 + 8)*exp(0.5*t_1*(DG + 2*G)) + (DG_sq*t_1_sq + 4*DG*t_1*(G*t_1 + 1) + 4*G_sq*t_1_sq + 8*G*t_1 + 8)*exp(0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq - 0.5*(DG + 2*G)*(t_0 + t_1)) 
     - 2*c*(DG - 2*G)*pow(DG + 2*G, 4.)*(-(DG_sq*t_0_sq - 4*DG*t_0*(G*t_0 + 1) + 4*G_sq*t_0_sq + 8*G*t_0 + 8)*exp(0.5*DG*t_0 + G*t_1) + (DG_sq*t_1_sq - 4*DG*t_1*(G*t_1 + 1) + 4*G_sq*t_1_sq + 8*G*t_1 + 8)*exp(0.5*DG*t_1 + G*t_0))*exp(-G*(t_0 + t_1)) 
     + 2*d*pow(DG - 2*G, 4.)*((-DG_cub*t_0_cub - 6*DG_sq*t_0_sq*(G*t_0 + 1) - 12*DG*t_0*(G_sq*t_0_sq + 2*G*t_0 + 2.) - 8*G_cub*t_0_cub - 24*G_sq*t_0_sq - 48*G*t_0 + 48*exp(0.5*t_0*(DG + 2*G)) - 48)*exp(-0.5*t_0*(DG + 2*G)) + (DG_cub*t_1_cub + 6*DG_sq*t_1_sq*(G*t_1 + 1) + 12*DG*t_1*(G_sq*t_1_sq + 2*G*t_1 + 2.) + 8*G_cub*t_1_cub + 24*G_sq*t_1_sq + 48*G*t_1 - 48*exp(0.5*t_1*(DG + 2*G)) + 48)*exp(-0.5*t_1*(DG + 2*G)))*exp(DG*G*delta_t_sq) 
     + 2*d*pow(DG + 2*G, 4.)*(((DG_cub*t_0_cub - 6*DG_sq*t_0_sq*(G*t_0 + 1) + 12*DG*t_0*(G_sq*t_0_sq + 2*G*t_0 + 2.) - 8*G_cub*t_0_cub - 24*G_sq*t_0_sq - 48*G*t_0 - 48)*exp(0.5*DG*t_0) + 48*exp(G*t_0))*exp(-G*t_0) - ((DG_cub*t_1_cub - 6*DG_sq*t_1_sq*(G*t_1 + 1) + 12*DG*t_1*(G_sq*t_1_sq + 2*G*t_1 + 2.) - 8*G_cub*t_1_cub - 24*G_sq*t_1_sq - 48*G*t_1 - 48)*exp(0.5*DG*t_1) + 48*exp(G*t_1))*exp(-G*t_1)))
    *exp(0.125*delta_t_sq*pow(DG - 2*G, 2.))/pow(DG_sq - 4*G_sq, 4.);
}

__device__ double get_int_tb_spline(double delta_t,double G,double DM,double DG,double a,double b,double c,double d,double t_0,double t_1)
{
    double G_sq = G*G;
    double G_cub = G_sq*G;
    double DG_sq = DG*DG;
    double DG_cub = DG_sq*DG;
    double delta_t_sq = delta_t*delta_t;
    double t_0_sq = t_0*t_0;
    double t_0_cub = t_0_sq*t_0;
    double t_1_sq = t_1*t_1;
    double t_1_cub = t_1_sq*t_1;
    
    return 0.5*sqrt(2.)*sqrt(delta_t)
    *(-2*a*pow(DG - 2*G, 4.)*pow(DG + 2*G, 3.)*(-exp(-0.5*t_1*(DG + 2*G)) + exp(-0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq) 
    - 2*a*pow(DG - 2*G, 3.)*pow(DG + 2*G, 4.)*(exp(0.5*t_0*(DG - 2*G)) - exp(0.5*t_1*(DG - 2*G))) + b*pow(DG - 2*G, 4.)*pow(DG + 2*G, 2.)*(-2*(DG*t_0 + 2*G*t_0 + 2.)*exp(0.5*t_1*(DG + 2*G)) + 2*(DG*t_1 + 2*G*t_1 + 2.)*exp(0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq - 0.5*(DG + 2*G)*(t_0 + t_1)) 
    + 2*b*pow(DG - 2*G, 2.)*pow(DG + 2*G, 4.)*((-DG*t_0 + 2*G*t_0 + 2.)*exp(0.5*DG*t_0 + G*t_1) + (DG*t_1 - 2*G*t_1 - 2.)*exp(0.5*DG*t_1 + G*t_0))*exp(-G*(t_0 + t_1)) 
    + 2*c*pow(DG - 2*G, 4.)*(DG + 2*G)*(-(DG_sq*t_0_sq + 4*DG*t_0*(G*t_0 + 1) + 4*G_sq*t_0_sq + 8*G*t_0 + 8)*exp(0.5*t_1*(DG + 2*G)) + (DG_sq*t_1_sq + 4*DG*t_1*(G*t_1 + 1) + 4*G_sq*t_1_sq + 8*G*t_1 + 8)*exp(0.5*t_0*(DG + 2*G)))*exp(DG*G*delta_t_sq - 0.5*(DG + 2*G)*(t_0 + t_1)) 
    + 2*c*(DG - 2*G)*pow(DG + 2*G, 4.)*(-(DG_sq*t_0_sq - 4*DG*t_0*(G*t_0 + 1) + 4*G_sq*t_0_sq + 8*G*t_0 + 8)*exp(0.5*DG*t_0 + G*t_1) + (DG_sq*t_1_sq - 4*DG*t_1*(G*t_1 + 1) + 4*G_sq*t_1_sq + 8*G*t_1 + 8)*exp(0.5*DG*t_1 + G*t_0))*exp(-G*(t_0 + t_1)) 
    + 2*d*pow(DG - 2*G, 4.)*((-DG_cub*t_0_cub - 6*DG_sq*t_0_sq*(G*t_0 + 1) - 12*DG*t_0*(G_sq*t_0_sq + 2*G*t_0 + 2.) - 8*G_cub*t_0_cub - 24*G_sq*t_0_sq - 48*G*t_0 + 48*exp(0.5*t_0*(DG + 2*G)) - 48)*exp(-0.5*t_0*(DG + 2*G)) + (DG_cub*t_1_cub + 6*DG_sq*t_1_sq*(G*t_1 + 1) + 12*DG*t_1*(G_sq*t_1_sq + 2*G*t_1 + 2.) + 8*G_cub*t_1_cub + 24*G_sq*t_1_sq + 48*G*t_1 - 48*exp(0.5*t_1*(DG + 2*G)) + 48)*exp(-0.5*t_1*(DG + 2*G)))*exp(DG*G*delta_t_sq) 
    - 2*d*pow(DG + 2*G, 4.)*(((DG_cub*t_0_cub - 6*DG_sq*t_0_sq*(G*t_0 + 1) + 12*DG*t_0*(G_sq*t_0_sq + 2*G*t_0 + 2.) - 8*G_cub*t_0_cub - 24*G_sq*t_0_sq - 48*G*t_0 - 48)*exp(0.5*DG*t_0) + 48*exp(G*t_0))*exp(-G*t_0) - ((DG_cub*t_1_cub - 6*DG_sq*t_1_sq*(G*t_1 + 1) + 12*DG*t_1*(G_sq*t_1_sq + 2*G*t_1 + 2.) - 8*G_cub*t_1_cub - 24*G_sq*t_1_sq - 48*G*t_1 - 48)*exp(0.5*DG*t_1) + 48*exp(G*t_1))*exp(-G*t_1)))
    *exp(0.125*delta_t_sq*pow(DG - 2*G, 2.))/pow(DG_sq - 4*G_sq, 4.);
}

__device__ double get_int_tc_spline(double delta_t,double G,double DM,double DG,double a,double b,double c,double d,double t_0,double t_1)
{
    double G_sq = G*G;
    double G_cub = G_sq*G;
    double DM_sq = DM*DM;
    double DM_fr = DM_sq*DM_sq;
    double DM_sx = DM_fr*DM_sq;
    double G_fr = G_sq*G_sq;
    double delta_t_sq = delta_t*delta_t;
    double t_0_sq = t_0*t_0;
    double t_0_cub = t_0_sq*t_0;
    double t_1_sq = t_1*t_1;
    double t_1_cub = t_1_sq*t_1;
    double exp_0_sin_1_term = exp(G*t_0)*sin(DM*G*delta_t_sq - DM*t_1);
    double exp_1_sin_0_term = exp(G*t_1)*sin(DM*G*delta_t_sq - DM*t_0);
    double exp_0_cos_1_term = exp(G*t_0)*cos(DM*G*delta_t_sq - DM*t_1);
    double exp_1_cos_0_term = exp(G*t_1)*cos(DM*G*delta_t_sq - DM*t_0);
    
    return (a*pow(DM_sq + G_sq, 3.)*(-DM*exp_0_sin_1_term + DM*exp_1_sin_0_term - G*exp_0_cos_1_term + G*exp_1_cos_0_term) 
    + b*pow(DM_sq + G_sq, 2.)*(DM*((DM_sq*t_0 + G_sq*t_0 + 2*G)*exp_1_sin_0_term - (DM_sq*t_1 + G_sq*t_1 + 2*G)*exp_0_sin_1_term) + (DM_sq*(G*t_0 - 1) + G_sq*(G*t_0 + 1))*exp_1_cos_0_term - (DM_sq*(G*t_1 - 1) + G_sq*(G*t_1 + 1))*exp_0_cos_1_term) 
    + c*(DM_sq + G_sq)*(DM*((DM_fr*t_0_sq + 2*DM_sq*(G_sq*t_0_sq + 2*G*t_0 - 1) + G_sq*(G_sq*t_0_sq + 4*G*t_0 + 6))*exp_1_sin_0_term - (DM_fr*t_1_sq + 2*DM_sq*(G_sq*t_1_sq + 2*G*t_1 - 1) + G_sq*(G_sq*t_1_sq + 4*G*t_1 + 6))*exp_0_sin_1_term) + (DM_fr*t_0*(G*t_0 - 2.) + 2*DM_sq*G*(G_sq*t_0_sq - 3.) + G_cub*(G_sq*t_0_sq + 2*G*t_0 + 2.))*exp_1_cos_0_term - (DM_fr*t_1*(G*t_1 - 2.) + 2*DM_sq*G*(G_sq*t_1_sq - 3.) + G_cub*(G_sq*t_1_sq + 2*G*t_1 + 2.))*exp_0_cos_1_term) 
    + d*(DM*((DM_sx*t_0_cub + 3*DM_fr*t_0*(G_sq*t_0_sq + 2*G*t_0 - 2.) + 3*DM_sq*G*(G_cub*t_0_cub + 4*G_sq*t_0_sq + 4*G*t_0 - 8) + G_cub*(G_cub*t_0_cub + 6*G_sq*t_0_sq + 18*G*t_0 + 24.))*exp_1_sin_0_term - (DM_sx*t_1_cub + 3*DM_fr*t_1*(G_sq*t_1_sq + 2*G*t_1 - 2.) + 3*DM_sq*G*(G_cub*t_1_cub + 4*G_sq*t_1_sq + 4*G*t_1 - 8) + G_cub*(G_cub*t_1_cub + 6*G_sq*t_1_sq + 18*G*t_1 + 24.))*exp_0_sin_1_term) + (DM_sx*t_0_sq*(G*t_0 - 3.) + 3*DM_fr*(G_cub*t_0_cub - G_sq*t_0_sq - 6*G*t_0 + 2.) + 3*DM_sq*G_sq*(G_cub*t_0_cub + G_sq*t_0_sq - 4*G*t_0 - 12.) + G_fr*(G_cub*t_0_cub + 3*G_sq*t_0_sq + 6*G*t_0 + 6))*exp_1_cos_0_term - (DM_sx*t_1_sq*(G*t_1 - 3.) + 3*DM_fr*(G_cub*t_1_cub - G_sq*t_1_sq - 6*G*t_1 + 2.) + 3*DM_sq*G_sq*(G_cub*t_1_cub + G_sq*t_1_sq - 4*G*t_1 - 12.) + G_fr*(G_cub*t_1_cub + 3*G_sq*t_1_sq + 6*G*t_1 + 6))*exp_0_cos_1_term))*sqrt(2.)*sqrt(delta_t)*exp(-G*(t_0 + t_1) + 0.5*delta_t_sq*(-DM_sq + G_sq))/pow(DM_sq + G_sq, 4.);
}

__device__ double get_int_td_spline(double delta_t,double G,double DM,double DG,double a,double b,double c,double d,double t_0,double t_1)
{
    double G_sq = G*G;
    double G_cub = G_sq*G;
    double G_fr = G_sq*G_sq;
    double G_fv = G_cub*G_sq;
    double DM_sq = DM*DM;
    double DM_fr = DM_sq*DM_sq;
    double DM_sx = DM_fr*DM_sq;
    double delta_t_sq = delta_t*delta_t;
    double t_0_sq = t_0*t_0;
    double t_0_cub = t_0_sq*t_0;
    double t_1_sq = t_1*t_1;
    double t_1_cub = t_1_sq*t_1;
    double exp_0_sin_1_term = exp(G*t_0)*sin(DM*G*delta_t_sq - DM*t_1);
    double exp_1_sin_0_term = exp(G*t_1)*sin(DM*G*delta_t_sq - DM*t_0);
    double exp_0_cos_1_term = exp(G*t_0)*cos(DM*G*delta_t_sq - DM*t_1);
    double exp_1_cos_0_term = exp(G*t_1)*cos(DM*G*delta_t_sq - DM*t_0);
    
    
    return -(a*pow(DM_sq + G_sq, 3.)*(DM*exp_0_cos_1_term - DM*exp_1_cos_0_term - G*exp_0_sin_1_term + G*exp_1_sin_0_term) 
    + b*pow(DM_sq + G_sq, 2.)*(DM_sq*G*t_0*exp_1_sin_0_term - DM_sq*G*t_1*exp_0_sin_1_term + DM_sq*exp_0_sin_1_term - DM_sq*exp_1_sin_0_term - DM*(DM_sq*t_0 + G_sq*t_0 + 2*G)*exp_1_cos_0_term + DM*(DM_sq*t_1 + G_sq*t_1 + 2*G)*exp_0_cos_1_term + G_cub*t_0*exp_1_sin_0_term - G_cub*t_1*exp_0_sin_1_term - G_sq*exp_0_sin_1_term + G_sq*exp_1_sin_0_term) 
    + c*(DM_sq + G_sq)*(DM_fr*G*t_0_sq*exp_1_sin_0_term - DM_fr*G*t_1_sq*exp_0_sin_1_term - 2*DM_fr*t_0*exp_1_sin_0_term + 2*DM_fr*t_1*exp_0_sin_1_term + 2*DM_sq*G_cub*t_0_sq*exp_1_sin_0_term - 2*DM_sq*G_cub*t_1_sq*exp_0_sin_1_term + 6*DM_sq*G*exp_0_sin_1_term - 6*DM_sq*G*exp_1_sin_0_term - DM*(DM_fr*t_0_sq + 2*DM_sq*(G_sq*t_0_sq + 2*G*t_0 - 1) + G_sq*(G_sq*t_0_sq + 4*G*t_0 + 6))*exp_1_cos_0_term + DM*(DM_fr*t_1_sq + 2*DM_sq*(G_sq*t_1_sq + 2*G*t_1 - 1) + G_sq*(G_sq*t_1_sq + 4*G*t_1 + 6))*exp_0_cos_1_term + G_fv*t_0_sq*exp_1_sin_0_term - G_fv*t_1_sq*exp_0_sin_1_term + 2*G_fr*t_0*exp_1_sin_0_term - 2*G_fr*t_1*exp_0_sin_1_term - 2*G_cub*exp_0_sin_1_term + 2*G_cub*exp_1_sin_0_term) 
    + d*(DM_sx*G*t_0_cub*exp_1_sin_0_term - DM_sx*G*t_1_cub*exp_0_sin_1_term - 3*DM_sx*t_0_sq*exp_1_sin_0_term + 3*DM_sx*t_1_sq*exp_0_sin_1_term + 3*DM_fr*G_cub*t_0_cub*exp_1_sin_0_term - 3*DM_fr*G_cub*t_1_cub*exp_0_sin_1_term - 3*DM_fr*G_sq*t_0_sq*exp_1_sin_0_term + 3*DM_fr*G_sq*t_1_sq*exp_0_sin_1_term - 18*DM_fr*G*t_0*exp_1_sin_0_term + 18*DM_fr*G*t_1*exp_0_sin_1_term - 6*DM_fr*exp_0_sin_1_term + 6*DM_fr*exp_1_sin_0_term + 3*DM_sq*G_fv*t_0_cub*exp_1_sin_0_term - 3*DM_sq*G_fv*t_1_cub*exp_0_sin_1_term + 3*DM_sq*G_fr*t_0_sq*exp_1_sin_0_term - 3*DM_sq*G_fr*t_1_sq*exp_0_sin_1_term - 12*DM_sq*G_cub*t_0*exp_1_sin_0_term + 12*DM_sq*G_cub*t_1*exp_0_sin_1_term + 36*DM_sq*G_sq*exp_0_sin_1_term - 36*DM_sq*G_sq*exp_1_sin_0_term - DM*(DM_sx*t_0_cub + 3*DM_fr*t_0*(G_sq*t_0_sq + 2*G*t_0 - 2.) + 3*DM_sq*G*(G_cub*t_0_cub + 4*G_sq*t_0_sq + 4*G*t_0 - 8) + G_cub*(G_cub*t_0_cub + 6*G_sq*t_0_sq + 18*G*t_0 + 24.))*exp_1_cos_0_term + DM*(DM_sx*t_1_cub + 3*DM_fr*t_1*(G_sq*t_1_sq + 2*G*t_1 - 2.) + 3*DM_sq*G*(G_cub*t_1_cub + 4*G_sq*t_1_sq + 4*G*t_1 - 8) + G_cub*(G_cub*t_1_cub + 6*G_sq*t_1_sq + 18*G*t_1 + 24.))*exp_0_cos_1_term + pow(G, 7)*t_0_cub*exp_1_sin_0_term - pow(G, 7)*t_1_cub*exp_0_sin_1_term + 3*pow(G, 6)*t_0_sq*exp_1_sin_0_term - 3*pow(G, 6)*t_1_sq*exp_0_sin_1_term + 6*G_fv*t_0*exp_1_sin_0_term - 6*G_fv*t_1*exp_0_sin_1_term - 6*G_fr*exp_0_sin_1_term + 6*G_fr*exp_1_sin_0_term))
    *sqrt(2.)*sqrt(delta_t)*exp(-G*(t_0 + t_1) + 0.5*delta_t_sq*(-DM_sq + G_sq))/pow(DM_sq + G_sq, 4.);
}

__device__ double Factorial(int n) 
{
   if(n <= 0) 
    return 1.;
   
   double x = 1;
   int b = 0;
   do {
      b++;
      x *= b;
   } while(b!=n);
   
   return x;
}

//Legendre polynomianls up to l = 2
__device__ double P_lm(int l, int m, double cos_psi)
{
    if(l == 0 && m == 0)
    {
        return 1.;
    }
    else if(l == 1 && m == 0)
    {
        return cos_psi;
    }
    else if(l == 2 && m == 0)
    {
        return 0.5*(3.*cos_psi*cos_psi - 1.);
    }
    else if(l == 2 && (m == 1 || m == -1))
    {
        return -3.*cos_psi*sqrt(1.-cos_psi*cos_psi);
    }
    else if(l == 2 && (m == 2 || m == -2))
    {
        return 3.*cos_psi*(1.-cos_psi*cos_psi);
    }
    else
        printf("ATTENTION: Legendre polynomial index l,m is out of the range of this function. Check code.");
    
    return 0.;
}

//Spherical harmonics up to l = 2
__device__ double Y_lm(int l, int m, double cos_theta, double phi)
{
    if(m == 0)
    {
        return sqrt((2*l + 1)/(4.*M_PI))*P_lm(l, m, cos_theta);
    }
    else if(m > 0)
    {
        return pow(-1.,m)*sqrt(2.)*sqrt((2*l + 1)/(4.*M_PI))*(sqrt(Factorial(l-m))/sqrt(Factorial(l+m)))*P_lm(l, m, cos_theta)*cos(m*phi);
    }
    else
    {
        return pow(-1.,m)*sqrt(2.)*sqrt((2*l + 1)/(4.*M_PI))*(sqrt(Factorial(l-(-1.*m)))/sqrt(Factorial(l-1.*m)))*P_lm(l, -1.*m, cos_theta)*sin(-1.*m*phi);
    }

    return 0.;
}

__device__ double ang_eff(double helcosthetaK, double helcosthetaL, double helphi, double *moments)
{
    double eff = 0.;
    
    eff += moments[0]*P_lm(0, 0, helcosthetaK)*Y_lm(0, 0, helcosthetaL, helphi);
    eff += moments[1]*P_lm(0, 0, helcosthetaK)*Y_lm(2, 0, helcosthetaL, helphi);
    eff += moments[2]*P_lm(0, 0, helcosthetaK)*Y_lm(2, 2, helcosthetaL, helphi);
    eff += moments[3]*P_lm(0, 0, helcosthetaK)*Y_lm(2, 1, helcosthetaL, helphi);
    eff += moments[4]*P_lm(0, 0, helcosthetaK)*Y_lm(2, -1, helcosthetaL, helphi);
    eff += moments[5]*P_lm(0, 0, helcosthetaK)*Y_lm(2, -2, helcosthetaL, helphi);
    eff += moments[6]*P_lm(1, 0, helcosthetaK)*Y_lm(0, 0, helcosthetaL, helphi);
    eff += moments[7]*P_lm(1, 0, helcosthetaK)*Y_lm(2, 1, helcosthetaL, helphi);
    eff += moments[8]*P_lm(1, 0, helcosthetaK)*Y_lm(2, -1, helcosthetaL, helphi);
    eff += moments[9]*P_lm(2, 0, helcosthetaK)*Y_lm(0, 0, helcosthetaL, helphi);
    
    return eff;
}

__device__ pycuda::complex<double> faddeeva(pycuda::complex<double> z)//, double t) 
{
   double in_real = pycuda::real(z);
   double in_imag = pycuda::imag(z);
   int n, nc, nu;
   double h, q, Saux, Sx, Sy, Tn, Tx, Ty, Wx, Wy, xh, xl, x, yh, y;
   double Rx [33];
   double Ry [33];

   x = fabs(in_real);
   y = fabs(in_imag);

   if (y < yLim && x < xLim) {
      q = (1.0 - y / yLim) * sqrt(1.0 - (x / xLim) * (x / xLim));
      h  = 1.0 / (3.2 * q);
      nc = 7 + int(23.0 * q);
//       xl = pow(h, double(1 - nc));
      double h_inv = 1./h;
      xl = h_inv;
      for(int i = 1; i < nc-1; i++)
          xl *= h_inv;
      
      xh = y + 0.5 / h;
      yh = x;
      nu = 10 + int(21.0 * q);
      Rx[nu] = 0.;
      Ry[nu] = 0.;
      for (n = nu; n > 0; n--){
         Tx = xh + n * Rx[n];
         Ty = yh - n * Ry[n];
         Tn = Tx*Tx + Ty*Ty;
         Rx[n-1] = 0.5 * Tx / Tn;
         Ry[n-1] = 0.5 * Ty / Tn;
         }
      Sx = 0.;
      Sy = 0.;
      for (n = nc; n>0; n--){
         Saux = Sx + xl;
         Sx = Rx[n-1] * Saux - Ry[n-1] * Sy;
         Sy = Rx[n-1] * Sy + Ry[n-1] * Saux;
         xl = h * xl;
      };
      Wx = errf_const * Sx;
      Wy = errf_const * Sy;
   }
   else {
      xh = y;
      yh = x;
      Rx[0] = 0.;
      Ry[0] = 0.;
      for (n = 9; n>0; n--){
         Tx = xh + n * Rx[0];
         Ty = yh - n * Ry[0];
         Tn = Tx * Tx + Ty * Ty;
         Rx[0] = 0.5 * Tx / Tn;
         Ry[0] = 0.5 * Ty / Tn;
      };
      Wx = errf_const * Rx[0];
      Wy = errf_const * Ry[0];
   }

   if (y == 0.) {
      Wx = exp(-x * x);
   }
   if (in_imag < 0.) {

      double exp_x2_y2 = exp(y * y - x * x);
      Wx =   2.0 * exp_x2_y2 * cos(2.0 * x * y) - Wx;
      Wy = - 2.0 * exp_x2_y2 * sin(2.0 * x * y) - Wy;
      if (in_real > 0.) {
         Wy = -Wy;
      }
   }
   else if (in_real < 0.) {
      Wy = -Wy;
   }

   return pycuda::complex<double>(Wx,Wy);
}

__device__ double conv_exp_single(double t, double G, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    if(t>sigma_t_threshold*sigma_t){
        double f = 2.*(sqrt(0.5*M_PI)); 
                   
       return f*exp(-G*t+0.5*G*G*sigma_t_sq);
    }
    else
        return sqrt(0.5*M_PI)*exp(-G*t+0.5*G*sigma_t_sq)*(1.+erf((t-G*sigma_t_sq)/(sigma_t*sqrt(2.)))); 
}

__device__ double conv_exp_cos_single(double t, double G, double Dm, double sigma_t) 
{   
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>sigma_t_threshold*sigma_t){        
        double f  = 2.*(sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*cos(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::real(faddeeva(z));
    }
}

__device__ double conv_exp_sin_single(double t, double G, double Dm, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>sigma_t_threshold*sigma_t){
        double f  = 2.*(sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*sin(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return -sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::imag(faddeeva(z));
    }
}

__device__ double conv_cosh_single(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp_single(t,G_m_hDG,sigma_t)+conv_exp_single(t,G_p_hDG,sigma_t));
}

__device__ double conv_sinh_single(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp_single(t,G_m_hDG,sigma_t)-conv_exp_single(t,G_p_hDG,sigma_t));
}

__device__ double conv_exp_cos(double t, double G, double Dm, double delta_t_1, double delta_t_2, double f_sigma_t) 
{   
    return f_sigma_t*conv_exp_cos_single(t, G, Dm, delta_t_1)
    + (1.-f_sigma_t)*conv_exp_cos_single(t, G, Dm, delta_t_2);
}

__device__ double conv_exp_sin(double t, double G, double Dm, double delta_t_1, double delta_t_2, double f_sigma_t) 
{
    return f_sigma_t*conv_exp_sin_single(t, G, Dm, delta_t_1)
    + (1.-f_sigma_t)*conv_exp_sin_single(t, G, Dm, delta_t_2);
}

__device__ double conv_cosh(double t, double G, double DG, double delta_t_1, double delta_t_2, double f_sigma_t) 
{
    return f_sigma_t*conv_cosh_single(t, G, DG, delta_t_1) 
    + (1.-f_sigma_t)*conv_cosh_single(t, G, DG, delta_t_2);
}

__device__ double conv_sinh(double t, double G, double DG, double delta_t_1, double delta_t_2, double f_sigma_t) 
{
    return f_sigma_t*conv_sinh_single(t, G, DG, delta_t_1) 
    + (1.-f_sigma_t)*conv_sinh_single(t, G, DG, delta_t_2);
}

__device__ pycuda::complex<double> conv_exp_VC(double t, double Gamma, double omega, double sigma_t) 
{
    pycuda::complex<double> I(0,1);
    double sigma_t_sq = sigma_t*sigma_t;
    
    if(t>sigma_t_threshold*sigma_t)                   
        return 2.*(sqrt(0.5*M_PI))*exp(-Gamma*t+0.5*Gamma*Gamma*sigma_t_sq-0.5*omega*omega*sigma_t_sq)
                                  *(cos(omega*(t-Gamma*sigma_t_sq)) + I*sin(omega*(t-Gamma*sigma_t_sq)));
    else
    {
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*Gamma) - omega*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        pycuda::complex<double> fad = faddeeva(z);
        
        return sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*(pycuda::real(fad) - I*pycuda::imag(fad));
    }
}

__device__ int get_spline_bin(double time, double *spline_knots, int spline_Nknots)
{

// for(int i =0; i< spline_Nknots; i++)
//     printf("spline_knots[%d] %lf \n ",i, spline_knots[i]);
    int i = 0;
    
    while(i < spline_Nknots)//(sizeof(spline_t_2016_unbiased)/sizeof(*spline_t_2016_unbiased)))
        {
            if(time < spline_knots[i])
                break;
            i++;
        }
     
//      printf("bin %d\n", i-1);
    
    return i-1;
}

__device__ double spline_bin_deg(int ibin, int deg, double *spline_coeffs) 
{ 
   return spline_coeffs[ibin*4+deg];
//     return spline_coeffs[ibin][deg];
}

__device__ double calculate_time_acc(double time, 
                                     double *spline_knots,
                                     double *spline_coeffs, 
                                     int spline_Nknots)
{
   int bin = get_spline_bin(time, spline_knots, spline_Nknots);
    
   double a = spline_bin_deg(bin, 0, spline_coeffs);
   double b = spline_bin_deg(bin, 1, spline_coeffs);
   double c = spline_bin_deg(bin, 2, spline_coeffs);
   double d = spline_bin_deg(bin, 3, spline_coeffs);

//     printf("time %lf, bin %d, a %lf, b %lf, c %lf, d %lf\n", time, bin, a, b, c, d);
    
   double acc = d*time*time*time + c*time*time + b*time + a;
   
   return acc;
}

__device__ double get_time_acc(double time, double *low_time_acc_bins_ul, double *low_time_acc)
{    
    int i;
    for(i = 0; i < time_acc_bins; i++)
    {
        if(time < low_time_acc_bins_ul[i])
            break;
    }
    
    double acc = low_time_acc[i];
    
    return acc;
}

// Flavour tagging calibration B
__device__ double omega(double eta, double p0, double dp0, double p1, double dp1, double p2, double dp2, double eta_bar)
{
    double om = (p0 + 0.5*dp0) + (p1 + 0.5*dp1)*(eta - eta_bar) + (p2 + 0.5*dp2)*(eta - eta_bar)*(eta - eta_bar);
    
    return om;    
}

// Flavour tagging calibration Bbar
__device__ double omega_bar(double eta, double p0, double dp0, double p1, double dp1, double p2, double dp2, double eta_bar)
{
    double om_bar = (p0 - 0.5*dp0) + (p1 - 0.5*dp1)*(eta - eta_bar) + (p2 - 0.5*dp2)*(eta - eta_bar)*(eta - eta_bar);
    
    return om_bar;  
}

__device__ void fix_tagging_pars(double tagging_pars[3])
{
    double omega = tagging_pars[0];
    double omega_bar = tagging_pars[1];
    
    /*if(omega > 0.5 || omega_bar > 0.5)
    {
        tagging_pars[0] = 0.5; 
        tagging_pars[1] = 0.5;
        tagging_pars[2] = 0.;
    }
    else */if(omega < 0. || omega_bar < 0.)
    {
        tagging_pars[0] = 0.; 
        tagging_pars[1] = 0.; 
    }
}

__device__ pycuda::complex<double> Kn(pycuda::complex<double> z, int n) 
{
   if (n == 0) 
   {
       return 1./(2.*z);
   }
   else if (n == 1) 
   {
       return 1./(2.*z*z);
   }
   else if (n == 2) 
   {
       return 1./z*(1.+1./(z*z));
   }
   else if (n == 3) 
   {
       return 3./(z*z)*(1.+1./(z*z));
   }
    
   return pycuda::complex<double>(0.,0.);

 }

__device__ pycuda::complex<double> Mn_x(double x, int n, double t, double delta_t, double Gamma, double omega) 
{
   pycuda::complex<double> conv_term = conv_exp_VC(t, Gamma, omega, delta_t)/(sqrt(0.5*M_PI));
    
   if (n == 0) 
   {
       return pycuda::complex<double>(erf(x),0.)-conv_term;
   }
   else if (n == 1) 
   {
       return 2.*(-pycuda::complex<double>(sqrt(1./M_PI)*exp(-x*x),0.)-x*conv_term);
   }
   else if (n == 2) 
   {
       return 2.*(-2.*x*exp(-x*x)*pycuda::complex<double>(sqrt(1./M_PI),0.)-(2.*x*x-1.)*conv_term);
   }
   else if (n == 3) 
   {
       return 4.*(-(2.*x*x-1.)*exp(-x*x)*pycuda::complex<double>(sqrt(1./M_PI),0.)-x*(2.*x*x-3.)*conv_term);
   }
   
   return pycuda::complex<double>(0.,0.);
}

__device__ void calculate_integral_terms(double time_terms[4], double delta_t, double G, double DG, double DM, double *spline_knots, double *spline_coeffs, const int spline_Nknots, double t_offset) 
{
//    static const int spline_bins = 7;//spline_Nknots - 1;
   // Convert spline knots to x coordinates
   double spline_x[spl_bins+1];
   
   double sigma_t_sqrt2_inv = 1./(sqrt(2.)*delta_t);
      
   for(int i = 0; i < spl_bins+1; i++)
   {
//          printf("Offset %lf spline_knots[i] %lf \n ", t_offset, spline_knots[i]);
       spline_x[i] = (spline_knots[i] - t_offset)*sigma_t_sqrt2_inv;
//         if((spline_knots[i] - t_offset) < 0.3 || (spline_knots[i] - t_offset) >15.)
//          printf("Offset %lf spline_knots[i] %lf spline_x[i] %lf \n ", t_offset, spline_knots[i], spline_x[i] );
   }
   
   // Fill Sjk matrix
   //TODO speed to be gained here - Sjk is constant
   double S_matrix[spl_bins][4][4];
    
   for (int ibin=0; ibin < spl_bins; ++ibin) 
   {
       for (int i=0; i<4; ++i) 
        {
            for (int j=0; j<4; ++j) 
            {               
                if(i+j < 4)
                {
                    S_matrix[ibin][i][j] = spline_bin_deg(ibin,i+j,spline_coeffs)*Factorial(i+j)/Factorial(j)/Factorial(i)/pow(2.,i+j);
                }
                else
                {
                    S_matrix[ibin][i][j] = 0.;
                }
                
            }
        }
   }
   
   // Fill Kn
   //TODO speed to be gained here - only need to calculate this once per minimization
   pycuda::complex<double> z1_hyper_m = delta_t*pycuda::complex<double>(G-0.5*DG,0.)/sqrt(2.);
   pycuda::complex<double> z1_hyper_p = delta_t*pycuda::complex<double>(G+0.5*DG,0.)/sqrt(2.);
   pycuda::complex<double> z1_trigo   = delta_t*pycuda::complex<double>(G,-DM)/sqrt(2.);
   
   pycuda::complex<double> Kn_hyper_m_vector[4];
   pycuda::complex<double> Kn_hyper_p_vector[4];
   pycuda::complex<double> Kn_trigo_vector[4];
    
   for (int j=0; j<4; ++j) 
   {
       Kn_hyper_m_vector[j] = Kn(z1_hyper_m,j);
       Kn_hyper_p_vector[j] = Kn(z1_hyper_p,j);
       Kn_trigo_vector[j]   = Kn(z1_trigo,j);
   } 
   
   // Fill Mn
   pycuda::complex<double> Mn_hyper_m_matrix[spl_bins+1][4];
   pycuda::complex<double> Mn_hyper_p_matrix[spl_bins+1][4];
   pycuda::complex<double> Mn_trigo_matrix[spl_bins+1][4];
   
   for (int j=0; j<4; ++j) 
    {
        for(int ibin=0; ibin < spl_bins+1; ++ibin) 
            {               
                Mn_hyper_m_matrix[ibin][j]  = Mn_x(spline_x[ibin], j, spline_knots[ibin]-t_offset, delta_t, G-0.5*DG, 0.);
                Mn_hyper_p_matrix[ibin][j]  = Mn_x(spline_x[ibin], j, spline_knots[ibin]-t_offset, delta_t, G+0.5*DG, 0.);
                Mn_trigo_matrix[ibin][j]    = Mn_x(spline_x[ibin], j, spline_knots[ibin]-t_offset, delta_t, G, DM);
            }
    }
  
  // Fill the delta factors to multiply by the integrals
  double delta_t_fact[4];
  
  for (int i=0; i<4; ++i) 
  {
      delta_t_fact[i] = pow(delta_t*sqrt(2.), i+1)/sqrt(2.);
  }
  
  // Integral calculation for cosh, sinh, cos, sin terms
   double integral_conv_exp_hyper_m = 0;
   double integral_conv_exp_hyper_p = 0;
   pycuda::complex<double> integral_conv_exp_trigo = pycuda::complex<double>(0.,0.);
   
   for (int ibin=0; ibin < spl_bins; ++ibin) 
    {
       for (int j=0; j<=3; ++j) 
         {  
             for (int k=0; k<=3-j; ++k) 
            {                
                integral_conv_exp_hyper_m += pycuda::real(S_matrix[ibin][j][k]
                                                           *(Mn_hyper_m_matrix[ibin+1][j] - Mn_hyper_m_matrix[ibin][j])
                                                           *Kn_hyper_m_vector[k])
                                                           *delta_t_fact[j+k];
                
                integral_conv_exp_hyper_p += pycuda::real(S_matrix[ibin][j][k]
                                                          *(Mn_hyper_p_matrix[ibin+1][j] - Mn_hyper_p_matrix[ibin][j])
                                                          *Kn_hyper_p_vector[k])
                                                          *delta_t_fact[j+k];
                
                integral_conv_exp_trigo   +=  S_matrix[ibin][j][k]
                                                  *(Mn_trigo_matrix[ibin+1][j] - Mn_trigo_matrix[ibin][j])
                                                  *Kn_trigo_vector[k]
                                                  *delta_t_fact[j+k];
            }
        }
//                 
   }
   
   //0:cosh, 1:sinh, 2:cos, 3:sin
   time_terms[0] = 0.5*(integral_conv_exp_hyper_m + integral_conv_exp_hyper_p);
   time_terms[1] = 0.5*(integral_conv_exp_hyper_m - integral_conv_exp_hyper_p);
   time_terms[2] = pycuda::real(integral_conv_exp_trigo);
   time_terms[3] = pycuda::imag(integral_conv_exp_trigo);
}

// Decay time resolution calibration
__device__ double delta(double sigma_t, double sigma_t_a, double sigma_t_b, double sigma_t_c)
{
    return sigma_t_a*sigma_t*sigma_t + sigma_t_b*sigma_t + sigma_t_c;
}

__device__ double delta_1(double sigma_t, double f_sigma_t, double r_offset_pr, double r_offset_sc, double r_slope_pr, double r_slope_sc, double sigma_t_bar)
{    
    return -sqrt(f_sigma_t/(1.-f_sigma_t))*(r_offset_sc+r_slope_sc*(sigma_t-sigma_t_bar))*sigma_t
                                         + (r_offset_pr+r_slope_pr*(sigma_t-sigma_t_bar))*sigma_t;
}


__device__ double delta_2(double sigma_t, double f_sigma_t, double r_offset_pr, double r_offset_sc, double r_slope_pr, double r_slope_sc, double sigma_t_bar)
{
    return sqrt((1.-f_sigma_t)/f_sigma_t)*(r_offset_sc+r_slope_sc*(sigma_t-sigma_t_bar))*sigma_t
                                        + (r_offset_pr+r_slope_pr*(sigma_t-sigma_t_bar))*sigma_t;
}


__device__ double Nk(double A_0_abs, 
                     double A_S_abs,
                     double A_pa_abs, 
                     double A_pe_abs,
                     double CSP,
                     int k)
{
    double nk;
    switch(k) {
        case 1:  nk = A_0_abs*A_0_abs;
                 break;
        case 2:  nk = A_pa_abs*A_pa_abs;
                 break;
        case 3:  nk = A_pe_abs*A_pe_abs;
                 break;
        case 4:  nk = A_pe_abs*A_pa_abs;
                 break;
        case 5:  nk = A_0_abs*A_pa_abs;
                 break;
        case 6:  nk = A_0_abs*A_pe_abs;
                 break;
        case 7:  nk = A_S_abs*A_S_abs;
                 break;
        case 8:  nk = CSP*A_S_abs*A_pa_abs;
                 break;
        case 9:  nk = CSP*A_S_abs*A_pe_abs;
                 break;
        case 10: nk = CSP*A_S_abs*A_0_abs;
                 break;
        default: printf("Wrong k index in nk, please check code %d\\n", k);
                 return 0.;
    }
    return nk;
}

__device__ double fk(double helcosthetaK, 
                     double helcosthetaL, 
                     double helphi,
                     int k)
{
    double helsinthetaK = sqrt(1. - helcosthetaK*helcosthetaK);
    double helsinthetaL = sqrt(1. - helcosthetaL*helcosthetaL);
//     helphi -= M_PI;
    double helsinphi = sin(-helphi);
    double helcosphi = cos(-helphi);
    
    double fk;
    switch(k) {
        case 1:  fk = helcosthetaK*helcosthetaK*helsinthetaL*helsinthetaL;
                 break;
        case 2:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helcosphi*helcosphi*helsinthetaL*helsinthetaL);
                 break;
        case 3:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helsinphi*helsinphi*helsinthetaL*helsinthetaL);
                 break;
        case 4:  fk = helsinthetaK*helsinthetaK*helsinthetaL*helsinthetaL*helsinphi*helcosphi;
                 break;
        case 5:  fk = sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helcosphi;
                 break;
        case 6:  fk = -sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helsinphi;
                 break;
        case 7:  fk = helsinthetaL*helsinthetaL/3.;
                 break;
        case 8:  fk = 2.*helsinthetaK*helsinthetaL*helcosthetaL*helcosphi/sqrt(6.);
                 break;
        case 9:  fk = -2.*helsinthetaK*helsinthetaL*helcosthetaL*helsinphi/sqrt(6.);
                 break;
        case 10: fk = 2.*helcosthetaK*helsinthetaL*helsinthetaL/sqrt(3.);
                 break;
        default: printf("Wrong k index in fk, please check code %d\\n", k);
                 return 0.;
    }
    return fk;
}

__device__ double ak(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double ak;
    switch(k) {
        case 1:  ak = 0.5*(1.+lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ak = 0.5*(1.+lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ak = 0.5*(1.+lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ak = 0.5*(sin(delta_pe-delta_pa) - lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ak = 0.5*(cos(delta_0-delta_pa) + lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ak = -0.5*(sin(delta_0-delta_pe) - lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ak = 0.5*(1.+lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ak = 0.5*(cos(delta_S-delta_pa) - lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ak = -0.5*(sin(delta_S-delta_pe) + lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ak = 0.5*(cos(delta_S-delta_0) - lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ak, please check code %d\\n", k);
                 return 0.;
    }
    return ak;
}

__device__ double bk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double bk;
    switch(k) {
        case 1:  bk = -lambda_0_abs*cos(phis_0);
                 break;
        case 2:  bk = -lambda_pa_abs*cos(phis_pa);
                 break;
        case 3:  bk = lambda_pe_abs*cos(phis_pe);
                 break;
        case 4:  bk = 0.5*(lambda_pe_abs*sin(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*sin(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  bk = -0.5*(lambda_0_abs*cos(delta_0-delta_pa-phis_0) + lambda_pa_abs*cos(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  bk = 0.5*(lambda_0_abs*sin(delta_0-delta_pe-phis_0) + lambda_pe_abs*sin(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  bk = lambda_S_abs*cos(phis_S);
                 break;
        case 8:  bk = 0.5*(lambda_S_abs*cos(delta_S-delta_pa-phis_S) - lambda_pa_abs*cos(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  bk = -0.5*(lambda_S_abs*sin(delta_S-delta_pe-phis_S) - lambda_pe_abs*sin(delta_pe-delta_S-phis_pe));
                 break;
        case 10: bk = 0.5*(lambda_S_abs*cos(delta_S-delta_0-phis_S) - lambda_0_abs*cos(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in bk, please check code %d\\n", k);
                 return 0.;
    }
    return bk;
}
    
__device__ double ck(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double ck;
    switch(k) {
        case 1:  ck = 0.5*(1.-lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ck = 0.5*(1.-lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ck = 0.5*(1.-lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ck = 0.5*(sin(delta_pe-delta_pa) + lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ck = 0.5*(cos(delta_0-delta_pa) - lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ck = -0.5*(sin(delta_0-delta_pe) + lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ck = 0.5*(1.-lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ck = 0.5*(cos(delta_S-delta_pa) + lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ck = -0.5*(sin(delta_S-delta_pe) - lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ck = 0.5*(cos(delta_S-delta_0) + lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ck, please check code %d\\n", k);
                 return 0.;
    }
    return ck;
}

__device__ double dk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double dk;
    switch(k) {
        case 1:  dk = lambda_0_abs*sin(phis_0);
                 break;
        case 2:  dk = lambda_pa_abs*sin(phis_pa);
                 break;
        case 3:  dk = -lambda_pe_abs*sin(phis_pe);
                 break;
        case 4:  dk = -0.5*(lambda_pe_abs*cos(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*cos(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  dk = -0.5*(lambda_0_abs*sin(delta_0-delta_pa-phis_0) + lambda_pa_abs*sin(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  dk = -0.5*(lambda_0_abs*cos(delta_0-delta_pe-phis_0) + lambda_pe_abs*cos(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  dk = -lambda_S_abs*sin(phis_S);
                 break;
        case 8:  dk = 0.5*(lambda_S_abs*sin(delta_S-delta_pa-phis_S) - lambda_pa_abs*sin(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  dk = -0.5*(-lambda_S_abs*cos(delta_S-delta_pe-phis_S) + lambda_pe_abs*cos(delta_pe-delta_S-phis_pe));
                 break;
        case 10: dk = 0.5*(lambda_S_abs*sin(delta_S-delta_0-phis_S) - lambda_0_abs*sin(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in dk, please check code %d\\n", k);
    
             return 0.;
    }
    return dk;
}


__device__ void integral4pitime(double integral[2], double vNk[10], double vak[10],double vbk[10],double vck[10],double vdk[10], 
                                double normweights[10],
                                double G, double DG, double DM, double t_ll, double delta_t, double t_mu, double *low_time_acc_bins_ul, double *low_time_acc)
{
  //double Nk, ak, bk, ck, dk;
  
  double conv_sin_term_t_0, conv_cos_term_t_0, conv_sinh_term_t_0, conv_cosh_term_t_0;
  double conv_sin_term_t_1, conv_cos_term_t_1, conv_sinh_term_t_1, conv_cosh_term_t_1;
  double int_ta, int_tb, int_tc, int_td;

  pycuda::complex<double> hyper_p = conv_exp_VC(t_ll-t_mu, G + 0.5*DG, 0., delta_t);
  pycuda::complex<double> hyper_m = conv_exp_VC(t_ll-t_mu, G - 0.5*DG, 0., delta_t);
  pycuda::complex<double> trig    = conv_exp_VC(t_ll-t_mu, G, DM, delta_t);

  conv_cosh_term_t_0 = pycuda::real(0.5*(hyper_m + hyper_p));
  conv_sinh_term_t_0 = pycuda::real(0.5*(hyper_m - hyper_p));
  conv_cos_term_t_0  = pycuda::real(trig);
  conv_sin_term_t_0  = pycuda::imag(trig);
  
  integral[0] = 0.;
  integral[1] = 0.;
  double den1 = -DG*DG + 4*G*G;
  double den2 = DM*DM + G*G;
  
  for(int i=0; i<time_acc_bins; i++)
  {
    double t_1 = low_time_acc_bins_ul[i];
 
    hyper_p = conv_exp_VC(t_1-t_mu, G + 0.5*DG, 0., delta_t);
    hyper_m = conv_exp_VC(t_1-t_mu, G - 0.5*DG, 0., delta_t);
    trig    = conv_exp_VC(t_1-t_mu, G, DM, delta_t);

    conv_cosh_term_t_1 = pycuda::real(0.5*(hyper_m + hyper_p));
    conv_sinh_term_t_1 = pycuda::real(0.5*(hyper_m - hyper_p));
    conv_cos_term_t_1  = pycuda::real(trig);
    conv_sin_term_t_1  = pycuda::imag(trig);
    
    double acc = low_time_acc[i];
            
    int_ta = 2*(DG*conv_sinh_term_t_0 + 2*G*conv_cosh_term_t_0 - DG*conv_sinh_term_t_1 - 2*G*conv_cosh_term_t_1)/den1;
    int_tb = 2*(DG*conv_cosh_term_t_0 + 2*G*conv_sinh_term_t_0 - DG*conv_cosh_term_t_1 - 2*G*conv_sinh_term_t_1)/den1;
    int_tc =   (-DM*conv_sin_term_t_0 + G*conv_cos_term_t_0 + DM*conv_sin_term_t_1 - G*conv_cos_term_t_1)/den2;
    int_td =   (DM*conv_cos_term_t_0 + G*conv_sin_term_t_0 - DM*conv_cos_term_t_1 - G*conv_sin_term_t_1)/den2;
    
    for(int k=0; k<10; k++)
    {
      integral[0] += acc*vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb + vck[k]*int_tc + vdk[k]*int_td);
      integral[1] += acc*vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb - vck[k]*int_tc - vdk[k]*int_td); 
    }
        
    conv_sin_term_t_0  = conv_sin_term_t_1;
    conv_cos_term_t_0  = conv_cos_term_t_1;
    conv_sinh_term_t_0 = conv_sinh_term_t_1;
    conv_cosh_term_t_0 = conv_cosh_term_t_1;
    }       
} 


__device__ void integral4pitime_double(double integral[2], double vNk[10], double vak[10],double vbk[10],double vck[10],double vdk[10], 
                                double normweights[10],
                                double G, double DG, double DM, double t_ll, double delta_t_1, double delta_t_2, double f_sigma_t,
                                double t_mu, double *low_time_acc_bins_ul, double *low_time_acc)
{
  //double Nk, ak, bk, ck, dk;
  
  double conv_sin_term_t_0, conv_cos_term_t_0, conv_sinh_term_t_0, conv_cosh_term_t_0;
  double conv_sin_term_t_1, conv_cos_term_t_1, conv_sinh_term_t_1, conv_cosh_term_t_1;
  double int_ta, int_tb, int_tc, int_td;

//   pycuda::complex<double> hyper_p = f_sigma_t*conv_exp_VC(t_ll-t_mu, G + 0.5*DG, 0., delta_t_1) + (1.-f_sigma_t)*conv_exp_VC(t_ll-t_mu, G + 0.5*DG, 0., delta_t_2);
//   pycuda::complex<double> hyper_m = f_sigma_t*conv_exp_VC(t_ll-t_mu, G - 0.5*DG, 0., delta_t_1) + (1.-f_sigma_t)*conv_exp_VC(t_ll-t_mu, G - 0.5*DG, 0., delta_t_2);
//   pycuda::complex<double> trig    = f_sigma_t*conv_exp_VC(t_ll-t_mu, G, DM, delta_t_1) + (1.-f_sigma_t)*conv_exp_VC(t_ll-t_mu, G, DM, delta_t_2);

//   pycuda::complex<double> hyper_p = f_sigma_t*conv_exp_VC(t_ll-t_mu, G + 0.5*DG, 0., 0.036) + (1.-f_sigma_t)*conv_exp_VC(t_ll-t_mu, G + 0.5*DG, 0., 0.065);
//   pycuda::complex<double> hyper_m = f_sigma_t*conv_exp_VC(t_ll-t_mu, G - 0.5*DG, 0., 0.036) + (1.-f_sigma_t)*conv_exp_VC(t_ll-t_mu, G - 0.5*DG, 0., 0.065);
//   pycuda::complex<double> trig    = f_sigma_t*conv_exp_VC(t_ll-t_mu, G, DM, 0.036) + (1.-f_sigma_t)*conv_exp_VC(t_ll-t_mu, G, DM, 0.065);

//   conv_cosh_term_t_0 = pycuda::real(0.5*(hyper_m + hyper_p));
//   conv_sinh_term_t_0 = pycuda::real(0.5*(hyper_m - hyper_p));
//   conv_cos_term_t_0  = pycuda::real(trig);
//   conv_sin_term_t_0  = pycuda::imag(trig);
  
//   conv_cosh_term_t_0 = conv_cosh(t_ll-t_mu, G, DG, delta_t_1, delta_t_2, f_sigma_t);
//   conv_sinh_term_t_0 = conv_sinh(t_ll-t_mu, G, DG, delta_t_1, delta_t_2, f_sigma_t);
//   conv_cos_term_t_0 = conv_exp_cos(t_ll-t_mu, G, DM, delta_t_1, delta_t_2, f_sigma_t);
//   conv_sin_term_t_0 = conv_exp_sin(t_ll-t_mu, G, DM, delta_t_1, delta_t_2, f_sigma_t);
  
  conv_cosh_term_t_0 = conv_cosh(t_ll-t_mu, G, DG, delta_t_2, delta_t_1, f_sigma_t);
  conv_sinh_term_t_0 = conv_sinh(t_ll-t_mu, G, DG, delta_t_2, delta_t_1, f_sigma_t);
  conv_cos_term_t_0 = conv_exp_cos(t_ll-t_mu, G, DM, delta_t_2, delta_t_1, f_sigma_t);
  conv_sin_term_t_0 = conv_exp_sin(t_ll-t_mu, G, DM, delta_t_2, delta_t_1, f_sigma_t);
    
  integral[0] = 0.;
  integral[1] = 0.;
  
  double den1 = -DG*DG + 4*G*G;
  double den2 = DM*DM + G*G;
  
  for(int i=0; i<time_acc_bins; i++)
  {
    double t_1 = low_time_acc_bins_ul[i];
 
//     hyper_p = f_sigma_t*conv_exp_VC(t_1-t_mu, G + 0.5*DG, 0., delta_t_1) + (1.-f_sigma_t)*conv_exp_VC(t_1-t_mu, G + 0.5*DG, 0., delta_t_2);
//     hyper_m = f_sigma_t*conv_exp_VC(t_1-t_mu, G - 0.5*DG, 0., delta_t_1) + (1.-f_sigma_t)*conv_exp_VC(t_1-t_mu, G - 0.5*DG, 0., delta_t_2);     
//     trig    = f_sigma_t*conv_exp_VC(t_1-t_mu, G, DM, delta_t_1) + (1.-f_sigma_t)*conv_exp_VC(t_1-t_mu, G, DM, delta_t_2);
 
//     hyper_p = f_sigma_t*conv_exp_VC(t_1-t_mu, G + 0.5*DG, 0., 0.036) + (1.-f_sigma_t)*conv_exp_VC(t_1-t_mu, G + 0.5*DG, 0., 0.065);
//     hyper_m = f_sigma_t*conv_exp_VC(t_1-t_mu, G - 0.5*DG, 0., 0.036) + (1.-f_sigma_t)*conv_exp_VC(t_1-t_mu, G - 0.5*DG, 0., 0.065);
//     trig    = f_sigma_t*conv_exp_VC(t_1-t_mu, G, DM, 0.036) + (1.-f_sigma_t)*conv_exp_VC(t_1-t_mu, G, DM, 0.065);

//     conv_cosh_term_t_1 = pycuda::real(0.5*(hyper_m + hyper_p));
//     conv_sinh_term_t_1 = pycuda::real(0.5*(hyper_m - hyper_p));
//     conv_cos_term_t_1  = pycuda::real(trig);
//     conv_sin_term_t_1  = pycuda::imag(trig);
  
    conv_cosh_term_t_1 = conv_cosh(t_1-t_mu, G, DG, delta_t_1, delta_t_2, f_sigma_t);
    conv_sinh_term_t_1 = conv_sinh(t_1-t_mu, G, DG, delta_t_1, delta_t_2, f_sigma_t);
    conv_cos_term_t_1 = conv_exp_cos(t_1-t_mu, G, DM, delta_t_1, delta_t_2, f_sigma_t);
    conv_sin_term_t_1 = conv_exp_sin(t_1-t_mu, G, DM, delta_t_1, delta_t_2, f_sigma_t);
    
    double acc = low_time_acc[i];
            
    int_ta = 2*(DG*conv_sinh_term_t_0 + 2*G*conv_cosh_term_t_0 - DG*conv_sinh_term_t_1 - 2*G*conv_cosh_term_t_1)/den1;
    int_tb = 2*(DG*conv_cosh_term_t_0 + 2*G*conv_sinh_term_t_0 - DG*conv_cosh_term_t_1 - 2*G*conv_sinh_term_t_1)/den1;
    int_tc =   (-DM*conv_sin_term_t_0 + G*conv_cos_term_t_0 + DM*conv_sin_term_t_1 - G*conv_cos_term_t_1)/den2;
    int_td =   (DM*conv_cos_term_t_0 + G*conv_sin_term_t_0 - DM*conv_cos_term_t_1 - G*conv_sin_term_t_1)/den2;
    
    for(int k=0; k<10; k++)
    {
      integral[0] += acc*vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb + vck[k]*int_tc + vdk[k]*int_td);
      integral[1] += acc*vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb - vck[k]*int_tc - vdk[k]*int_td); 
    }
       
    conv_sin_term_t_0  = conv_sin_term_t_1;
    conv_cos_term_t_0  = conv_cos_term_t_1;
    conv_sinh_term_t_0 = conv_sinh_term_t_1;
    conv_cosh_term_t_0 = conv_cosh_term_t_1;
    }       
} 

//This integral works in the limit t>>sigma_t, e.g. t>0.3ps, and should not be used for low times.
__device__ void integral4pitime_spline( double integral[2], double vNk[10], double vak[10],double vbk[10],
                                        double vck[10],double vdk[10], double *normweights, double G, double DG, double DM, 
                                        double delta_t, double t_ll, double t_offset, int spline_Nknots, double *spline_knots, double *spline_coeffs) 
{  
  int bin0 = 0;
  double t_0 = t_ll-t_offset;
  double t_1 = spline_knots[bin0+1]-t_offset;
  int spline_bins = spline_Nknots-1;
  for(int bin = bin0; bin < spline_bins; bin++)
  {
    double a = spline_bin_deg(bin, 0, spline_coeffs);
    double b = spline_bin_deg(bin, 1, spline_coeffs);
    double c = spline_bin_deg(bin, 2, spline_coeffs);
    double d = spline_bin_deg(bin, 3, spline_coeffs);
    
    double int_ta = get_int_ta_spline( delta_t, G, DM, DG, a, b, c, d, t_0, t_1);
    double int_tb = get_int_tb_spline( delta_t, G, DM, DG, a, b, c, d, t_0, t_1);
    double int_tc = get_int_tc_spline( delta_t, G, DM, DG, a, b, c, d, t_0, t_1);
    double int_td = get_int_td_spline( delta_t, G, DM, DG, a, b, c, d, t_0, t_1);
        
    double time_terms[4] = {0., 0., 0., 0.};
    
    for(int k=0; k<10; k++)
    {
        integral[0] += vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb + vck[k]*int_tc + vdk[k]*int_td);
        integral[1] += vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb - vck[k]*int_tc - vdk[k]*int_td); 
    }   
    
    t_0 = spline_knots[bin+1]-t_offset;
    t_1 = spline_knots[bin+2]-t_offset;
  }
} 

//This integral works for all decay times.
__device__ void integral4pitime_full_spline( double integral[2], double vNk[10], double vak[10],double vbk[10],
                                        double vck[10],double vdk[10], double *normweights, double G, double DG, double DM, 
                                        double delta_t, double t_ll, double t_offset, int spline_Nknots, double *spline_knots, double *spline_coeffs) 
{  
    double time_terms[4] = {0., 0., 0., 0.};
    calculate_integral_terms(time_terms, delta_t, G, DG, DM, spline_knots, spline_coeffs, spline_Nknots, t_offset) ;
    
    double int_ta = time_terms[0];
    double int_tb = time_terms[1];
    double int_tc = time_terms[2];
    double int_td = time_terms[3];
    
    for(int k=0; k<10; k++)
    {
        integral[0] += vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb + vck[k]*int_tc + vdk[k]*int_td);
        integral[1] += vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb - vck[k]*int_tc - vdk[k]*int_td); 
    }   
} 

__global__ void DiffRate(double *data, 
                         double *out, 
                         double CSP,
                         double A_0_abs, double A_S_abs, double A_pa_abs, double A_pe_abs,
                         double phis_0, double phis_S, double phis_pa, double phis_pe,
                         double delta_S, double delta_pa, double delta_pe,
                         double l_0_abs, double l_S_abs, double l_pa_abs, double l_pe_abs,
                         double G, double DG, double DM, 
                         double p0_OS, double dp0_OS, double p1_OS, double dp1_OS, double p2_OS, double dp2_OS, double eta_bar_OS,
                         double p0_SSK, double dp0_SSK, double p1_SSK, double dp1_SSK, double eta_bar_SSK,
                         double sigma_t_a, double sigma_t_b, double sigma_t_c, double t_ll,
                         double sigma_t_mu_a, double sigma_t_mu_b, double sigma_t_mu_c,
                         double f_sigma_t, double r_offset_pr, double r_offset_sc, double r_slope_pr, double r_slope_sc, 
                         double sigma_t_bar, double t_res_scale, double t_mu,
                         int spline_Nknots,
                         double *normweights,
                         double *spline_knots,
                         double *spline_coeffs, 
                         double *low_time_acc_bins_ul, double *low_time_acc,
                         int Nevt)
{
int row = threadIdx.x + blockDim.x * blockIdx.x; //ntuple entry

if (row >= Nevt) { 
    return;
}

int i0 = row*13;// general rule for cuda matrices : index = col + row*N; 
int idx = 0 + i0; 
int idy = 1 + i0;
int idz = 2 + i0;
int idt = 3 + i0;
int idsigma_t = 4 + i0;
int idq_OS = 5 + i0;
int idq_SSK = 6 + i0;
int ideta_OS = 7 + i0;
int ideta_SSK = 8 + i0;
int iyear = 12 + i0;

double helcosthetaK = data[idx];
double helcosthetaL = data[idy];
double helphi = data[idz];
double t = data[idt];
double sigma_t = data[idsigma_t];
double q_OS = data[idq_OS];
double q_SSK = data[idq_SSK];
double eta_OS = data[ideta_OS];
double eta_SSK = data[ideta_SSK];
int year = data[iyear];

double delta_0 = 0.;

double pdf_B = 0.;
double pdf_Bbar = 0.;  

double delta_t =  delta(sigma_t, sigma_t_a, sigma_t_b, sigma_t_c);

double delta_t_1 = delta_1(sigma_t, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar);
double delta_t_2 = delta_2(sigma_t, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar);

double omega_OS = omega(eta_OS, p0_OS, dp0_OS, p1_OS, dp1_OS, p2_OS, dp2_OS, eta_bar_OS);
double omega_bar_OS = omega_bar(eta_OS, p0_OS, dp0_OS, p1_OS, dp1_OS, p2_OS, dp2_OS, eta_bar_OS);
double omega_SSK = omega(eta_SSK, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, 0., 0., eta_bar_SSK);
double omega_bar_SSK = omega_bar(eta_SSK, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, 0., 0., eta_bar_SSK);

double tagging_pars_OS[3] = {omega_OS, omega_bar_OS, q_OS};
double tagging_pars_SSK[3] = {omega_SSK, omega_bar_SSK, q_SSK};

// if(year > 2013)
// {
    fix_tagging_pars(tagging_pars_OS);
    fix_tagging_pars(tagging_pars_SSK);

    omega_OS = tagging_pars_OS[0];
    omega_bar_OS = tagging_pars_OS[1];
    omega_SSK = tagging_pars_SSK[0];
    omega_bar_SSK = tagging_pars_SSK[1];

    if((tagging_pars_OS[0] == 0.5 || tagging_pars_OS[1] == 0.5) && (tagging_pars_OS[0] != tagging_pars_OS[1])) 
        printf("OS tag mismatch!!! Check code %lf vs %lf and %lf \n", tagging_pars_OS[0], tagging_pars_OS[1], tagging_pars_OS[2]);
    else
        q_OS = tagging_pars_OS[2];

    if((tagging_pars_SSK[0] == 0.5 || tagging_pars_SSK[1] == 0.5) && (tagging_pars_SSK[0] != tagging_pars_SSK[1])) 
        printf("SSK tag mismatch!!! Check code %lf vs %lf and %lf \n", tagging_pars_SSK[0], tagging_pars_SSK[1], tagging_pars_SSK[2]);
    else
        q_SSK = tagging_pars_SSK[2];
// }
// double t_offset = delta(delta_t, sigma_t_mu_a, sigma_t_mu_b, sigma_t_mu_c);
double t_offset;
pycuda::complex<double> hyper_p, hyper_m, trig;
double ta, tb, tc, td;

if(year > 2013)
{
    t_offset = delta(sigma_t, sigma_t_mu_a, sigma_t_mu_b, sigma_t_mu_c);
    
    hyper_p = conv_exp_VC(t-t_offset, G + 0.5*DG, 0., delta_t);
    hyper_m = conv_exp_VC(t-t_offset, G - 0.5*DG, 0., delta_t);
    trig    = conv_exp_VC(t-t_offset, G, DM, delta_t);
    
    ta = pycuda::real(0.5*(hyper_m + hyper_p));
    tb = pycuda::real(0.5*(hyper_m - hyper_p));
    tc = pycuda::real(trig);
    td = pycuda::imag(trig);
}
else
{
    t_offset = t_mu;
    
//     hyper_p = f_sigma_t*conv_exp_VC(t-t_offset, G + 0.5*DG, 0., delta_t_1) + (1.-f_sigma_t)*conv_exp_VC(t-t_offset, G + 0.5*DG, 0., delta_t_2);
//     hyper_m = f_sigma_t*conv_exp_VC(t-t_offset, G - 0.5*DG, 0., delta_t_1) + (1.-f_sigma_t)*conv_exp_VC(t-t_offset, G - 0.5*DG, 0., delta_t_2);
//     trig    = f_sigma_t*conv_exp_VC(t-t_offset, G, DM, delta_t_1) + (1.-f_sigma_t)*conv_exp_VC(t-t_offset, G, DM, delta_t_2);

    // double ta = conv_cosh(t-t_offset, G, DG, delta_t_1, delta_t_2, f_sigma_t);
    // double tb = conv_sinh(t-t_offset, G, DG, delta_t_1, delta_t_2, f_sigma_t);
    // double tc = conv_exp_cos(t-t_offset, G, DM, delta_t_1, delta_t_2, f_sigma_t);
    // double td = conv_exp_sin(t-t_offset, G, DM, delta_t_1, delta_t_2, f_sigma_t);

    ta = conv_cosh(t-t_offset, G, DG, delta_t_2, delta_t_1, f_sigma_t);
    tb = conv_sinh(t-t_offset, G, DG, delta_t_2, delta_t_1, f_sigma_t);
    tc = conv_exp_cos(t-t_offset, G, DM, delta_t_2, delta_t_1, f_sigma_t);
    td = conv_exp_sin(t-t_offset, G, DM, delta_t_2, delta_t_1, f_sigma_t);
}
// printf("year %d, sigma_t %lf, f_sigma_t %lf, r_offset_pr %lf, r_offset_sc %lf, r_slope_pr %lf, r_slope_sc %lf, sigma_t_bar %lf \n", year, sigma_t, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar);

double vNk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vak[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vbk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vck[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vdk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};

for(int k = 1; k <= 10; k++)
{
    double Nk_term = Nk(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,CSP,k);
    double fk_term = fk(helcosthetaK,helcosthetaL,helphi,k); 
    
    double ak_term = ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);    
    double bk_term = bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    double ck_term = ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    double dk_term = dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    
    double hk_B    = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb + ck_term*tc + dk_term*td);
    double hk_Bbar = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb - ck_term*tc - dk_term*td);
    
    pdf_B    += Nk_term*hk_B*fk_term;            
    pdf_Bbar += Nk_term*hk_Bbar*fk_term;

    vNk[k-1] = 1.*Nk_term;
    vak[k-1] = 1.*ak_term;
    vbk[k-1] = 1.*bk_term;
    vck[k-1] = 1.*ck_term;
    vdk[k-1] = 1.*dk_term;
}

double integral[2] = {0.,0.};

double time_acc = 0.;
 
if(year > 2013)
{    
    // Time acceptance with a spline
    time_acc = calculate_time_acc(t, spline_knots, spline_coeffs, spline_Nknots); 
    integral4pitime_spline(integral, vNk, vak, vbk, vck, vdk, normweights, G, DG, DM, delta_t, t_ll, t_offset, spline_Nknots, spline_knots, spline_coeffs);
    // integral4pitime_spline(integral, vNk, vak, vbk, vck, vdk, normweights, G, DG,DM, delta_t, t_ll, 0, spline_Nknots, spline_knots, spline_coeffs);
    // integral4pitime_full_spline(integral, vNk, vak, vbk, vck, vdk, normweights, G, DG,DM, delta_t, t_ll, t_offset, spline_Nknots, spline_knots, spline_coeffs);
}
else
{
    // Time acceptance with a histogram
    time_acc = get_time_acc(t, low_time_acc_bins_ul, low_time_acc); 
//     integral4pitime(integral, vNk, vak, vbk, vck, vdk, normweights, G, DG,DM, t_ll, delta_t, t_mu, low_time_acc_bins_ul, low_time_acc);
    integral4pitime_double(integral, vNk, vak, vbk, vck, vdk, normweights, G, DG, DM, t_ll, delta_t_1, delta_t_2, f_sigma_t, t_mu, low_time_acc_bins_ul, low_time_acc);
}

// printf("year %d, time_acc %lf, t %lf\n", year, time_acc, t);
// printf("sigma_t %lf delta_t_1 %lf, delta_t_2 %lf\n", sigma_t, delta_t_1, delta_t_2);
    
double int_B = integral[0];
double int_Bbar = integral[1];

// Total PDF
double num = time_acc*((1.+ q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*pdf_B 
                     + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*pdf_Bbar);
 
// Total PDF integral
double den =  ((1.+q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*int_B 
             + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*int_Bbar);

//REMOVE ATTENTION
// double num, den, omega, q;
// if(q_OS == q_SSK)
//     {
//        q = q_OS;
//        omega = (omega_OS*omega_SSK)/(omega_OS*omega_SSK + (1.-omega_OS)*(1.-omega_SSK));
//     }
// else{
//         if(omega_OS < omega_SSK)
//         {
//             q = q_OS;
//             omega = (omega_OS*(1.-omega_SSK))/((1.-omega_OS)*omega_SSK + (1.-omega_SSK)*omega_OS);
//             
//         }
//         else
//         {
//             q = -q_OS;
//             omega = (omega_SSK*(1.-omega_OS))/((1.-omega_OS)*omega_SSK + (1.-omega_SSK)*omega_OS);
//             
//         }
//     }
// 
//     num = time_acc*((1.+q*(1.-2.*omega))*pdf_B + (1.-q*(1.-2.*omega))*pdf_Bbar);
// 
//     den =  (1.+q*(1.-2.*omega))*int_B  + (1.-q*(1.-2.*omega))*int_Bbar; 


// double num = time_acc*((1.+ q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*pdf_B/int_B
//                      + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*pdf_Bbar/int_Bbar);

// double num = time_acc*((1.+ q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*pdf_B
//                       + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*pdf_Bbar);
// double den =  (int_B + int_Bbar);
//REMOVE ATTENTION

// printf("t %lf, num %lf, den %lf, time_acc %lf, int_B %lf, int_Bbar %lf, integral[0] %lf, integral[1] %lf, ta %lf, tb %lf, tc %lf, td %lf\n", t, num, den, time_acc, int_B, int_Bbar, integral[0], integral[1], ta, tb, tc, td);
// printf("helcosthetaK %lf, helcosthetaL %lf, helphi %lf, t %lf, sigma_t %lf, q_OS %lf, q_SSK %lf, eta_OS %lf, eta_SSK %lf\n", helcosthetaK, helcosthetaL, helphi, t, sigma_t, q_OS, q_SSK, eta_OS, eta_SSK);

    out[row] = num/den;//ATTENTION
}       

__global__ void DiffRate_MC(double *data, 
                         double *out, 
                         double CSP,
                         double A_0_abs, double A_S_abs, double A_pa_abs, double A_pe_abs,
                         double phis_0, double phis_S, double phis_pa, double phis_pe,
                         double delta_S, double delta_pa, double delta_pe,
                         double l_0_abs, double l_S_abs, double l_pa_abs, double l_pe_abs,
                         double G, double DG, double DM, 
                         double p0_OS, double dp0_OS, double p1_OS, double dp1_OS, double p2_OS, double dp2_OS, double eta_bar_OS,
                         double p0_SSK, double dp0_SSK, double p1_SSK, double dp1_SSK, double eta_bar_SSK,
                         double sigma_t_a, double sigma_t_b, double sigma_t_c, double t_ll,
                         double sigma_t_mu_a, double sigma_t_mu_b, double sigma_t_mu_c,
                         double f_sigma_t, double r_offset_pr, double r_offset_sc, double r_slope_pr, double r_slope_sc, 
                         double sigma_t_bar, double t_res_scale, double t_mu,
                         int spline_Nknots,
                         double *normweights,
                         double *spline_knots,
                         double *spline_coeffs, 
                         double *low_time_acc_bins_ul, double *low_time_acc,
                         int Nevt)
{
int row = threadIdx.x + blockDim.x * blockIdx.x; //ntuple entry

if (row >= Nevt) { 
    return;
}

int i0 = row*13;// general rule for cuda matrices : index = col + row*N; 
int idx = 0 + i0; 
int idy = 1 + i0;
int idz = 2 + i0;
int idt = 3 + i0;
int idsigma_t = 4 + i0;
int idq_OS = 5 + i0;
int idq_SSK = 6 + i0;
int ideta_OS = 7 + i0;
int ideta_SSK = 8 + i0;
int iyear = 12 + i0;

double helcosthetaK = data[idx];
double helcosthetaL = data[idy];
double helphi = data[idz];
double t = data[idt];
double sigma_t = data[idsigma_t];
double q_OS = data[idq_OS];
double q_SSK = data[idq_SSK];
double eta_OS = data[ideta_OS];
double eta_SSK = data[ideta_SSK];
int year = data[iyear];

double delta_0 = 0.;

double pdf_B = 0.;
double pdf_Bbar = 0.;  

double delta_t =  delta(sigma_t, sigma_t_a, sigma_t_b, sigma_t_c);

double delta_t_1 = delta_1(sigma_t, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar);
double delta_t_2 = delta_2(sigma_t, f_sigma_t, r_offset_pr, r_offset_sc, r_slope_pr, r_slope_sc, sigma_t_bar);

double omega_OS = omega(eta_OS, p0_OS, dp0_OS, p1_OS, dp1_OS, p2_OS, dp2_OS, eta_bar_OS);
double omega_bar_OS = omega_bar(eta_OS, p0_OS, dp0_OS, p1_OS, dp1_OS, p2_OS, dp2_OS, eta_bar_OS);
double omega_SSK = omega(eta_SSK, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, 0., 0., eta_bar_SSK);
double omega_bar_SSK = omega_bar(eta_SSK, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, 0., 0., eta_bar_SSK);

double tagging_pars_OS[3] = {omega_OS, omega_bar_OS, q_OS};
double tagging_pars_SSK[3] = {omega_SSK, omega_bar_SSK, q_SSK};

fix_tagging_pars(tagging_pars_OS);
fix_tagging_pars(tagging_pars_SSK);

omega_OS = tagging_pars_OS[0];
omega_bar_OS = tagging_pars_OS[1];
omega_SSK = tagging_pars_SSK[0];
omega_bar_SSK = tagging_pars_SSK[1];

if((tagging_pars_OS[0] == 0.5 || tagging_pars_OS[1] == 0.5) && (tagging_pars_OS[0] != tagging_pars_OS[1])) 
    printf("OS tag mismatch!!! Check code %lf vs %lf and %lf \n", tagging_pars_OS[0], tagging_pars_OS[1], tagging_pars_OS[2]);
else
    q_OS = tagging_pars_OS[2];

if((tagging_pars_SSK[0] == 0.5 || tagging_pars_SSK[1] == 0.5) && (tagging_pars_SSK[0] != tagging_pars_SSK[1])) 
    printf("SSK tag mismatch!!! Check code %lf vs %lf and %lf \n", tagging_pars_SSK[0], tagging_pars_SSK[1], tagging_pars_SSK[2]);
else
    q_SSK = tagging_pars_SSK[2];
    
double t_offset;
pycuda::complex<double> hyper_p, hyper_m, trig;
double ta, tb, tc, td;

if(year > 2013)
{
    t_offset = delta(sigma_t, sigma_t_mu_a, sigma_t_mu_b, sigma_t_mu_c);
    
    hyper_p = conv_exp_VC(t-t_offset, G + 0.5*DG, 0., delta_t);
    hyper_m = conv_exp_VC(t-t_offset, G - 0.5*DG, 0., delta_t);
    trig    = conv_exp_VC(t-t_offset, G, DM, delta_t);
    
    ta = pycuda::real(0.5*(hyper_m + hyper_p));
    tb = pycuda::real(0.5*(hyper_m - hyper_p));
    tc = pycuda::real(trig);
    td = pycuda::imag(trig);
}
else
{
    t_offset = t_mu;

    ta = conv_cosh(t-t_offset, G, DG, delta_t_2, delta_t_1, f_sigma_t);
    tb = conv_sinh(t-t_offset, G, DG, delta_t_2, delta_t_1, f_sigma_t);
    tc = conv_exp_cos(t-t_offset, G, DM, delta_t_2, delta_t_1, f_sigma_t);
    td = conv_exp_sin(t-t_offset, G, DM, delta_t_2, delta_t_1, f_sigma_t);
}

double vNk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vak[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vbk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vck[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vdk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};

for(int k = 1; k <= 10; k++)
{
    double Nk_term = Nk(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,CSP,k);
    double fk_term = fk(helcosthetaK,helcosthetaL,helphi,k); 
    
    double ak_term = ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);    
    double bk_term = bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    double ck_term = ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    double dk_term = dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    
    double hk_B    = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb + ck_term*tc + dk_term*td);
    double hk_Bbar = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb - ck_term*tc - dk_term*td);
    
    pdf_B    += Nk_term*hk_B*fk_term;            
    pdf_Bbar += Nk_term*hk_Bbar*fk_term;

    vNk[k-1] = 1.*Nk_term;
    vak[k-1] = 1.*ak_term;
    vbk[k-1] = 1.*bk_term;
    vck[k-1] = 1.*ck_term;
    vdk[k-1] = 1.*dk_term;
}

double integral[2] = {0.,0.};

double time_acc = 0.;
 
if(year > 2013)
{    
    // Time acceptance with a spline
    time_acc = calculate_time_acc(t, spline_knots, spline_coeffs, spline_Nknots); 
    integral4pitime_spline(integral, vNk, vak, vbk, vck, vdk, normweights, G, DG, DM, delta_t, t_ll, t_offset, spline_Nknots, spline_knots, spline_coeffs);
}
else
{
    // Time acceptance with a histogram
    time_acc = get_time_acc(t, low_time_acc_bins_ul, low_time_acc); 
    integral4pitime_double(integral, vNk, vak, vbk, vck, vdk, normweights, G, DG, DM, t_ll, delta_t_1, delta_t_2, f_sigma_t, t_mu, low_time_acc_bins_ul, low_time_acc);
}
    
double int_B = integral[0];
double int_Bbar = integral[1];

// Total PDF
double num = time_acc*((1.+ q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*pdf_B 
                      + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*pdf_Bbar);
// double num = time_acc*((1.+ q_OS)*(1.+q_SSK)*pdf_B 
//                      + (1.-q_OS)*(1.-q_SSK)*pdf_Bbar);
 
// Total PDF integral
double den =  ((1.+q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*int_B 
              + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*int_Bbar);
// double den =  ((1.+q_OS)*(1.+q_SSK)*int_B 
//              + (1.-q_OS)*(1.-q_SSK)*int_Bbar);

//     printf("omega_SSK %lf, omega_bar_SSK %lf, omega_OS %lf, omega_bar_OS %lf \n", omega_SSK, omega_bar_SSK, omega_OS, omega_bar_OS);
    out[row] = num/den;
}       

__global__ void generate(//double *data, 
                         double *out, 
                         double CSP,
                         double A_0_abs, double A_S_abs, double A_pa_abs, double A_pe_abs,
                         double phis_0, double phis_S, double phis_pa, double phis_pe,
                         double delta_S, double delta_pa, double delta_pe,
                         double l_0_abs, double l_S_abs, double l_pa_abs, double l_pe_abs,
                         double G, double DG, double DM, 
                         double p0_OS, double dp0_OS, double p1_OS, double dp1_OS, double eta_bar_OS,
                         double p0_SSK, double dp0_SSK, double p1_SSK, double dp1_SSK, double eta_bar_SSK,
                         double sigma_t_a, double sigma_t_b, double sigma_t_c, double t_ll,
                         int spline_Nknots,
                         double *normweights,
                         double *moments,
                         double *spline_knots,
                         double *spline_coeffs, 
                         int year, int hltB,
                         double Probmax, int Nevt)
{
  int row = threadIdx.x + blockDim.x * blockIdx.x; //ntuple entry
  
  if (row >= Nevt) { 
    return;
  }

  int i0 = row*12;// general rule for cuda matrices : index = col + row*N; 
  int idx = 0 + i0; 
  int idy = 1 + i0;
  int idz = 2 + i0;
  int idt = 3 + i0;
  int idsigma_t = 4 + i0;
  int idq_OS = 5 + i0;
  int idq_SSK = 6 + i0;
  int ideta_OS = 7 + i0;
  int ideta_SSK = 8 + i0;

  curandState state;
  curand_init((unsigned long long)clock(), row, 0, &state);

  //double sigma_t = 0.04;//data[idsigma_t];
  double sigma_t = curand_log_normal(&state,-3.22,0.309);
  
  double q_OS = 0.;
  double q_SSK = 0.;//data[idq_SSK];
  double eta_OS = 0.5;
  double eta_SSK = 0.5;//ta[ideta_SSK];

  double tag = curand_uniform(&state);
  if (tag < 0.16) 
      q_OS = 1.;
  else if (tag<0.32) 
      q_OS = -1.;
  else 
      q_OS = 0.;
  
  tag = curand_uniform(&state);
  if (tag < 0.31) 
      q_SSK = 1.;
  else if (tag<0.62) 
      q_SSK = -1.;
  else 
      q_SSK = 0.;
  
  double OSmax = P_omega_os(0.5);
  double SSmax = P_omega_ss(0.5);
  double thr;//, mt, pt;

  if (q_OS > 0.5 || q_OS < -0.5) 
  {
    while(1)
    {
      tag = .49*curand_uniform(&state);
      thr = OSmax*curand_uniform(&state);
      if (P_omega_os(tag) > thr) break;
    }
    eta_OS = tag;
  }
  
  if (q_SSK > 0.5 || q_SSK < -0.5) 
   {
    while(1)
    {
      tag = .49*curand_uniform(&state);
      thr = SSmax*curand_uniform(&state);
      if (P_omega_ss(tag) > thr) break;
    }
    eta_SSK = tag;
  }

  double delta_0 = 0.;
  
  double delta_t =  delta(sigma_t, sigma_t_a, sigma_t_b, sigma_t_c);
  
  double omega_OS = eta_OS; //omega(eta_OS, p0_OS, dp0_OS, p1_OS, dp1_OS, eta_bar_OS);
  double omega_bar_OS = eta_OS; //omega_bar(eta_OS, p0_OS, dp0_OS, p1_OS, dp1_OS, eta_bar_OS);
  double omega_SSK = eta_SSK; // omega(eta_SSK, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, eta_bar_SSK);
  double omega_bar_SSK = eta_SSK; //omega_bar(eta_SSK, p0_SSK, dp0_SSK, p1_SSK, dp1_SSK, eta_bar_SSK);
  
  double tagging_pars_OS[3] = {omega_OS, omega_bar_OS, q_OS};
  double tagging_pars_SSK[3] = {omega_SSK, omega_bar_SSK, q_SSK};
  
  fix_tagging_pars(tagging_pars_OS);
  fix_tagging_pars(tagging_pars_SSK);
  
  omega_OS = tagging_pars_OS[0];
  omega_bar_OS = tagging_pars_OS[1];
  omega_SSK = tagging_pars_SSK[0];
  omega_bar_SSK = tagging_pars_SSK[1];
  
  
  double vNk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double vak[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double vbk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double vck[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double vdk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double ta, tb, tc, td, pdf;
  double x,y,z,t;
  double Nk_term, fk_term, ak_term, bk_term, ck_term, dk_term, hk_B, hk_Bbar;
  double pdf_B = 0.;
  double pdf_Bbar = 0.;
  int Niter = 0;
  
  while(1) 
  {
    pdf_B = 0.;
    pdf_Bbar = 0.;  
  
    x = -1. + 2*curand_uniform(&state);
    y = -1. + 2*curand_uniform(&state);
    z = -M_PI + 2*M_PI*curand_uniform(&state);
    t = t_ll -log(curand_uniform(&state))/(G-0.5*DG);
    
    thr = Probmax*curand_uniform(&state);
    
    //mt = exp(-(0.5*G + 0.25*DG)*t); 
    //pt = exp(-(0.5*G - 0.25*DG)*t);
    
    pycuda::complex<double> hyper_p = conv_exp_VC(t, G + 0.5*DG, 0., delta_t);
    pycuda::complex<double> hyper_m = conv_exp_VC(t, G - 0.5*DG, 0., delta_t);
    pycuda::complex<double> trig    = conv_exp_VC(t, G, DM, delta_t);

    ta = pycuda::real(0.5*(hyper_m + hyper_p));
    tb = pycuda::real(0.5*(hyper_m - hyper_p));
    tc = pycuda::real(trig);
    td = pycuda::imag(trig);

    for(int k = 1; k <= 10; k++)
    {
      Nk_term = Nk(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,CSP,k);
      fk_term = fk(x,y,z,k); 
      
      ak_term = ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);    
      bk_term = bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
      ck_term = ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
      dk_term = dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
      
      hk_B    = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb + ck_term*tc + dk_term*td);
      hk_Bbar = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb - ck_term*tc - dk_term*td);
    
      pdf_B    += Nk_term*hk_B*fk_term;
      pdf_Bbar += Nk_term*hk_Bbar*fk_term;

      vNk[k-1] = 1.*Nk_term;
      vak[k-1] = 1.*ak_term;
      vbk[k-1] = 1.*bk_term;
      vck[k-1] = 1.*ck_term;
      vdk[k-1] = 1.*dk_term;
    }
    
    double integral[2] = {0.,0.};
    
    double time_acc = calculate_time_acc(t, spline_knots, spline_coeffs, spline_Nknots); 
    // Integral time acceptance with a spline
    double ang_acc = ang_eff(x, y, z, moments);

//     integral4pitime_spline(integral, vNk, vak, vbk, vck, vdk, flat_normweights, G, DG, DM, delta_t, 
//                            t_ll,0., spline_Nknots, spline_knots, spline_coeffs);
    integral4pitime_spline(integral, vNk, vak, vbk, vck, vdk, normweights, G, DG, DM, delta_t, 
                           t_ll,0., spline_Nknots, spline_knots, spline_coeffs);

    
    double int_B = integral[0];
    double int_Bbar = integral[1];

    // Total PDF
    double num = time_acc*ang_acc*((1.+ q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*pdf_B 
                     + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*pdf_Bbar);

    // Total PDF integral
    double den =  ((1.+q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*int_B 
             + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*int_Bbar);

    //double pdf = complex_angular(x,y,z,A0, As, Apa, Ape, Cfact_10);
    double pdf = num/den*exp((G-0.5*DG)*(t-t_ll))*(G-0.5*DG)*(1- exp((G-0.5*DG)*(-15.+t_ll))    );
    //printf("%e\n",(G-0.5*DG)*(exp((G-0.5*DG)*(-t_ll)) - exp((G-0.5*DG)*(-15.))    ));
    
    //printf("%lf %lf %lf\n",q,thr, Probmax);
    if (t > 15.) 
        pdf = 0.;
    
    if (pdf > Probmax) 
    {
        printf("pdf greater than Probmax (p = %e, Pmax = %e) at t = %e. I would say that is a problem. But not really my business. I'll just continue generating. \n", pdf, Probmax,t);
    }
    
    Niter++;
    
    if (pdf >= thr) 
    {
        //      printf("pdf greater than threshold (p = %e, thr = %e) at t = %e. after %i iters  \n", pdf, thr,t, Niter);
       out[idx] = x;
       out[idy] = y;
       out[idz] = z;
       out[idt] = t;
       out[idsigma_t] = sigma_t;
       out[idq_OS] = q_OS;
       out[idq_SSK] = q_SSK;
       out[ideta_OS] = eta_OS;
       out[ideta_SSK] = eta_SSK;
       return;
    }
   }
  }
  

__global__ void generate_perfect_tag(//double *data, 
                         double *out, 
                         double CSP,
                         double A_0_abs, double A_S_abs, double A_pa_abs, double A_pe_abs,
                         double phis_0, double phis_S, double phis_pa, double phis_pe,
                         double delta_S, double delta_pa, double delta_pe,
                         double l_0_abs, double l_S_abs, double l_pa_abs, double l_pe_abs,
                         double G, double DG, double DM, 
                         double p0_OS, double dp0_OS, double p1_OS, double dp1_OS, double eta_bar_OS,
                         double p0_SSK, double dp0_SSK, double p1_SSK, double dp1_SSK, double eta_bar_SSK,
                         double sigma_t_a, double sigma_t_b, double sigma_t_c, double t_ll,
                         int spline_Nknots,
                         double *normweights,
                         double *moments,
                         double *spline_knots,
                         double *spline_coeffs,
                         int year, int hltB,
                         double Probmax, int Nevt)
{
  int row = threadIdx.x + blockDim.x * blockIdx.x; //ntuple entry
  
  if (row >= Nevt) { 
    return;
  }

  int i0 = row*13;// general rule for cuda matrices : index = col + row*N; 
  int idx = 0 + i0; 
  int idy = 1 + i0;
  int idz = 2 + i0;
  int idt = 3 + i0;
  int idsigma_t = 4 + i0;
  int idq_OS = 5 + i0;
  int idq_SSK = 6 + i0;
  int ideta_OS = 7 + i0;
  int ideta_SSK = 8 + i0;
  int isw = 9 + i0;
  int iplaceholder = 10 + i0;
  int ihltB = 11 + i0;
  int iyear = 12 + i0;

  curandState state;
  curand_init((unsigned long long)clock(), row, 0, &state);

  //double sigma_t = 0.04;//data[idsigma_t];
//   double sigma_t = curand_log_normal(&state,-3.22,0.309);
  double sigma_t = 0.04554;
  
  double q = 0.;
  double eta = 0.;

  double tag = curand_uniform(&state);
  if (tag < 0.5) 
      q = 1.;
  else
      q = -1;

  double delta_0 = 0.;
  
  double delta_t =  sigma_t;//delta(sigma_t, sigma_t_a, sigma_t_b, sigma_t_c);
  
  double omega = eta;
  double omega_bar = eta; 
  
  double vNk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double vak[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double vbk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double vck[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double vdk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double ta, tb, tc, td, pdf;
  double x,y,z,t;
  double Nk_term, fk_term, ak_term, bk_term, ck_term, dk_term, hk_B, hk_Bbar;
  double pdf_B = 0.;
  double pdf_Bbar = 0.;
  int Niter = 0;
  double thr;
  while(1) 
  {
    pdf_B = 0.;
    pdf_Bbar = 0.;  
  
    x = -1. + 2*curand_uniform(&state);
    y = -1. + 2*curand_uniform(&state);
    z = -M_PI + 2*M_PI*curand_uniform(&state);
    t = t_ll -log(curand_uniform(&state))/(G-0.5*DG);
    
    thr = Probmax*curand_uniform(&state);
    
    //mt = exp(-(0.5*G + 0.25*DG)*t); 
    //pt = exp(-(0.5*G - 0.25*DG)*t);
    
    pycuda::complex<double> hyper_p = conv_exp_VC(t, G + 0.5*DG, 0., delta_t);
    pycuda::complex<double> hyper_m = conv_exp_VC(t, G - 0.5*DG, 0., delta_t);
    pycuda::complex<double> trig    = conv_exp_VC(t, G, DM, delta_t);

    ta = pycuda::real(0.5*(hyper_m + hyper_p));
    tb = pycuda::real(0.5*(hyper_m - hyper_p));
    tc = pycuda::real(trig);
    td = pycuda::imag(trig);

    for(int k = 1; k <= 10; k++)
    {
      Nk_term = Nk(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,CSP,k);
      fk_term = fk(x,y,z,k); 
      
      ak_term = ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);    
      bk_term = bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
      ck_term = ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
      dk_term = dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
      
      hk_B    = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb + ck_term*tc + dk_term*td);
      hk_Bbar = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb - ck_term*tc - dk_term*td);
    
      pdf_B    += Nk_term*hk_B*fk_term;
      pdf_Bbar += Nk_term*hk_Bbar*fk_term;

      vNk[k-1] = 1.*Nk_term;
      vak[k-1] = 1.*ak_term;
      vbk[k-1] = 1.*bk_term;
      vck[k-1] = 1.*ck_term;
      vdk[k-1] = 1.*dk_term;
    }
    
    double integral[2] = {0.,0.};
    
    double time_acc = calculate_time_acc(t, spline_knots, spline_coeffs, spline_Nknots); 
    // Integral time acceptance with a spline
    double ang_acc = ang_eff(x, y, z, moments);

//     integral4pitime_spline(integral, vNk, vak, vbk, vck, vdk, flat_normweights, G, DG, DM, delta_t, 
//                            t_ll,0., spline_Nknots, spline_knots, spline_coeffs);
    integral4pitime_spline(integral, vNk, vak, vbk, vck, vdk, normweights, G, DG, DM, delta_t, 
                           t_ll,0., spline_Nknots, spline_knots, spline_coeffs);
    
    double int_B = integral[0];
    double int_Bbar = integral[1];

    // Total PDF
    double num = time_acc*ang_acc*((1.+q*(1.-2.*omega))*pdf_B 
                                + (1.-q*(1.-2.*omega_bar))*pdf_Bbar);

    // Total PDF integral
    double den =  (1.+q*(1.-2.*omega))*int_B 
                + (1.-q*(1.-2.*omega_bar))*int_Bbar;

    double pdf = num/den*exp((G-0.5*DG)*(t-t_ll))*(G-0.5*DG)*(1- exp((G-0.5*DG)*(-15.+t_ll)));

    if (t > 15.) 
        pdf = 0.;
    
    if (pdf > Probmax) 
    {
        printf("pdf greater than Probmax (p = %e, Pmax = %e) at t = %e. I would say that is a problem. But not really my business. I'll just continue generating. \n", pdf, Probmax,t);
    }
    
    Niter++;
    
    if(Niter > 1000)
     {
         printf("slacking (p = %e, num %e, den %e, pdf_B %e, pdf_Bbar %e, time_acc %e, ang_acc %e, q %e, q %e, omega %e, omega_bar %e, thr = %e) at t = %e. after %i iters  \n", pdf, num, den, pdf_B, pdf_Bbar, time_acc, ang_acc, q, q, omega, omega_bar, thr, t, Niter);
         return;
     }

    
    if (pdf >= thr) 
    {
//        printf("pdf greater than threshold (p = %e, thr = %e) at t = %e. after %i iters  \n", pdf, thr,t, Niter);
       out[idx] = x;
       out[idy] = y;
       out[idz] = z;
       out[idt] = t;
       out[idsigma_t] = sigma_t;
       out[idq_OS] = q;
       out[idq_SSK] = q;
       out[ideta_OS] = eta;
       out[ideta_SSK] = eta;
       out[isw] = 1.;
       out[iplaceholder] = 1.;
       out[ihltB] = hltB;
       out[iyear] = year;
       return;
    }
   }
  }  

}
