import pycuda.cumath
from timeit import default_timer as timer
from tools import plt
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
from math import pi
import pycuda.gpuarray as gpuarray
from toygen import poissonLL_b as poissonLL
from iminuit import Minuit
from ModelBricks import Parameter, Free, Cat
import numpy as np
from PhisModel_ang_acc import mod, Badjanak as Model
#from phisParams import CSPRun1 as CSP, sWscales
from phisParams_ang_acc import CSP, sWscales
from scipy import random as rnd
from ROOT import *
import cPickle
BLOCK_SIZE = 90 #(1 - 1024) For complex the max no. smaller

#integra = mod.get_function("binnedTimeIntegral")

#integraB = mod.get_function("binnedTimeIntegralB")
#integraBbar = mod.get_function("binnedTimeIntegralBbar")
phis_dummy_ = RooRealVar("phis_","phis_",2,0,4)
DG_dummy_ = RooRealVar("DG_","DG_",2,0,4)

phis_dummy= RooUnblindUniform("phis","phis", "BsPhis20152016", .1, phis_dummy_)
DG_dummy = RooUnblindUniform("DG","DG", "BsDGs20152016", .1, DG_dummy_)
phisOff = phis_dummy.getVal()- phis_dummy_.getVal()
DGOff = DG_dummy.getVal() - DG_dummy_.getVal()

def manipulate_parameter(par, factor):
    rand1 = rnd.uniform()
    rand2 = rnd.uniform()
       
    if rand1 > 0.5:
        par = par + factor*rand2
    else:
        par = par - factor*rand2
            
    return par  

def getGrid(thiscat, BLOCK_SIZE):
    Nbunch = thiscat.Nevts *1. / BLOCK_SIZE
    if Nbunch > int(Nbunch): 
        Nbunch = int(Nbunch) +1
    else : 
        Nbunch = int(Nbunch)
    return  (Nbunch,1,1)
   
cats, Params = [], []
from math import pi
initial_fL = 0.50#0.50#0.5241
initial_fpe = 0.25#0.25#0.2504
initial_phis_0 = -0.03#0.#0.8#
initial_phis_S = 0.#-0.03
initial_phis_pa = 0.#-0.03#0.8#
initial_phis_pe = 0.#-0.03#0.8#
initial_dpa = 3.1#1.57#ATTENTION
initial_dpe = 2.64#5.78#-1.57#-0.1#ATTENTION

Params.append(Free("fL",initial_fL, limits=(0.48,0.52)))
Params.append(Free("fpe",initial_fpe, limits=(0.23,0.27)))
#Params.append(Free("phis_0",initial_phis_0, limits=(-1.,1.) , blind_offset = 0))#limits=(-pi,pi))#0.9 for phi only#ATTENTION
Params.append(Free("phis_0",initial_phis_0, limits=(-0.5,0.5) , blind_offset = phisOff))#limits=(-pi,pi))#0.9 for phi only
Params.append(Parameter("phis_S",initial_phis_S, limits=(-0.2,0.2), blind_offset = phisOff))
Params.append(Parameter("phis_pa",initial_phis_pa, limits=(-0.2,0.2), blind_offset = phisOff))##limits=(-pi,pi)#0.8 for phi only
Params.append(Parameter("phis_pe",initial_phis_pe, limits=(-0.2,0.2), blind_offset = phisOff))## limits=(-pi,pi)#0.7 for phi only
Params.append(Free("dpa",initial_dpa, limits=(-2*pi,2*pi)))#1.57 for phi only
Params.append(Free("dpe",initial_dpe, limits=(-2*pi,2*pi)))#-1.57 for phi only
Params.append(Free("lambda_0_abs",1., limits=(0.8,1.2)))
Params.append(Parameter("lambda_S_abs",1., limits=(0.8,1.2)))
Params.append(Parameter("lambda_pa_abs",1., limits=(0.8,1.2)))
Params.append(Parameter("lambda_pe_abs",1., limits=(0.8,1.2)))

Params.append(Free("G",0.6563, limits=(0.5,0.8)))
#Params.append(Free("G",0.6603, limits=(0.5,0.8)))#ATTENTION
Params.append(Free("DG",0.0805, limits=(-0.2,0.4), blind_offset = DGOff))
#Params.append(Free("DG",0.0805, limits=(0.06,0.50), blind_offset = 0.))#ATTENTION
Params.append(Free("Dm",17.77, limits=(17.,18.5)))

#a = cPickle.load(file("./data/data_bbar_phi_0.5pi_res_tag_per_evt_3.ext","r"))
#a = np.float64(a)
#datahist = np.histogram(a[:,7], 100)
#plt.plot(datahist[1][:-1],datahist[0])
#plt.show()
#EXIT
        
Ntot = 0
for i in range(1,7):
#for i in range(3,4):
    ibin = str(i)
    cat_name = "Cat_"+ ibin

    Params.append(Parameter("CSP_" + ibin ,CSP[i]))
        
    if i<4:
        Params.append(Free("ds_m_dpe_" + ibin, pi/4., limits=(-1.5*pi,2.*pi)))
        #Params.append(Free("ds_" + ibin, 0., limits=(-1.5*pi,2.*pi)))
    else:
        Params.append(Free("ds_m_dpe_" + ibin, -pi/4., limits=(-2.*pi,1.5*pi)))
        #Params.append(Free("ds_" + ibin, 0., limits=(-2.*pi,1.5*pi)))
        
    Params.append(Free("Fs_" + ibin, 0., limits=(0.0001,0.8)))
    #Params.append(Parameter("Fs_" + ibin,0, limits=(0.,.5)))#ATTENTION
    #Params.append(Parameter("ds_" + ibin,0., limits=(-10,10.)))#ATTENTION
    
    filename = "/scratch28/veronika/BsJpsiKK/data2016ForFit_0.20_"+ ibin +".ext"#ATTENTION
    #filename = "/scratch28/veronika/BsJpsiKK/data_b_phi_m0.03_0.5pi_0.2ps_SS_1_"+ ibin +".ext"#ATTENTION
    ## Define category for i-th bin
    thiscat = Cat(cat_name, filename, getN = True)
    
    sweights = np.float64(thiscat.data.get()[:,9])
    thiscat.weights = sWscales[i]*gpuarray.to_gpu(np.float64(sweights))
    #thiscat.weights = gpuarray.to_gpu(np.float64(sweights))#ATTENTION
    #thiscat.weights = gpuarray.to_gpu(np.float64(thiscat.Nevts*[1.])) ## all weights to 1.
    
    thiscat.wNorm = np.float64(gpuarray.sum(thiscat.weights).get())
    
    print "Nevts", i, thiscat.Nevts
    print "wNorm", i, thiscat.wNorm
    Ntot+=thiscat.Nevts
    #datahist = np.histogram(sweights[:6], 100)
    #plt.plot(datahist[1][:-1],datahist[0])
    #plt.show()
    
    #thiscat.integra = integra
    thiscat.bin = i
    thiscat.ibin = str(i)
    thiscat.block = (BLOCK_SIZE,1,1)
    thiscat.grid = getGrid(thiscat, BLOCK_SIZE)
    cats.append(thiscat)
print "Ntot", Ntot
#BREAK 
start = timer()
manager = Model(Params, cats, weighted = 1, blind = 1)
manager.createFit()
manager.fit.set_strategy(2)
manager.fit.tol = 2

manager.fit.migrad()
print timer() - start
#manager.fit.hesse()
#BREAK
while manager.fit.get_fmin()["edm"] >0.01:# 2e-04:
     print "Edm still too high. Trying again."
     for i in range(len(Params)):
##         #if Params[i].name == "fL": Params[i].setVal(manipulate_parameter(initial_fL, 0.01))
##         #if Params[i].name == "fpe": Params[i].setVal(manipulate_parameter(initial_fpe, 0.01))
         if Params[i].name == "phis_0": Params[i].setVal(manipulate_parameter(initial_phis_0, 0.01))
##         #if Params[i].name == "phis_S": Params[i].setVal(manipulate_parameter(initial_phis_S, 0.01))
##         #if Params[i].name == "phis_pa": Params[i].setVal(manipulate_parameter(initial_phis_pa, 0.01))
##         #if Params[i].name == "phis_pe": Params[i].setVal(manipulate_parameter(initial_phis_pe, 0.01))
##         #if Params[i].name == "G": Params[i].setVal(manipulate_parameter(initial_phis_pe, 0.01))
         if Params[i].name == "dpa": Params[i].setVal(manipulate_parameter(initial_dpa, 0.5))
         if Params[i].name == "dpe": Params[i].setVal(manipulate_parameter(initial_dpe, 0.5))
         #if Params[i].name == "lambda_0_abs": Params[i].setVal(manipulate_parameter(1, 0.01))
##         for j in range(1,7):
##             if Params[i].name == "ds_" + str(j) : Params[i].setVal(manipulate_parameter(0, 2*pi))
##     #manager = Model(Params, cats)
     manager.createFit()
     manager.fit.set_strategy(2)
     manager.fit.tol = 2
     manager.fit.migrad()
    
manager.fit.hesse()
## #manager.fit.minos()
## print manager.fit.get_fmin()['fval']
##manager.plotcat(cats[0])
##plt.show()
EXIT
manager.createMultinest("mnest_party", reset = False)
#manager.createMultinest("mnest_party")
values_dic = manager.mnest_vals()
values_list = len(manager.params)*[0.]
for i in xrange(len(manager.params)): 
    values_list[i] = values_dic[manager.params[i]]
manager(*values_list)
    
        
