#include <stdio.h>
#include <math.h>
// #include <thrust/complex.h>
#include <pycuda-complex.hpp>

#define errf_const 1.12837916709551
#define xLim 5.33
#define yLim 4.29
#define t0 0.5

__device__ pycuda::complex<double> faddeeva(pycuda::complex<double> z, double t) 
{
   double in_real = pycuda::real(z);
   double in_imag = pycuda::imag(z);
   int n, nc, nu;
   double h, q, Saux, Sx, Sy, Tn, Tx, Ty, Wx, Wy, xh, xl, x, yh, y;
   double Rx [33];
   double Ry [33];

   x = fabs(in_real);
   y = fabs(in_imag);

   if (y < yLim && x < xLim) {
      q = (1.0 - y / yLim) * sqrt(1.0 - (x / xLim) * (x / xLim));
      h  = 1.0 / (3.2 * q);
      nc = 7 + int(23.0 * q);
      xl = pow(h, double(1 - nc));
      xh = y + 0.5 / h;
      yh = x;
      nu = 10 + int(21.0 * q);
      Rx[nu] = 0.;
      Ry[nu] = 0.;
      for (n = nu; n > 0; n--){
         Tx = xh + n * Rx[n];
         Ty = yh - n * Ry[n];
         Tn = Tx*Tx + Ty*Ty;
         Rx[n-1] = 0.5 * Tx / Tn;
         Ry[n-1] = 0.5 * Ty / Tn;
         }
      Sx = 0.;
      Sy = 0.;
      for (n = nc; n>0; n--){
         Saux = Sx + xl;
         Sx = Rx[n-1] * Saux - Ry[n-1] * Sy;
         Sy = Rx[n-1] * Sy + Ry[n-1] * Saux;
         xl = h * xl;
      };
      Wx = errf_const * Sx;
      Wy = errf_const * Sy;
   }
   else {
      xh = y;
      yh = x;
      Rx[0] = 0.;
      Ry[0] = 0.;
      for (n = 9; n>0; n--){
         Tx = xh + n * Rx[0];
         Ty = yh - n * Ry[0];
         Tn = Tx * Tx + Ty * Ty;
         Rx[0] = 0.5 * Tx / Tn;
         Ry[0] = 0.5 * Ty / Tn;
      };
      Wx = errf_const * Rx[0];
      Wy = errf_const * Ry[0];
   }

   if (y == 0.) {
      Wx = exp(-x * x);
   }
   if (in_imag < 0.) {
      Wx =   2.0 * exp(y * y - x * x) * cos(2.0 * x * y) - Wx;
      Wy = - 2.0 * exp(y * y - x * x) * sin(2.0 * x * y) - Wy;
      if (in_real > 0.) {
         Wy = -Wy;
      }
   }
   else if (in_real < 0.) {
      Wy = -Wy;
   }

//    printf("=============== \n fad(z) = (%lf,%lf), z = (%lf,%lf), t = %lf \n =============== \n ", Wx, Wy, in_real, in_imag, t);   
   return pycuda::complex<double>(Wx,Wy);
}

__device__ double conv_exp(double t, double G, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    if(t>t0){
        double f = 2.*(sigma_t*sqrt(0.5*M_PI)); 
                   
        return f*exp(-G*t+0.5*G*G*sigma_t_sq);
    }
    else
        return sigma_t*sqrt(0.5*M_PI)*exp(-G*t+0.5*G*sigma_t_sq)*(1.+erf((t-G*sigma_t_sq)/(sigma_t*sqrt(2.)))); 
}

__device__ double conv_exp_cos(double t, double G, double Dm, double sigma_t) 
{   
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>t0){        
        double f  = 2.*(sigma_t*sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*cos(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return sigma_t*sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::real(faddeeva(z,t));
    }
}

__device__ double conv_exp_sin(double t, double G, double Dm, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>t0){
        double f  = 2.*(sigma_t*sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*sin(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return -sigma_t*sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::imag(faddeeva(z,t));
    }
}

__device__ double conv_cosh(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp(t,G_m_hDG,sigma_t)+conv_exp(t,G_p_hDG,sigma_t));
}

__device__ double conv_sinh(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp(t,G_m_hDG,sigma_t)-conv_exp(t,G_p_hDG,sigma_t));
}

__device__ double Nk(double A_0_abs, 
                     double A_S_abs,
                     double A_pa_abs, 
                     double A_pe_abs,
                     double CSP,
                     int k)
{
    double nk;
    switch(k) {
        case 1:  nk = A_0_abs*A_0_abs;
                 break;
        case 2:  nk = A_pa_abs*A_pa_abs;
                 break;
        case 3:  nk = A_pe_abs*A_pe_abs;
                 break;
        case 4:  nk = A_pe_abs*A_pa_abs;
                 break;
        case 5:  nk = A_0_abs*A_pa_abs;
                 break;
        case 6:  nk = A_0_abs*A_pe_abs;
                 break;
        case 7:  nk = A_S_abs*A_S_abs;
                 break;
        case 8:  nk = CSP*A_S_abs*A_pa_abs;
                 break;
        case 9:  nk = CSP*A_S_abs*A_pe_abs;
                 break;
        case 10: nk = CSP*A_S_abs*A_0_abs;
                 break;
        default: printf("Wrong k index in nk, please check code %d\\n", k);
                 return 0.;
    }
    return nk;
}

__device__ double fk(double helcosthetaK, 
                     double helcosthetaL, 
                     double helphi,
                     int k)
{
    double helsinthetaK = sqrt(1. - helcosthetaK*helcosthetaK);
    double helsinthetaL = sqrt(1. - helcosthetaL*helcosthetaL);
//     helphi -= M_PI;
    double helsinphi = sin(-helphi);
    double helcosphi = cos(-helphi);
    
    double fk;
    switch(k) {
        case 1:  fk = helcosthetaK*helcosthetaK*helsinthetaL*helsinthetaL;
                 break;
        case 2:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helcosphi*helcosphi*helsinthetaL*helsinthetaL);
                 break;
        case 3:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helsinphi*helsinphi*helsinthetaL*helsinthetaL);
                 break;
        case 4:  fk = helsinthetaK*helsinthetaK*helsinthetaL*helsinthetaL*helsinphi*helcosphi;
                 break;
        case 5:  fk = sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helcosphi;
                 break;
        case 6:  fk = -sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helsinphi;
                 break;
        case 7:  fk = helsinthetaL*helsinthetaL/3.;
                 break;
        case 8:  fk = 2.*helsinthetaK*helsinthetaL*helcosthetaL*helcosphi/sqrt(6.);
                 break;
        case 9:  fk = -2.*helsinthetaK*helsinthetaL*helcosthetaL*helsinphi/sqrt(6.);
                 break;
        case 10: fk = 2.*helcosthetaK*helsinthetaL*helsinthetaL/sqrt(3.);
                 break;
        default: printf("Wrong k index in fk, please check code %d\\n", k);
                 return 0.;
    }
    return fk;
}

__device__ double ak(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double ak;
    switch(k) {
        case 1:  ak = 0.5*(1.+lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ak = 0.5*(1.+lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ak = 0.5*(1.+lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ak = 0.5*(sin(delta_pe-delta_pa) - lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ak = 0.5*(cos(delta_0-delta_pa) + lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ak = -0.5*(sin(delta_0-delta_pe) - lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ak = 0.5*(1.+lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ak = 0.5*(cos(delta_S-delta_pa) - lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ak = -0.5*(sin(delta_S-delta_pe) + lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ak = 0.5*(cos(delta_S-delta_0) - lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ak, please check code %d\\n", k);
                 return 0.;
    }
    return ak;
}

__device__ double bk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double bk;
    switch(k) {
        case 1:  bk = -lambda_0_abs*cos(phis_0);
                 break;
        case 2:  bk = -lambda_pa_abs*cos(phis_pa);
                 break;
        case 3:  bk = lambda_pe_abs*cos(phis_pe);
                 break;
        case 4:  bk = 0.5*(lambda_pe_abs*sin(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*sin(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  bk = -0.5*(lambda_0_abs*cos(delta_0-delta_pa-phis_0) + lambda_pa_abs*cos(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  bk = 0.5*(lambda_0_abs*sin(delta_0-delta_pe-phis_0) + lambda_pe_abs*sin(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  bk = lambda_S_abs*cos(phis_S);
                 break;
        case 8:  bk = 0.5*(lambda_S_abs*cos(delta_S-delta_pa-phis_S) - lambda_pa_abs*cos(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  bk = -0.5*(lambda_S_abs*sin(delta_S-delta_pe-phis_S) - lambda_pe_abs*sin(delta_pe-delta_S-phis_pe));
                 break;
        case 10: bk = 0.5*(lambda_S_abs*cos(delta_S-delta_0-phis_S) - lambda_0_abs*cos(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in bk, please check code %d\\n", k);
                 return 0.;
    }
    return bk;
}
    
__device__ double ck(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double ck;
    switch(k) {
        case 1:  ck = 0.5*(1.-lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ck = 0.5*(1.-lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ck = 0.5*(1.-lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ck = 0.5*(sin(delta_pe-delta_pa) + lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ck = 0.5*(cos(delta_0-delta_pa) - lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ck = -0.5*(sin(delta_0-delta_pe) + lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ck = 0.5*(1.-lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ck = 0.5*(cos(delta_S-delta_pa) + lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ck = -0.5*(sin(delta_S-delta_pe) - lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ck = 0.5*(cos(delta_S-delta_0) + lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ck, please check code %d\\n", k);
                 return 0.;
    }
    return ck;
}

__device__ double dk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double dk;
    switch(k) {
        case 1:  dk = lambda_0_abs*sin(phis_0);
                 break;
        case 2:  dk = lambda_pa_abs*sin(phis_pa);
                 break;
        case 3:  dk = -lambda_pe_abs*sin(phis_pe);
                 break;
        case 4:  dk = -0.5*(lambda_pe_abs*cos(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*cos(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  dk = -0.5*(lambda_0_abs*sin(delta_0-delta_pa-phis_0) + lambda_pa_abs*sin(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  dk = -0.5*(lambda_0_abs*cos(delta_0-delta_pe-phis_0) + lambda_pe_abs*cos(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  dk = -lambda_S_abs*sin(phis_S);
                 break;
        case 8:  dk = 0.5*(lambda_S_abs*sin(delta_S-delta_pa-phis_S) - lambda_pa_abs*sin(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  dk = -0.5*(-lambda_S_abs*cos(delta_S-delta_pe-phis_S) + lambda_pe_abs*cos(delta_pe-delta_S-phis_pe));
                 break;
        case 10: dk = 0.5*(lambda_S_abs*sin(delta_S-delta_0-phis_S) - lambda_0_abs*sin(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in dk, please check code %d\\n", k);
                 return 0.;
    }
    return dk;
}

__device__ double hk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     double G, 
                     double DG, 
                     double Dm, 
                     double sigma_t, 
                     double t,
                     double q,
                     int k)
{
    double hk = 3./(4.*M_PI);
//     double hk = 3.*exp(-G*t)/(4.*M_PI);
    
     double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
    double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);
    double exp_cos_term  = conv_exp_cos(t, G, Dm, sigma_t);
     double exp_sin_term  = conv_exp_sin(t, G, Dm, sigma_t);
    
//     double exp_cosh_term = exp(-t*G)*cosh(t*DG/2);
//     double exp_sin_term  = exp(-t*G)*sin(t*Dm);
//     double exp_cos_term  = exp(-t*G)*cos(t*Dm);
//     double exp_sinh_term = exp(-t*G)*sinh(t*DG/2);
    
    if(q < 0)
      hk *=  ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cosh_term//cosh(0.5*DG*t)
           + bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sinh_term//sinh(0.5*DG*t)
           + ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cos_term//*cos(Dm*t)
           + dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sin_term;//sin(Dm*t));
    else
      hk *=  ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cosh_term//cosh(0.5*DG*t)
           + bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sinh_term//sinh(0.5*DG*t)
           - ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cos_term//*cos(Dm*t)
           - dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sin_term;//sin(Dm*t));

    return hk;
}

__device__ double angular_time_B(   double t, 
                                    double helcosthetaK, 
                                    double helcosthetaL, 
                                    double helphi, 
                                    double CSP,
                                    double A_0_mod,
                                    double A_S_mod,
                                    double A_pa_mod,
                                    double A_pe_mod,
                                    double phi_0,
                                    double phi_S,
                                    double phi_pa,
                                    double phi_pe,
                                    double delta_S,
                                    double delta_pa,
                                    double delta_pe,
                                    double G, 
                                    double DG, 
                                    double Dm)
{
    double ck2 = helcosthetaK*helcosthetaK;
    double cl2 = helcosthetaL*helcosthetaL;
    double shp = sin(helphi);
    double chp = cos(helphi);
    double sqmcl2p1 = sqrt(-cl2 + 1);
    double sqmck2p1 = sqrt(-ck2 + 1);
    double sq2 = sqrt(2.);
    double sq6 = sqrt(6.);
    double exp_G_t = exp(-t*G);
    double cdmt = cos(t*Dm);
    double sdmt = sin(t*Dm);
    double cosh_term = cosh(t*DG/2);
    double sinh_term = sinh(t*DG/2);
    
    double Cfact_10 = CSP;
    double x2 = pow(helcosthetaK, 2);
    double y2 = pow(helcosthetaL, 2);
    //double landa_pa = 1.;
    //double landa_pe = 1;
    //caca;
    
    // double landa_0 = 1.;
    //double landa_S = 1.;
    // caca;
    
     return 3*sq2*exp_G_t*helcosthetaK*helcosthetaL*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(cdmt*A_0_mod*A_pa_mod*(-sin(phi_0)*sin(-delta_pa + phi_pa) + cos(delta_pa) - cos(phi_0)*cos(-delta_pa + phi_pa)) + cosh_term*A_0_mod*A_pa_mod*(sin(phi_0)*sin(-delta_pa + phi_pa) + cos(delta_pa) + cos(phi_0)*cos(-delta_pa + phi_pa)) + sdmt*A_0_mod*A_pa_mod*(2*sin(phi_0)*cos(delta_pa) + sin(-delta_pa + phi_pa) + sin(delta_pa - phi_0)) + sinh_term*A_0_mod*A_pa_mod*(2*sin(delta_pa)*sin(phi_0) - cos(-delta_pa + phi_pa) - cos(delta_pa - phi_0)))*cos(helphi)/(4*M_PI) + 3*sq2*exp_G_t*helcosthetaK*helcosthetaL*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(cdmt*A_0_mod*A_pe_mod*(sin(delta_pe)*sin(phi_0)*sin(phi_pe) + sin(delta_pe) + 3*sin(phi_0)*cos(delta_pe)*cos(phi_pe) + 2*sin(delta_pe - phi_0)*cos(phi_pe) - sin(delta_pe + phi_pe)*cos(phi_0)) + cosh_term*A_0_mod*A_pe_mod*(sin(delta_pe) - sin(phi_0)*cos(delta_pe - phi_pe) - sin(delta_pe - phi_pe)*cos(phi_0)) + sdmt*A_0_mod*A_pe_mod*(2*sin(delta_pe)*sin(phi_0) - cos(delta_pe - phi_0) - cos(delta_pe - phi_pe)) + sinh_term*A_0_mod*A_pe_mod*(2*sin(delta_pe)*cos(phi_pe) - 2*sin(phi_0)*cos(delta_pe) - sin(delta_pe - phi_0) - sin(delta_pe + phi_pe)))*sin(helphi)/(4*M_PI) + sqrt(3.)*exp_G_t*helcosthetaK*Cfact_10*(-y2 + 1)*(cdmt*A_0_mod*A_S_mod*(sin(phi_0)*sin(phi_S)*cos(delta_S) + sin(phi_0)*sin(-delta_S + phi_S) + sin(phi_S)*sin(delta_S - phi_0) + cos(delta_S)*cos(phi_0)*cos(phi_S) + cos(delta_S)) + cosh_term*A_0_mod*A_S_mod*(2*sin(delta_S)*sin(phi_0)*cos(phi_S) - sin(phi_0)*sin(delta_S + phi_S) + cos(delta_S) - cos(phi_0)*cos(-delta_S + phi_S)) + sdmt*A_0_mod*A_S_mod*(2*sin(delta_S)*cos(phi_S) + 2*sin(phi_0)*cos(delta_S) + sin(delta_S - phi_0) - sin(delta_S + phi_S)) + sinh_term*A_0_mod*A_S_mod*(sin(delta_S)*sin(phi_S) - 2*cos(delta_S)*cos(phi_0) + cos(delta_S)*cos(phi_S) + cos(delta_S - phi_0)))/(2*M_PI) + sq6*exp_G_t*helcosthetaL*Cfact_10*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(cdmt*A_S_mod*A_pa_mod*(sin(phi_S)*sin(phi_pa)*cos(-delta_S + delta_pa) + 2*sin(phi_pa)*sin(-delta_S + delta_pa)*cos(phi_S) - sin(-delta_S + delta_pa)*sin(phi_S + phi_pa) + cos(phi_S)*cos(phi_pa)*cos(-delta_S + delta_pa) + cos(-delta_S + delta_pa)) + cosh_term*A_S_mod*A_pa_mod*(-sin(-delta_S + delta_pa)*sin(-phi_S + phi_pa) - cos(-delta_S + delta_pa)*cos(-phi_S + phi_pa) + cos(-delta_S + delta_pa)) + sdmt*A_S_mod*A_pa_mod*(-sin(phi_S)*cos(-delta_S + delta_pa) + sin(phi_pa)*cos(-delta_S + delta_pa) - sin(-delta_S + delta_pa)*cos(phi_S) - sin(-delta_S + delta_pa)*cos(phi_pa)) + sinh_term*A_S_mod*A_pa_mod*(-sin(phi_S)*sin(-delta_S + delta_pa) - sin(phi_pa)*sin(-delta_S + delta_pa) + cos(phi_S)*cos(-delta_S + delta_pa) - cos(phi_pa)*cos(-delta_S + delta_pa)))*cos(helphi)/(4*M_PI) + sq6*exp_G_t*helcosthetaL*Cfact_10*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(cdmt*A_S_mod*A_pe_mod*(sin(delta_S - delta_pe)*cos(-phi_S + phi_pe) - sin(delta_S - delta_pe) + sin(-phi_S + phi_pe)*cos(delta_S - delta_pe)) + cosh_term*A_S_mod*A_pe_mod*(-sin(delta_S - delta_pe)*cos(-phi_S + phi_pe) - sin(delta_S - delta_pe) - sin(-phi_S + phi_pe)*cos(delta_S - delta_pe)) + sdmt*A_S_mod*A_pe_mod*(sin(phi_S)*sin(delta_S - delta_pe) + sin(phi_pe)*sin(delta_S - delta_pe) + cos(phi_S)*cos(delta_S - delta_pe) - cos(phi_pe)*cos(delta_S - delta_pe)) + sinh_term*A_S_mod*A_pe_mod*(sin(phi_S)*cos(delta_S - delta_pe) - sin(phi_pe)*cos(delta_S - delta_pe) - sin(delta_S - delta_pe)*cos(phi_S) - sin(delta_S - delta_pe)*cos(phi_pe)))*sin(helphi)/(4*M_PI) + 3*exp_G_t*x2*(-y2 + 1)*(2*cosh_term*pow(A_0_mod, 2) + 2*sdmt*pow(A_0_mod, 2)*sin(phi_0) - 2*sinh_term*pow(A_0_mod, 2)*cos(phi_0))/(4*M_PI) + 3*exp_G_t*(-x2 + 1)*(-y2 + 1)*(cdmt*A_pa_mod*A_pe_mod*(sin(phi_pa)*sin(phi_pe)*sin(delta_pa - delta_pe) + sin(delta_pa - delta_pe)*cos(phi_pa)*cos(phi_pe) + sin(delta_pa - delta_pe) - sin(phi_pa - phi_pe)*cos(delta_pa - delta_pe)) + cosh_term*A_pa_mod*A_pe_mod*(2*sin(phi_pa)*cos(phi_pe)*cos(delta_pa - delta_pe) - sin(delta_pa - delta_pe)*cos(phi_pa - phi_pe) + sin(delta_pa - delta_pe) - sin(phi_pa + phi_pe)*cos(delta_pa - delta_pe)) + sdmt*A_pa_mod*A_pe_mod*(sin(phi_pa)*sin(delta_pa - delta_pe) - sin(phi_pe)*sin(delta_pa - delta_pe) + cos(phi_pa)*cos(delta_pa - delta_pe) + cos(phi_pe)*cos(delta_pa - delta_pe)) + sinh_term*A_pa_mod*A_pe_mod*(sin(phi_pa)*cos(delta_pa - delta_pe) + sin(phi_pe)*cos(delta_pa - delta_pe) - sin(delta_pa - delta_pe)*cos(phi_pa) + sin(delta_pa - delta_pe)*cos(phi_pe)))*sin(helphi)*cos(helphi)/(4*M_PI) + 3*exp_G_t*(-x2 + 1)*(2*y2*pow(cos(helphi), 2) + 2*pow(sin(helphi), 2))*(2*cosh_term*pow(A_pa_mod, 2) + 2*sdmt*pow(A_pa_mod, 2)*sin(phi_pa) - 2*sinh_term*pow(A_pa_mod, 2)*cos(phi_pa))/(16*M_PI) + 3*exp_G_t*(-x2 + 1)*(2*cosh_term*pow(A_pe_mod, 2) - 2*sdmt*pow(A_pe_mod, 2)*sin(phi_pe) + 2*sinh_term*pow(A_pe_mod, 2)*cos(phi_pe))*(-2*y2*pow(cos(helphi), 2) + 2*y2 + 2*pow(cos(helphi), 2))/(16*M_PI) + exp_G_t*(-y2 + 1)*(2*cosh_term*pow(A_S_mod, 2) - 2*sdmt*pow(A_S_mod, 2)*sin(phi_S) + 2*sinh_term*pow(A_S_mod, 2)*cos(phi_S))/(4*M_PI) ;
}

__device__ double angular_time_Bbar(double t, 
                                    double helcosthetaK, 
                                    double helcosthetaL, 
                                    double helphi, 
                                    double CSP,
                                    double A_0_mod,
                                    double A_S_mod,
                                    double A_pa_mod,
                                    double A_pe_mod,
                                    double phi_0,
                                    double phi_S,
                                    double phi_pa,
                                    double phi_pe,
                                    double delta_S,
                                    double delta_pa,
                                    double delta_pe,
                                    double G, 
                                    double DG, 
                                    double Dm)
{
    double ck2 = helcosthetaK*helcosthetaK;
    double cl2 = helcosthetaL*helcosthetaL;
    double shp = sin(helphi);
    double chp = cos(helphi);
    double sqmcl2p1 = sqrt(-cl2 + 1);
    double sqmck2p1 = sqrt(-ck2 + 1);
    double sq2 = sqrt(2.);
    double sq6 = sqrt(6.);
    double exp_G_t = exp(-t*G);
    double cdmt = cos(t*Dm);
    double sdmt = sin(t*Dm);
    double cosh_term = cosh(t*DG/2);
    double sinh_term = sinh(t*DG/2);
    //double delta_pe = delta_pe_ + M_PI;
    double Cfact_10 = CSP;
    double x2 = pow(helcosthetaK, 2);
    double y2 = pow(helcosthetaL, 2);
    return 3*sq2*helcosthetaK*helcosthetaL*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(-cdmt*exp_G_t*A_0_mod*A_pa_mod*(-sin(phi_0)*sin(-delta_pa + phi_pa) + cos(delta_pa) - cos(phi_0)*cos(-delta_pa + phi_pa)) + cosh_term*exp_G_t*A_0_mod*A_pa_mod*(sin(phi_0)*sin(-delta_pa + phi_pa) + cos(delta_pa) + cos(phi_0)*cos(-delta_pa + phi_pa)) - exp_G_t*sdmt*A_0_mod*A_pa_mod*(2*sin(phi_0)*cos(delta_pa) + sin(-delta_pa + phi_pa) + sin(delta_pa - phi_0)) + exp_G_t*sinh_term*A_0_mod*A_pa_mod*(2*sin(delta_pa)*sin(phi_0) - cos(-delta_pa + phi_pa) - cos(delta_pa - phi_0)))*cos(helphi)/(4*M_PI) + 3*sq2*helcosthetaK*helcosthetaL*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(-cdmt*exp_G_t*A_0_mod*A_pe_mod*(sin(delta_pe)*sin(phi_0)*sin(phi_pe) + sin(delta_pe) + 3*sin(phi_0)*cos(delta_pe)*cos(phi_pe) + 2*sin(delta_pe - phi_0)*cos(phi_pe) - sin(delta_pe + phi_pe)*cos(phi_0)) + cosh_term*exp_G_t*A_0_mod*A_pe_mod*(sin(delta_pe) - sin(phi_0)*cos(delta_pe - phi_pe) - sin(delta_pe - phi_pe)*cos(phi_0)) - exp_G_t*sdmt*A_0_mod*A_pe_mod*(2*sin(delta_pe)*sin(phi_0) - cos(delta_pe - phi_0) - cos(delta_pe - phi_pe)) + exp_G_t*sinh_term*A_0_mod*A_pe_mod*(2*sin(delta_pe)*cos(phi_pe) - 2*sin(phi_0)*cos(delta_pe) - sin(delta_pe - phi_0) - sin(delta_pe + phi_pe)))*sin(helphi)/(4*M_PI) + sqrt(3.)*helcosthetaK*Cfact_10*(-y2 + 1)*(-cdmt*exp_G_t*A_0_mod*A_S_mod*(sin(phi_0)*sin(phi_S)*cos(delta_S) + sin(phi_0)*sin(-delta_S + phi_S) + sin(phi_S)*sin(delta_S - phi_0) + cos(delta_S)*cos(phi_0)*cos(phi_S) + cos(delta_S)) + cosh_term*exp_G_t*A_0_mod*A_S_mod*(2*sin(delta_S)*sin(phi_0)*cos(phi_S) - sin(phi_0)*sin(delta_S + phi_S) + cos(delta_S) - cos(phi_0)*cos(-delta_S + phi_S)) - exp_G_t*sdmt*A_0_mod*A_S_mod*(2*sin(delta_S)*cos(phi_S) + 2*sin(phi_0)*cos(delta_S) + sin(delta_S - phi_0) - sin(delta_S + phi_S)) + exp_G_t*sinh_term*A_0_mod*A_S_mod*(sin(delta_S)*sin(phi_S) - 2*cos(delta_S)*cos(phi_0) + cos(delta_S)*cos(phi_S) + cos(delta_S - phi_0)))/(2*M_PI) + sq6*helcosthetaL*Cfact_10*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(-cdmt*exp_G_t*A_S_mod*A_pa_mod*(sin(phi_S)*sin(phi_pa)*cos(-delta_S + delta_pa) + 2*sin(phi_pa)*sin(-delta_S + delta_pa)*cos(phi_S) - sin(-delta_S + delta_pa)*sin(phi_S + phi_pa) + cos(phi_S)*cos(phi_pa)*cos(-delta_S + delta_pa) + cos(-delta_S + delta_pa)) + cosh_term*exp_G_t*A_S_mod*A_pa_mod*(-sin(-delta_S + delta_pa)*sin(-phi_S + phi_pa) - cos(-delta_S + delta_pa)*cos(-phi_S + phi_pa) + cos(-delta_S + delta_pa)) - exp_G_t*sdmt*A_S_mod*A_pa_mod*(-sin(phi_S)*cos(-delta_S + delta_pa) + sin(phi_pa)*cos(-delta_S + delta_pa) - sin(-delta_S + delta_pa)*cos(phi_S) - sin(-delta_S + delta_pa)*cos(phi_pa)) + exp_G_t*sinh_term*A_S_mod*A_pa_mod*(-sin(phi_S)*sin(-delta_S + delta_pa) - sin(phi_pa)*sin(-delta_S + delta_pa) + cos(phi_S)*cos(-delta_S + delta_pa) - cos(phi_pa)*cos(-delta_S + delta_pa)))*cos(helphi)/(4*M_PI) + sq6*helcosthetaL*Cfact_10*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(-cdmt*exp_G_t*A_S_mod*A_pe_mod*(sin(delta_S - delta_pe)*cos(-phi_S + phi_pe) - sin(delta_S - delta_pe) + sin(-phi_S + phi_pe)*cos(delta_S - delta_pe)) + cosh_term*exp_G_t*A_S_mod*A_pe_mod*(-sin(delta_S - delta_pe)*cos(-phi_S + phi_pe) - sin(delta_S - delta_pe) - sin(-phi_S + phi_pe)*cos(delta_S - delta_pe)) - exp_G_t*sdmt*A_S_mod*A_pe_mod*(sin(phi_S)*sin(delta_S - delta_pe) + sin(phi_pe)*sin(delta_S - delta_pe) + cos(phi_S)*cos(delta_S - delta_pe) - cos(phi_pe)*cos(delta_S - delta_pe)) + exp_G_t*sinh_term*A_S_mod*A_pe_mod*(sin(phi_S)*cos(delta_S - delta_pe) - sin(phi_pe)*cos(delta_S - delta_pe) - sin(delta_S - delta_pe)*cos(phi_S) - sin(delta_S - delta_pe)*cos(phi_pe)))*sin(helphi)/(4*M_PI) + 3*x2*(-y2 + 1)*(2*cosh_term*exp_G_t*pow(A_0_mod, 2) - 2*exp_G_t*sdmt*pow(A_0_mod, 2)*sin(phi_0) - 2*exp_G_t*sinh_term*pow(A_0_mod, 2)*cos(phi_0))/(4*M_PI) + 3*(-x2 + 1)*(-y2 + 1)*(-cdmt*exp_G_t*A_pa_mod*A_pe_mod*(sin(phi_pa)*sin(phi_pe)*sin(delta_pa - delta_pe) + sin(delta_pa - delta_pe)*cos(phi_pa)*cos(phi_pe) + sin(delta_pa - delta_pe) - sin(phi_pa - phi_pe)*cos(delta_pa - delta_pe)) + cosh_term*exp_G_t*A_pa_mod*A_pe_mod*(2*sin(phi_pa)*cos(phi_pe)*cos(delta_pa - delta_pe) - sin(delta_pa - delta_pe)*cos(phi_pa - phi_pe) + sin(delta_pa - delta_pe) - sin(phi_pa + phi_pe)*cos(delta_pa - delta_pe)) - exp_G_t*sdmt*A_pa_mod*A_pe_mod*(sin(phi_pa)*sin(delta_pa - delta_pe) - sin(phi_pe)*sin(delta_pa - delta_pe) + cos(phi_pa)*cos(delta_pa - delta_pe) + cos(phi_pe)*cos(delta_pa - delta_pe)) + exp_G_t*sinh_term*A_pa_mod*A_pe_mod*(sin(phi_pa)*cos(delta_pa - delta_pe) + sin(phi_pe)*cos(delta_pa - delta_pe) - sin(delta_pa - delta_pe)*cos(phi_pa) + sin(delta_pa - delta_pe)*cos(phi_pe)))*sin(helphi)*cos(helphi)/(4*M_PI) + 3*(-x2 + 1)*(2*y2*pow(cos(helphi), 2) + 2*pow(sin(helphi), 2))*(2*cosh_term*exp_G_t*pow(A_pa_mod, 2) - 2*exp_G_t*sdmt*pow(A_pa_mod, 2)*sin(phi_pa) - 2*exp_G_t*sinh_term*pow(A_pa_mod, 2)*cos(phi_pa))/(16*M_PI) + 3*(-x2 + 1)*(-2*y2*pow(cos(helphi), 2) + 2*y2 + 2*pow(cos(helphi), 2))*(2*cosh_term*exp_G_t*pow(A_pe_mod, 2) + 2*exp_G_t*sdmt*pow(A_pe_mod, 2)*sin(phi_pe) + 2*exp_G_t*sinh_term*pow(A_pe_mod, 2)*cos(phi_pe))/(16*M_PI) + (-y2 + 1)*(2*cosh_term*exp_G_t*pow(A_S_mod, 2) + 2*exp_G_t*sdmt*pow(A_S_mod, 2)*sin(phi_S) + 2*exp_G_t*sinh_term*pow(A_S_mod, 2)*cos(phi_S))/(4*M_PI) ; 
 
    
}


__global__ void DiffRate(double *data, 
                         double *out, 
                         double CSP,
                         double A_0_abs,
                         double A_S_abs,
                         double A_pa_abs,
                         double A_pe_abs,
                         double phis_0,
                         double phis_S,
                         double phis_pa,
                         double phis_pe,
                         double delta_S,
                         double delta_pa,
                         double delta_pe,
                         double G, 
                         double DG, 
                         double Dm, 
                         double sigma_t, 
                         int Nevt)
{
int row = threadIdx.x + blockDim.x * blockIdx.x; //ntuple entry

if (row >= Nevt) { 
    return;
}

int i0 = row*5;// general rule for cuda matrices : index = col + row*N; as it is now, N = 5 (cthk,cthl,cphi,t,q)
int idx = 0 + i0; 
int idy = 1 + i0;
int idz = 2 + i0;
int idt = 3 + i0;
int idq = 4 + i0;

out[row] = 0.;
/*
if(data[idq] < 0)
    out[row] = angular_time_B(data[idt],data[idx],data[idy],data[idz],CSP,A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,phis_0,phis_S,
                              phis_pa,phis_pe,delta_S,delta_pa,delta_pe,G,DG,Dm);
else
    out[row] = angular_time_Bbar(data[idt],data[idx],data[idy],data[idz],CSP,A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,phis_0,phis_S,
                                 phis_pa,phis_pe,delta_S,delta_pa,delta_pe,G,DG,Dm);
    */

double lambda_0_abs = 1.;
double lambda_S_abs = 1.;
double lambda_pa_abs = 1.;
double lambda_pe_abs = 1.;
double delta_0 = 0.;
                     
for(int k = 1; k <= 10; k++){
    out[row] += Nk(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,CSP,k)
//                *hk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,G,DG,Dm,data[idt],data[idq],k)
               *hk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,G,DG,Dm,sigma_t,data[idt],data[idq],k)
               *fk(data[idx],data[idy],data[idz],k);
    }

}
__device__ double integral4piB( double t, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double sigma_t )
{
    double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
     double exp_sin_term  = conv_exp_sin(t, G, DM, sigma_t);
     double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);
     double exp_cos_term  = conv_exp_cos(t, G, DM, sigma_t);
    /*
    double exp_cosh_term = exp(-t*G)*cosh(t*DG/2);
    double exp_sin_term  = exp(-t*G)*sin(t*DM);
    double exp_sinh_term = exp(-t*G)*sinh(t*DG/2);
    double exp_cos_term  = exp(-t*G)*cos(t*DM);*/

double A_0_mod_2 = A_0_mod*A_0_mod;
double A_pa_mod_2 = A_pa_mod*A_pa_mod;
double A_pe_mod_2 = A_pe_mod*A_pe_mod;
double A_S_mod_2 = A_S_mod*A_S_mod;
/*
return  2.0*exp_cosh_term* (A_0_mod_2 + A_S_mod_2 + A_pa_mod_2 + A_pe_mod_2)
      + 2.0*exp_sin_term*  (A_0_mod_2*sin(phi_0) - A_S_mod_2*sin(phi_S) + A_pa_mod_2*sin(phi_pa) - A_pe_mod_2*sin(phi_pe))
      + 2.0*exp_sinh_term*(-A_0_mod_2*cos(phi_0) + A_S_mod_2*cos(phi_S) - A_pa_mod_2*cos(phi_pa) + A_pe_mod_2*cos(phi_pe));*/

double landa_0 = 1.;
double landa_S = 1.;
double landa_pe = 1.;
double landa_pa = 1.;

double phis_pe_rot = phi_pe + M_PI;
double phis_S_rot = phi_S + M_PI;

return 1.0*(exp_cos_term*pow(A_0_mod, 2)*(-pow(landa_0, 2) + 1) + exp_cosh_term*pow(A_0_mod, 2)*(pow(landa_0, 2) + 1) + 2.*exp_sin_term*pow(A_0_mod, 2)*landa_0*sin(phi_0) - 2.*exp_sinh_term*pow(A_0_mod, 2)*landa_0*cos(phi_0)) 
+ 1.0*(exp_cos_term*pow(A_S_mod, 2)*(-pow(landa_S, 2) + 1) + exp_cosh_term*pow(A_S_mod, 2)*(pow(landa_S, 2) + 1) + 2.*exp_sin_term*pow(A_S_mod, 2)*landa_S*sin(phis_S_rot) - 2.*exp_sinh_term*pow(A_S_mod, 2)*landa_S*cos(phis_S_rot)) 
+ 1.0*(exp_cos_term*pow(A_pa_mod, 2)*(-pow(landa_pa, 2) + 1) + exp_cosh_term*pow(A_pa_mod, 2)*(pow(landa_pa, 2) + 1) + 2.*exp_sin_term*pow(A_pa_mod, 2)*landa_pa*sin(phi_pa) - 2.*exp_sinh_term*pow(A_pa_mod, 2)*landa_pa*cos(phi_pa)) 
+ 1.0*(exp_cos_term*pow(A_pe_mod, 2)*(-pow(landa_pe, 2) + 1) + exp_cosh_term*pow(A_pe_mod, 2)*(pow(landa_pe, 2) + 1) + 2.*exp_sin_term*pow(A_pe_mod, 2)*landa_pe*sin(phis_pe_rot) - 2.*exp_sinh_term*pow(A_pe_mod, 2)*landa_pe*cos(phis_pe_rot));
}

__device__ double integral4piBbar( double t, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double sigma_t )
{
     double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
     double exp_sin_term  = conv_exp_sin(t, G, DM, sigma_t);
     double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);
     double exp_cos_term  = conv_exp_cos(t, G, DM, sigma_t);
    /*
    double exp_cosh_term = exp(-t*G)*cosh(t*DG/2);
    double exp_sin_term  = exp(-t*G)*sin(t*DM);
    double exp_sinh_term = exp(-t*G)*sinh(t*DG/2);
    double exp_cos_term  = exp(-t*G)*cos(t*DM);*/

double A_0_mod_2 = A_0_mod*A_0_mod;
double A_pa_mod_2 = A_pa_mod*A_pa_mod;
double A_pe_mod_2 = A_pe_mod*A_pe_mod;
double A_S_mod_2 = A_S_mod*A_S_mod;

double phis_pe_rot = phi_pe + M_PI;
double phis_S_rot = phi_S + M_PI;

double landa_0 = 1.;
double landa_S = 1.;
double landa_pe = 1.;
double landa_pa = 1.;
/*
return 2.0*exp_cosh_term* (A_0_mod_2 + A_S_mod_2 + A_pa_mod_2 + A_pe_mod_2)
     + 2.0*exp_sin_term* (-A_0_mod_2*sin(phi_0) + A_S_mod_2*sin(phi_S) - A_pa_mod_2*sin(phi_pa) + A_pe_mod_2*sin(phi_pe))
     + 2.0*exp_sinh_term*(-A_0_mod_2*cos(phi_0) + A_S_mod_2*cos(phi_S) - A_pa_mod_2*cos(phi_pa) + A_pe_mod_2*cos(phi_pe));*/

return -1.0*exp_cos_term*pow(A_0_mod, 2)*(-pow(landa_0, 2) + 1) - 1.0*exp_cos_term*pow(A_S_mod, 2)*(-pow(landa_S, 2) + 1) - 1.0*exp_cos_term*pow(A_pa_mod, 2)*(-pow(landa_pa, 2) + 1) - 1.0*exp_cos_term*pow(A_pe_mod, 2)*(-pow(landa_pe, 2) + 1) + 1.0*exp_cosh_term*pow(A_0_mod, 2)*(pow(landa_0, 2) + 1) + 1.0*exp_cosh_term*pow(A_S_mod, 2)*(pow(landa_S, 2) + 1) + 1.0*exp_cosh_term*pow(A_pa_mod, 2)*(pow(landa_pa, 2) + 1) + 1.0*exp_cosh_term*pow(A_pe_mod, 2)*(pow(landa_pe, 2) + 1) - 2.0*exp_sin_term*pow(A_0_mod, 2)*landa_0*sin(phi_0) - 2.0*exp_sin_term*pow(A_S_mod, 2)*landa_S*sin(phis_S_rot) - 2.0*exp_sin_term*pow(A_pa_mod, 2)*landa_pa*sin(phi_pa) - 2.0*exp_sin_term*pow(A_pe_mod, 2)*landa_pe*sin(phis_pe_rot) - 2.0*exp_sinh_term*pow(A_0_mod, 2)*landa_0*cos(phi_0) - 2.0*exp_sinh_term*pow(A_S_mod, 2)*landa_S*cos(phis_S_rot) - 2.0*exp_sinh_term*pow(A_pa_mod, 2)*landa_pa*cos(phi_pa) - 2.0*exp_sinh_term*pow(A_pe_mod, 2)*landa_pe*cos(phis_pe_rot);
}

__global__ void binnedTimeIntegralB(double *time, double *out, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double sigma_t ){
 int idx = threadIdx.x + blockDim.x * blockIdx.x;
 out[idx] = integral4piB(time[idx], A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0, phi_S, phi_pa, phi_pe, sigma_t);
}

__global__ void binnedTimeIntegralBbar(double *time, double *out, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double sigma_t ){
 int idx = threadIdx.x + blockDim.x * blockIdx.x;
 out[idx] = integral4piBbar(time[idx], A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0,phi_S, phi_pa, phi_pe, sigma_t);
}
