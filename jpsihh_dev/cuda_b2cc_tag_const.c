#include <stdio.h>
#include <math.h>
// #include <thrust/complex.h>
#include <pycuda-complex.hpp>

#define errf_const 1.12837916709551
#define xLim 5.33
#define yLim 4.29
#define eta_OS 0.3791
#define eta_SSK 0.445
#define p0_OS 0.3863
#define p0_SSK 0.450
#define dp0_hf_OS 0.070
#define dp0_hf_SSK -0.079
#define p1_OS 0.982
#define p1_SSK 0.976
#define dp1_hf_OS 0.033
#define dp1_hf_SSK 0.004
#define f_sigma_t 0.244
#define r_offset_pr 1.4207
#define r_offset_sc 0.3778
#define r_slope_pr -2.85
#define r_slope_sc -1.55
#define sigma_t_bar 0.0350 
#define t_0 0.
#define t_1 14.
#define sigma_t_threshold 30.

__device__ double omega(double eta, int isOS)
{
    double om;
    
    if(isOS == 1)
        om = (p0_OS + dp0_hf_OS) + (p1_OS + dp1_hf_OS)*(eta - eta_OS);
    else
        om = (p0_SSK + dp0_hf_SSK) + (p1_SSK + dp1_hf_SSK)*(eta - eta_SSK);
    
    return om;    
}

__device__ double omega_bar(double eta, int isOS)
{
    double om_bar;
    
    if(isOS == 1)
        om_bar = (p0_OS - dp0_hf_OS) + (p1_OS - dp1_hf_OS)*(eta - eta_OS);
    else
        om_bar = (p0_SSK - dp0_hf_SSK) + (p1_SSK - dp1_hf_SSK)*(eta - eta_SSK);
    
    return om_bar;  
}

__device__ pycuda::complex<double> z(double t, double sigma_t, double G, double Dm)
{
    pycuda::complex<double> I(0,1);
    
    return (-I*(t-sigma_t*sigma_t*G) - Dm*sigma_t*sigma_t)/(sigma_t*sqrt(2.));
}

__device__ double delta_1(double sigma_t)
{
    return -sqrt(f_sigma_t/(1.-f_sigma_t))*(r_offset_sc+r_slope_sc*(sigma_t-sigma_t_bar))*sigma_t
                                         + (r_offset_pr+r_slope_pr*(sigma_t-sigma_t_bar))*sigma_t;
}

__device__ double delta_2(double sigma_t)
{
    return sqrt((1.-f_sigma_t)/f_sigma_t)*(r_offset_sc+r_slope_sc*(sigma_t-sigma_t_bar))*sigma_t
                                        + (r_offset_pr+r_slope_pr*(sigma_t-sigma_t_bar))*sigma_t;
}

__device__ pycuda::complex<double> faddeeva(pycuda::complex<double> z, double t) 
{
   double in_real = pycuda::real(z);
   double in_imag = pycuda::imag(z);
   int n, nc, nu;
   double h, q, Saux, Sx, Sy, Tn, Tx, Ty, Wx, Wy, xh, xl, x, yh, y;
   double Rx [33];
   double Ry [33];

   x = fabs(in_real);
   y = fabs(in_imag);

   if (y < yLim && x < xLim) {
      q = (1.0 - y / yLim) * sqrt(1.0 - (x / xLim) * (x / xLim));
      h  = 1.0 / (3.2 * q);
      nc = 7 + int(23.0 * q);
      xl = pow(h, double(1 - nc));
      xh = y + 0.5 / h;
      yh = x;
      nu = 10 + int(21.0 * q);
      Rx[nu] = 0.;
      Ry[nu] = 0.;
      for (n = nu; n > 0; n--){
         Tx = xh + n * Rx[n];
         Ty = yh - n * Ry[n];
         Tn = Tx*Tx + Ty*Ty;
         Rx[n-1] = 0.5 * Tx / Tn;
         Ry[n-1] = 0.5 * Ty / Tn;
         }
      Sx = 0.;
      Sy = 0.;
      for (n = nc; n>0; n--){
         Saux = Sx + xl;
         Sx = Rx[n-1] * Saux - Ry[n-1] * Sy;
         Sy = Rx[n-1] * Sy + Ry[n-1] * Saux;
         xl = h * xl;
      };
      Wx = errf_const * Sx;
      Wy = errf_const * Sy;
   }
   else {
      xh = y;
      yh = x;
      Rx[0] = 0.;
      Ry[0] = 0.;
      for (n = 9; n>0; n--){
         Tx = xh + n * Rx[0];
         Ty = yh - n * Ry[0];
         Tn = Tx * Tx + Ty * Ty;
         Rx[0] = 0.5 * Tx / Tn;
         Ry[0] = 0.5 * Ty / Tn;
      };
      Wx = errf_const * Rx[0];
      Wy = errf_const * Ry[0];
   }

   if (y == 0.) {
      Wx = exp(-x * x);
   }
   if (in_imag < 0.) {
      Wx =   2.0 * exp(y * y - x * x) * cos(2.0 * x * y) - Wx;
      Wy = - 2.0 * exp(y * y - x * x) * sin(2.0 * x * y) - Wy;
      if (in_real > 0.) {
         Wy = -Wy;
      }
   }
   else if (in_real < 0.) {
      Wy = -Wy;
   }

//    printf("=============== \n fad(z) = (%lf,%lf), z = (%lf,%lf), t = %lf \n =============== \n ", Wx, Wy, in_real, in_imag, t);   
   return pycuda::complex<double>(Wx,Wy);
}

__device__ double conv_exp_single(double t, double G, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    if(t>sigma_t_threshold*sigma_t){
        double f = 2.*(sqrt(0.5*M_PI)); 
                   
        return f*exp(-G*t+0.5*G*G*sigma_t_sq);
    }
    else
        return sqrt(0.5*M_PI)*exp(-G*t+0.5*G*sigma_t_sq)*(1.+erf((t-G*sigma_t_sq)/(sigma_t*sqrt(2.)))); 
}

__device__ double conv_exp_cos_single(double t, double G, double Dm, double sigma_t) 
{   
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>sigma_t_threshold*sigma_t){        
        double f  = 2.*(sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*cos(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::real(faddeeva(z,t));
    }
}

__device__ double conv_exp_sin_single(double t, double G, double Dm, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>sigma_t_threshold*sigma_t){
        double f  = 2.*(sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*sin(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return -sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::imag(faddeeva(z,t));
    }
}

__device__ double conv_cosh_single(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp_single(t,G_m_hDG,sigma_t)+conv_exp_single(t,G_p_hDG,sigma_t));
}

__device__ double conv_sinh_single(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp_single(t,G_m_hDG,sigma_t)-conv_exp_single(t,G_p_hDG,sigma_t));
}

__device__ double conv_exp(double t, double G, double sigma_t) 
{
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t);
    
    return f_sigma_t*conv_exp_single(t, G, sigma_t_fcn1) 
    + (1.-f_sigma_t)*conv_exp_single(t, G, sigma_t_fcn2); 
}

__device__ double conv_exp_cos(double t, double G, double Dm, double sigma_t) 
{   
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t);
    
    return f_sigma_t*conv_exp_cos_single(t, G, Dm, sigma_t_fcn1)
    + (1.-f_sigma_t)*conv_exp_cos_single(t, G, Dm, sigma_t_fcn2);
}

__device__ double conv_exp_sin(double t, double G, double Dm, double sigma_t) 
{
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t);
    
    return f_sigma_t*conv_exp_sin_single(t, G, Dm, sigma_t_fcn1)
    + (1.-f_sigma_t)*conv_exp_sin_single(t, G, Dm, sigma_t_fcn2);
}

__device__ double conv_cosh(double t, double G, double DG, double sigma_t) 
{
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t); 
    
    return f_sigma_t*conv_cosh_single(t, G, DG, sigma_t_fcn1) 
    + (1.-f_sigma_t)*conv_cosh_single(t, G, DG, sigma_t_fcn2);
}

__device__ double conv_sinh(double t, double G, double DG, double sigma_t) 
{
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t);
    
    return f_sigma_t*conv_sinh_single(t, G, DG, sigma_t_fcn1) 
    + (1.-f_sigma_t)*conv_sinh_single(t, G, DG, sigma_t_fcn2);
}

__device__ double Nk(double A_0_abs, 
                     double A_S_abs,
                     double A_pa_abs, 
                     double A_pe_abs,
                     double CSP,
                     int k)
{
    double nk;
    switch(k) {
        case 1:  nk = A_0_abs*A_0_abs;
                 break;
        case 2:  nk = A_pa_abs*A_pa_abs;
                 break;
        case 3:  nk = A_pe_abs*A_pe_abs;
                 break;
        case 4:  nk = A_pe_abs*A_pa_abs;
                 break;
        case 5:  nk = A_0_abs*A_pa_abs;
                 break;
        case 6:  nk = A_0_abs*A_pe_abs;
                 break;
        case 7:  nk = A_S_abs*A_S_abs;
                 break;
        case 8:  nk = CSP*A_S_abs*A_pa_abs;
                 break;
        case 9:  nk = CSP*A_S_abs*A_pe_abs;
                 break;
        case 10: nk = CSP*A_S_abs*A_0_abs;
                 break;
        default: printf("Wrong k index in nk, please check code %d\\n", k);
                 return 0.;
    }
    return nk;
}

__device__ double fk(double helcosthetaK, 
                     double helcosthetaL, 
                     double helphi,
                     int k)
{
    double helsinthetaK = sqrt(1. - helcosthetaK*helcosthetaK);
    double helsinthetaL = sqrt(1. - helcosthetaL*helcosthetaL);
//     helphi -= M_PI;
    double helsinphi = sin(-helphi);
    double helcosphi = cos(-helphi);
    
    double fk;
    switch(k) {
        case 1:  fk = helcosthetaK*helcosthetaK*helsinthetaL*helsinthetaL;
                 break;
        case 2:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helcosphi*helcosphi*helsinthetaL*helsinthetaL);
                 break;
        case 3:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helsinphi*helsinphi*helsinthetaL*helsinthetaL);
                 break;
        case 4:  fk = helsinthetaK*helsinthetaK*helsinthetaL*helsinthetaL*helsinphi*helcosphi;
                 break;
        case 5:  fk = sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helcosphi;
                 break;
        case 6:  fk = -sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helsinphi;
                 break;
        case 7:  fk = helsinthetaL*helsinthetaL/3.;
                 break;
        case 8:  fk = 2.*helsinthetaK*helsinthetaL*helcosthetaL*helcosphi/sqrt(6.);
                 break;
        case 9:  fk = -2.*helsinthetaK*helsinthetaL*helcosthetaL*helsinphi/sqrt(6.);
                 break;
        case 10: fk = 2.*helcosthetaK*helsinthetaL*helsinthetaL/sqrt(3.);
                 break;
        default: printf("Wrong k index in fk, please check code %d\\n", k);
                 return 0.;
    }
    return fk;
}

__device__ double ak(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double ak;
    switch(k) {
        case 1:  ak = 0.5*(1.+lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ak = 0.5*(1.+lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ak = 0.5*(1.+lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ak = 0.5*(sin(delta_pe-delta_pa) - lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ak = 0.5*(cos(delta_0-delta_pa) + lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ak = -0.5*(sin(delta_0-delta_pe) - lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ak = 0.5*(1.+lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ak = 0.5*(cos(delta_S-delta_pa) - lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ak = -0.5*(sin(delta_S-delta_pe) + lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ak = 0.5*(cos(delta_S-delta_0) - lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ak, please check code %d\\n", k);
                 return 0.;
    }
    return ak;
}

__device__ double bk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double bk;
    switch(k) {
        case 1:  bk = -lambda_0_abs*cos(phis_0);
                 break;
        case 2:  bk = -lambda_pa_abs*cos(phis_pa);
                 break;
        case 3:  bk = lambda_pe_abs*cos(phis_pe);
                 break;
        case 4:  bk = 0.5*(lambda_pe_abs*sin(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*sin(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  bk = -0.5*(lambda_0_abs*cos(delta_0-delta_pa-phis_0) + lambda_pa_abs*cos(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  bk = 0.5*(lambda_0_abs*sin(delta_0-delta_pe-phis_0) + lambda_pe_abs*sin(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  bk = lambda_S_abs*cos(phis_S);
                 break;
        case 8:  bk = 0.5*(lambda_S_abs*cos(delta_S-delta_pa-phis_S) - lambda_pa_abs*cos(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  bk = -0.5*(lambda_S_abs*sin(delta_S-delta_pe-phis_S) - lambda_pe_abs*sin(delta_pe-delta_S-phis_pe));
                 break;
        case 10: bk = 0.5*(lambda_S_abs*cos(delta_S-delta_0-phis_S) - lambda_0_abs*cos(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in bk, please check code %d\\n", k);
                 return 0.;
    }
    return bk;
}
    
__device__ double ck(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double ck;
    switch(k) {
        case 1:  ck = 0.5*(1.-lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ck = 0.5*(1.-lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ck = 0.5*(1.-lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ck = 0.5*(sin(delta_pe-delta_pa) + lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ck = 0.5*(cos(delta_0-delta_pa) - lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ck = -0.5*(sin(delta_0-delta_pe) + lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ck = 0.5*(1.-lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ck = 0.5*(cos(delta_S-delta_pa) + lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ck = -0.5*(sin(delta_S-delta_pe) - lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ck = 0.5*(cos(delta_S-delta_0) + lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ck, please check code %d\\n", k);
                 return 0.;
    }
    return ck;
}

__device__ double dk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double dk;
    switch(k) {
        case 1:  dk = lambda_0_abs*sin(phis_0);
                 break;
        case 2:  dk = lambda_pa_abs*sin(phis_pa);
                 break;
        case 3:  dk = -lambda_pe_abs*sin(phis_pe);
                 break;
        case 4:  dk = -0.5*(lambda_pe_abs*cos(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*cos(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  dk = -0.5*(lambda_0_abs*sin(delta_0-delta_pa-phis_0) + lambda_pa_abs*sin(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  dk = -0.5*(lambda_0_abs*cos(delta_0-delta_pe-phis_0) + lambda_pe_abs*cos(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  dk = -lambda_S_abs*sin(phis_S);
                 break;
        case 8:  dk = 0.5*(lambda_S_abs*sin(delta_S-delta_pa-phis_S) - lambda_pa_abs*sin(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  dk = -0.5*(-lambda_S_abs*cos(delta_S-delta_pe-phis_S) + lambda_pe_abs*cos(delta_pe-delta_S-phis_pe));
                 break;
        case 10: dk = 0.5*(lambda_S_abs*sin(delta_S-delta_0-phis_S) - lambda_0_abs*sin(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in dk, please check code %d\\n", k);
                 return 0.;
    }
    return dk;
}

__device__ double hk_B( double phis_0,
                        double phis_S,
                        double phis_pa,
                        double phis_pe,
                        double delta_0,
                        double delta_S,
                        double delta_pa,
                        double delta_pe,
                        double lambda_0_abs,
                        double lambda_S_abs,
                        double lambda_pa_abs,
                        double lambda_pe_abs,
                        double G, 
                        double DG, 
                        double Dm, 
                        double sigma_t, 
                        double t,
                        int k)
{
    double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
    double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);
    double exp_cos_term  = conv_exp_cos(t, G, Dm, sigma_t);
    double exp_sin_term  = conv_exp_sin(t, G, Dm, sigma_t);
    
    double hk = 3./(4.*M_PI)
           *(ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cosh_term//cosh(0.5*DG*t)
           + bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sinh_term//sinh(0.5*DG*t)
           + ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cos_term//*cos(Dm*t)
           + dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sin_term);//sin(Dm*t));

    return hk;
}

__device__ double hk_Bbar(  double phis_0,
                            double phis_S,
                            double phis_pa,
                            double phis_pe,
                            double delta_0,
                            double delta_S,
                            double delta_pa,
                            double delta_pe,
                            double lambda_0_abs,
                            double lambda_S_abs,
                            double lambda_pa_abs,
                            double lambda_pe_abs,
                            double G, 
                            double DG, 
                            double Dm, 
                            double sigma_t, 
                            double t,
                            int k)
{
    double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
    double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);
    double exp_cos_term  = conv_exp_cos(t, G, Dm, sigma_t);
    double exp_sin_term  = conv_exp_sin(t, G, Dm, sigma_t);
    
    double hk = 3./(4.*M_PI)
           *(ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cosh_term//cosh(0.5*DG*t)
           + bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sinh_term//sinh(0.5*DG*t)
           - ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cos_term//*cos(Dm*t)
           - dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sin_term);//sin(Dm*t));

    return hk;
}

__device__ double integral4pitimeB( double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, double sigma_t )
{
    double G_sq = G*G;
    double DG_sq = DG*DG;
    double DM_sq = DM*DM;
    
    double A_0_mod_2 = A_0_mod*A_0_mod;
    double A_pa_mod_2 = A_pa_mod*A_pa_mod;
    double A_pe_mod_2 = A_pe_mod*A_pe_mod;
    double A_S_mod_2 = A_S_mod*A_S_mod;
    
    double conv_sin_term_t_0  = conv_exp_sin(t_0, G, DM, sigma_t);
    double conv_cos_term_t_0  = conv_exp_cos(t_0, G, DM, sigma_t);
    double conv_sinh_term_t_0 = conv_sinh(t_0, G, DG, sigma_t);
    double conv_cosh_term_t_0 = conv_cosh(t_0, G, DG, sigma_t);
     
    double conv_sin_term_t_1  = conv_exp_sin(t_1, G, DM, sigma_t);
    double conv_cos_term_t_1  = conv_exp_cos(t_1, G, DM, sigma_t);
    double conv_sinh_term_t_1 = conv_sinh(t_1, G, DG, sigma_t);
    double conv_cosh_term_t_1 = conv_cosh(t_1, G, DG, sigma_t);
    
    double integral = (-2.0*A_0_mod_2*lambda_0_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin(phi_0) 
                        + 8.0*A_0_mod_2*lambda_0_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos(phi_0) 
                        + A_0_mod_2*(DG_sq - 4.0*G_sq)*(pow(lambda_0_abs, 2) - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_0_mod_2*(DM_sq + G_sq)*(pow(lambda_0_abs, 2) + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        + 2.0*A_S_mod_2*lambda_S_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin(phi_S) 
                        - 8.0*A_S_mod_2*lambda_S_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos(phi_S) 
                        + A_S_mod_2*(DG_sq - 4.0*G_sq)*(pow(lambda_S_abs, 2) - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_S_mod_2*(DM_sq + G_sq)*(pow(lambda_S_abs, 2) + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        - 2.0*A_pa_mod_2*lambda_pa_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin(phi_pa) 
                        + 8.0*A_pa_mod_2*lambda_pa_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos(phi_pa) 
                        + A_pa_mod_2*(DG_sq - 4.0*G_sq)*(pow(lambda_pa_abs, 2) - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_pa_mod_2*(DM_sq + G_sq)*(pow(lambda_pa_abs, 2) + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        + 2.0*A_pe_mod_2*lambda_pe_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin(phi_pe) 
                        - 8.0*A_pe_mod_2*lambda_pe_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos(phi_pe) 
                        + A_pe_mod_2*(DG_sq - 4.0*G_sq)*(pow(lambda_pe_abs, 2) - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_pe_mod_2*(DM_sq + G_sq)*(pow(lambda_pe_abs, 2) + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)))
            /((DG_sq - 4*G_sq)*(DM_sq + G_sq)); 

// printf("integral B = %lf for phi_0 = %lf, phi_S = %lf, phi_pa = %lf, phi_pe = %lf \n A_0_mod = %lf, A_S_mod = %lf, A_pa_mod = %lf, A_pe_mod = %lf  \n  sigma_t = %lf\n", integral, phi_0, phi_S, phi_pa, phi_pe, A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, sigma_t);

return integral;
}

__device__ double integral4pitimeBbar( double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, double sigma_t )
{ 
    double G_sq = G*G;
    double DG_sq = DG*DG;
    double DM_sq = DM*DM;
    
    double A_0_mod_2 = A_0_mod*A_0_mod;
    double A_pa_mod_2 = A_pa_mod*A_pa_mod;
    double A_pe_mod_2 = A_pe_mod*A_pe_mod;
    double A_S_mod_2 = A_S_mod*A_S_mod;

    double conv_sin_term_t_0  = conv_exp_sin(t_0, G, DM, sigma_t);
    double conv_cos_term_t_0  = conv_exp_cos(t_0, G, DM, sigma_t);
    double conv_sinh_term_t_0 = conv_sinh(t_0, G, DG, sigma_t);
    double conv_cosh_term_t_0 = conv_cosh(t_0, G, DG, sigma_t);
     
    double conv_sin_term_t_1  = conv_exp_sin(t_1, G, DM, sigma_t);
    double conv_cos_term_t_1  = conv_exp_cos(t_1, G, DM, sigma_t);
    double conv_sinh_term_t_1 = conv_sinh(t_1, G, DG, sigma_t);
    double conv_cosh_term_t_1 = conv_cosh(t_1, G, DG, sigma_t);

    double integral =  (2.0*A_0_mod_2*lambda_0_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin(phi_0) 
                        + 8.0*A_0_mod_2*lambda_0_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos(phi_0) 
                        - A_0_mod_2*(DG_sq - 4.0*G_sq)*(pow(lambda_0_abs, 2) - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_0_mod_2*(DM_sq + G_sq)*(pow(lambda_0_abs, 2) + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        - 2.0*A_S_mod_2*lambda_S_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin(phi_S) 
                        - 8.0*A_S_mod_2*lambda_S_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos(phi_S) 
                        - A_S_mod_2*(DG_sq - 4.0*G_sq)*(pow(lambda_S_abs, 2) - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_S_mod_2*(DM_sq + G_sq)*(pow(lambda_S_abs, 2) + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        + 2.0*A_pa_mod_2*lambda_pa_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin(phi_pa) 
                        + 8.0*A_pa_mod_2*lambda_pa_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos(phi_pa) 
                        - A_pa_mod_2*(DG_sq - 4.0*G_sq)*(pow(lambda_pa_abs, 2) - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_pa_mod_2*(DM_sq + G_sq)*(pow(lambda_pa_abs, 2) + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        - 2.0*A_pe_mod_2*lambda_pe_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin(phi_pe) 
                        - 8.0*A_pe_mod_2*lambda_pe_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos(phi_pe) 
                        - A_pe_mod_2*(DG_sq - 4.0*G_sq)*(pow(lambda_pe_abs, 2) - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_pe_mod_2*(DM_sq + G_sq)*(pow(lambda_pe_abs, 2) + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)))
                /((DG_sq - 4*G_sq)*(DM_sq + G_sq));
    
//   printf("integral Bbar = %lf for phi_0 = %lf, phi_S = %lf, phi_pa = %lf, phi_pe = %lf \n A_0_mod = %lf, A_S_mod = %lf, A_pa_mod = %lf, A_pe_mod = %lf  \n  sigma_t = %lf\n", integral, phi_0, phi_S, phi_pa, phi_pe, A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, sigma_t);

return integral;
}
 
__device__ double integral4piB( double t, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, double sigma_t )
{
     double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
     double exp_sin_term  = conv_exp_sin(t, G, DM, sigma_t);
     double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);

    double A_0_mod_2 = A_0_mod*A_0_mod;
    double A_pa_mod_2 = A_pa_mod*A_pa_mod;
    double A_pe_mod_2 = A_pe_mod*A_pe_mod;
    double A_S_mod_2 = A_S_mod*A_S_mod;

    double num =  2.0*exp_cosh_term* (A_0_mod_2 + A_S_mod_2 + A_pa_mod_2 + A_pe_mod_2)
                + 2.0*exp_sin_term*  (A_0_mod_2*sin(phi_0) - A_S_mod_2*sin(phi_S) + A_pa_mod_2*sin(phi_pa) - A_pe_mod_2*sin(phi_pe))
                + 2.0*exp_sinh_term*(-A_0_mod_2*cos(phi_0) + A_S_mod_2*cos(phi_S) - A_pa_mod_2*cos(phi_pa) + A_pe_mod_2*cos(phi_pe));

    return num/integral4pitimeB( A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0, phi_S, phi_pa, phi_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, sigma_t);
}

__device__ double integral4piBbar( double t, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, double sigma_t )
{
     double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
     double exp_sin_term  = conv_exp_sin(t, G, DM, sigma_t);
     double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);

    double A_0_mod_2 = A_0_mod*A_0_mod;
    double A_pa_mod_2 = A_pa_mod*A_pa_mod;
    double A_pe_mod_2 = A_pe_mod*A_pe_mod;
    double A_S_mod_2 = A_S_mod*A_S_mod;

    double num =  2.0*exp_cosh_term* (A_0_mod_2 + A_S_mod_2 + A_pa_mod_2 + A_pe_mod_2)
                + 2.0*exp_sin_term* (-A_0_mod_2*sin(phi_0) + A_S_mod_2*sin(phi_S) - A_pa_mod_2*sin(phi_pa) + A_pe_mod_2*sin(phi_pe))
                + 2.0*exp_sinh_term*(-A_0_mod_2*cos(phi_0) + A_S_mod_2*cos(phi_S) - A_pa_mod_2*cos(phi_pa) + A_pe_mod_2*cos(phi_pe));

    return num/integral4pitimeBbar( A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0, phi_S, phi_pa, phi_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, sigma_t);
}

__global__ void binnedTimeIntegralB(double *time, double *out, double *sigmat, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, int N)
{
  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  int i;
  int n = N;
  
  out[idx] = 0;
  if (N > 100) 
    n = 100;
  
  for (i=0;i<n;i++) { 
    out[idx] += integral4piB(time[idx], A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0, phi_S, phi_pa, phi_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs,  sigmat[i]);
  }
}

__global__ void binnedTimeIntegralBbar(double *time, double *out, double *sigmat, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, int N)
{
 int idx = threadIdx.x + blockDim.x * blockIdx.x;
 int i;
 int n = N;
 
 if (N > 100) 
    n = 100;
 out[idx] = 0;
 
 for (i=0;i<n;i++) {
    out[idx] += integral4piBbar(time[idx], A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0,phi_S, phi_pa, phi_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, sigmat[i]);
 }
}

__global__ void DiffRate(double *data, 
                         double *out, 
                         double CSP,
                         double A_0_abs,
                         double A_S_abs,
                         double A_pa_abs,
                         double A_pe_abs,
                         double phis_0,
                         double phis_S,
                         double phis_pa,
                         double phis_pe,
                         double delta_S,
                         double delta_pa,
                         double delta_pe,
                         double lambda_0_abs,
                         double lambda_S_abs,
                         double lambda_pa_abs,
                         double lambda_pe_abs,
                         double G, 
                         double DG, 
                         double Dm, 
                         int Nevt)
{
int row = threadIdx.x + blockDim.x * blockIdx.x; //ntuple entry

if (row >= Nevt) { 
    return;
}

int i0 = row*7;// general rule for cuda matrices : index = col + row*N; as it is now, N = 6 (cthk,cthl,cphi,t,q,sigma_t)
int idx = 0 + i0; 
int idy = 1 + i0;
int idz = 2 + i0;
int idt = 3 + i0;
int idsigma_t = 4 + i0;
int idq_OS = 5 + i0;
int idq_SSK = 6 + i0;

double helcosthetaK = data[idx];
double helcosthetaL = data[idy];
double helphi = data[idz];
double t = data[idt];
double sigma_t = data[idsigma_t];
double q_OS = data[idq_OS];
double q_SSK = data[idq_SSK];

double delta_0 = 0.;

double pdf_B = 0.;
double pdf_Bbar = 0.;  

double omega_OS = 0.353;
double omega_bar_OS = 0.353;
double omega_SSK = 0.259;
double omega_bar_SSK = 0.259;
        
double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);
double exp_cos_term  = conv_exp_cos(t, G, Dm, sigma_t);
double exp_sin_term  = conv_exp_sin(t, G, Dm, sigma_t);

for(int k = 1; k <= 10; k++){
    double Nk_term = Nk(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,CSP,k);
    double fk_term = fk(helcosthetaK,helcosthetaL,helphi,k);
    
    double ak_term = ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cosh_term;
    double bk_term = bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sinh_term;
    double ck_term = ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cos_term;
    double dk_term = dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sin_term;
    
    double hk_B = 3./(4.*M_PI)*(ak_term + bk_term + ck_term + dk_term);
    double hk_Bbar = 3./(4.*M_PI)*(ak_term + bk_term - ck_term - dk_term);
    
    pdf_B += Nk_term
             *hk_B
//              *hk_B(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,G,DG,Dm,sigma_t,t,k)
             *fk_term; 
             
    pdf_Bbar += Nk_term
             *hk_Bbar
//                 *hk_Bbar(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,G,DG,Dm,sigma_t,t,k)
                *fk_term;
    }
    
out[row] = (1.+q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))
           *pdf_B/
           integral4pitimeB(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,G,DG,Dm,phis_0,phis_S,phis_pa,phis_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,sigma_t) 
         + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))
           *pdf_Bbar/integral4pitimeBbar(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,G,DG,Dm,phis_0,phis_S,phis_pa,phis_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,sigma_t);
         
           
//             printf("out_row %lf q_OS %lf q_SSK %lf time %lf and sigma %lf\n G %lf DG %lf Dm %lf A0 %lf As %lf Ape %lf Apa %lf\n", out[row], q_OS, q_SSK, t, sigma_t, G, DG, Dm, A_0_abs, A_S_abs, A_pe_abs, A_pa_abs );
//             printf("integral = %lf for flavour %lf  time %lf and sigma %lf\n phi_0 %lf phi_pa %lf phi_pe %lf A0 %lf As %lf Ape %lf Apa %lf\n", integral, q, data[idt], data[idsigma_t], phis_0,phis_pa, phis_pe, A_0_abs, A_S_abs, A_pe_abs, A_pa_abs );
}

