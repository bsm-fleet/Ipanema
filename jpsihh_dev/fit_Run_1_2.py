from tools import initialize
initialize(1)
import pycuda.cumath
from timeit import default_timer as timer
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')
#from tools import plt
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
from math import pi
import pycuda.gpuarray as gpuarray
#from toygen import poissonLL_b as poissonLL
from iminuit import Minuit
from ModelBricks import Parameter, Free, Cat, getGrid
import numpy as np
from PhisModel_Run_1_2 import *
from remove_rnd_dup import remove_random_multies, remove_random
from scipy import interpolate

Model = Badjanak
CSP = CSP_factors
CSP_syst = CSP_factors_spline

from scipy import random as rnd
from ROOT import *
import cPickle
BLOCK_SIZE = 90 #(1 - 1024) For complex the max no. smaller

POLDEP = 0
FIXTAG_RUNI = 1
FIXTAG_RUNII = 0
CARTESIAN = 0

systname = "random_junk"
#systname = "tagging_05_events"
#systname = "RunI_"*(1-FIXTAG_RUNI)+"RunII_"*(1-FIXTAG_RUNII)+"190201"
#systname = "RunII_190201_fixed_tag"
#systname = "RunII_190201_lambda_1"
#systname = "RunII_190201_shared_lambda"
#systname = "RunII_190201_shared_phis"
#systname = "RunII_181205_quadratic_tagging"#Free dp2_OS and p2_OS
#systname = "RunII_181205_2015"
#systname = "RunII_181205_2016"
#systname = "RunII_181205_biased"
#systname = "RunII_181205_unbiased"
#systname = "RunII_181205_polarity_1"
#systname = "RunII_181205_polarity_-1"
#systname = "RunII_181205_nPV1"
#systname = "RunII_181205_nPV2"
#systname = "RunII_181205_nPV3plus"
#systname = "RunII_181205_SSK"
#systname = "RunII_181205_OS"
#systname = "RunII_181205_SSKandOS"
#systname = "RunII_181205_SSK_free"
#systname = "RunII_190225"
#systname = "RunII_190225_fixed_tag"
#systname = "RunII_190225_lambda_1"
#systname = "RunII_190225_shared_lambda"
#systname = "RunII_190225_shared_phis"
print systname

data_syst = cPickle.load(file("syst/RunII_181205_SSKandOS","r"))

tcut = 0.30
#years = ["2011", "2012", "2015", "2016"]
years = ["2015", "2016"]
#years = ["2011","2012"]

trig_cats = ["biased","unbiased"]

Tristan = {"2011":{},"2012":{},"2015":{},"2016":{}}
Tristan["2011"]["unbiased"] = normweights_RunI
Tristan["2011"]["biased"] = normweights_RunI
Tristan["2012"]["unbiased"] = normweights_RunI
Tristan["2012"]["biased"] = normweights_RunI
Tristan["2015"]["unbiased"] = normweights_unbiased_2015
Tristan["2015"]["biased"] = normweights_biased_2015
Tristan["2016"]["unbiased"] = normweights_unbiased_2016
Tristan["2016"]["biased"] = normweights_biased_2016

#Time acceptance parameters
Knots = {"2011":{},"2012":{},"2015":{},"2016":{}}
Knots["2011"]["unbiased"] = [0.]
Knots["2011"]["biased"] = [0.]
Knots["2012"]["unbiased"] = [0.]
Knots["2012"]["biased"] = [0.]
Knots["2015"]["unbiased"] = spline_t_unbiased_knots_2015
Knots["2015"]["biased"] = spline_t_biased_knots_2015
Knots["2016"]["unbiased"] = spline_t_unbiased_knots_2016
Knots["2016"]["biased"] =  spline_t_biased_knots_2016
#print spline_t_unbiased_knots_2016
#print spline_t_biased_knots_2016

SplineMatrix = {"2011":{},"2012":{},"2015":{},"2016":{}}
SplineMatrix["2011"]["unbiased"] = [[0.]]
SplineMatrix["2011"]["biased"] = [[0.]]
SplineMatrix["2012"]["unbiased"] = [[0.]]
SplineMatrix["2012"]["biased"] = [[0.]]
SplineMatrix["2015"]["unbiased"] = spline_pol_c_unbiased_2015
SplineMatrix["2015"]["biased"] = spline_pol_c_biased_2015
SplineMatrix["2016"]["unbiased"] = spline_pol_c_unbiased_2016
SplineMatrix["2016"]["biased"] = spline_pol_c_biased_2016

HistUL = {"2011":{},"2012":{},"2015":{},"2016":{}}
HistUL["2011"]["unbiased"] = ta_bins_ul_ub_2011
HistUL["2011"]["biased"] = ta_bins_ul_b_2011
HistUL["2012"]["unbiased"] = ta_bins_ul_ub_2012
HistUL["2012"]["biased"] = ta_bins_ul_b_2012
HistUL["2015"]["unbiased"] = [0.]
HistUL["2015"]["biased"] = [0.]
HistUL["2016"]["unbiased"] = [0.]
HistUL["2016"]["biased"] = [0.]

HistAcc = {"2011":{},"2012":{},"2015":{},"2016":{}}
HistAcc["2011"]["unbiased"] = ta_acc_ul_ub_2011
HistAcc["2011"]["biased"] = ta_acc_ul_b_2011
HistAcc["2012"]["unbiased"] = ta_acc_ul_ub_2012
HistAcc["2012"]["biased"] = ta_acc_ul_b_2012
HistAcc["2015"]["unbiased"] = [0.]
HistAcc["2015"]["biased"] = [0.]
HistAcc["2016"]["unbiased"] = [0.]
HistAcc["2016"]["biased"] = [0.]

phis_dummy_ = RooRealVar("phis_","phis_",2,0,4)

phis_0_dummy_ = RooRealVar("phis_0_","phis_0_",2,0,4)
phis_pa_dummy_ = RooRealVar("phis_pa_","phis_pa_",2,0,4)
phis_pe_dummy_ = RooRealVar("phis_pe_","phis_pe_",2,0,4)
phis_S_dummy_ = RooRealVar("phis_S_","phis_S_",2,0,4)
DG_dummy_ = RooRealVar("DG_","DG_",2,0,4)

phis_dummy= RooUnblindUniform("phis","phis", "BsPhis20152016", .1, phis_dummy_)

phis_0_dummy = RooUnblindUniform("phis0","phis0", "BsPhiszero20152016", .2, phis_0_dummy_)
phis_pa_dummy = RooUnblindUniform("phispa","phispa", "BsPhisparaDel20152016", .2, phis_pa_dummy_)
phis_pe_dummy = RooUnblindUniform("phispe","phispe", "BsPhisperpDel20152016", .2, phis_pe_dummy_)
phis_S_dummy = RooUnblindUniform("phisS","phisS", "BsPhisSDel20152016", .2, phis_S_dummy_)
DG_dummy = RooUnblindUniform("DG","DG", "BsDGs20152016", .1, DG_dummy_)

phisOff = phis_dummy.getVal()- phis_dummy_.getVal()
phis0Off = phis_0_dummy.getVal()- phis_0_dummy_.getVal()
phispaOff = phis_pa_dummy.getVal()- phis_pa_dummy_.getVal()
phispeOff = phis_pe_dummy.getVal()- phis_pe_dummy_.getVal()
phisSOff = phis_S_dummy.getVal()- phis_S_dummy_.getVal()
DGOff = DG_dummy.getVal() - DG_dummy_.getVal()

def manipulate_parameter(par, factor):
    rand1 = rnd.uniform()
    rand2 = rnd.uniform()
       
    if rand1 > 0.5:
        par = par + factor*rand2
    else:
        par = par - factor*rand2
            
    return par  

   
cats, Params = [], []
from math import pi
initial_fL = 0.5241#0.50#0.52#
initial_fpe = 0.25#0.24#0.2504
initial_phis_0 = -0.03#0.#0.8#
initial_phis_S = -0.03#-0.03
initial_phis_pa = -0.03#-0.03#0.8#
initial_phis_pe = -0.03#-0.03#0.8#
initial_dpa = 3.26#3.1#3.5#1.57
initial_dpe = 3.1#2.6#2.64#5.78#-1.57#-0.1

if(POLDEP == 1):                                   
    Params.append(Free("fL",initial_fL, limits=(0.45,0.6)))            
    Params.append(Free("fpe",initial_fpe, limits=(0.15,0.3)))            
    Params.append(Free("dpa",initial_dpa, limits=(-pi,1.5*pi)))
    Params.append(Free("dpe",initial_dpe, limits=(-pi,1.5*pi)))   
    Params.append(Free("phis_0",initial_phis_0,limits=(-0.5,0.5), blind_offset = phisOff))                                                                                                                          
    Params[-1].blindpoldep =  phis0Off
    Params.append(Free("delta_phis_S_0",0, limits=(-0.5,0.5), blind_offset = phisSOff))#, limits=(-0.5,0.5) , blind_offset = phisOff))                                                                  
    Params.append(Free("delta_phis_pa_0",0.,limits=(-0.5,0.5), blind_offset = phispaOff))#, limits=(-0.5,0.5) , blind_offset = phisOff))                                                                      
    Params.append(Free("delta_phis_pe_0",0., limits=(-0.5,0.5), blind_offset = phispeOff))#, limits=(-0.5,0.5) , blind_offset = phisOff))              
    Params.append(Free("DG",0.080, limits=(-0.2,0.2), blind_offset = DGOff))#, limits=(-0.2,0.4), blind_offset = DGOff))                                                                     
    Params.append(Free("lambda_0_abs",1., limits=(0.7,1.6)))                                                                                                                      
    Params.append(Free("lambda_S_0_abs",1., limits=(0.7,1.6)))                                                                                                                      
    Params.append(Free("lambda_pa_0_abs",1., limits=(0.7,1.6)))                                                                                                                        
    Params.append(Free("lambda_pe_0_abs",1., limits=(0.7,1.6)))                                                  
    Params.append(Free("Gs_m_Gd", 0.0, limits=(-0.1,0.1)))                                                                          
    #Params.append(Free("Gs_m_Gd", 0.6604, limits=(0.6, 0.7)))                                                                          
    Params.append(Free("Dm",17.757, limits=(17.,18.)))
else:                                      
    Params.append(Free("fL",initial_fL, limits=(0.4,0.6)))             
    Params.append(Free("fpe",initial_fpe, limits=(0.15,0.35)))          
    Params.append(Free("dpa",initial_dpa, limits=(0.,2.*pi)))
    Params.append(Free("dpe",initial_dpe, limits=(0.,2.*pi)))              
    Params.append(Free("phis_0",initial_phis_0, limits=(-0.5,0.5), blind_offset = phisOff))                                                                                                                       
    Params[-1].blindpoldep =  phis0Off                    
    Params.append(Free("DG",0.080, limits=(-0.2,0.2), blind_offset = DGOff))#, limits=(-0.2,0.4), blind_offset = DGOff))                                                                            
    Params.append(Free("lambda_0_abs",1., limits=(0.5,1.5)))                                                                                                                            
    Params.append(Free("Gs_m_Gd", 0.0, limits=(-0.1,0.1)))                                                                                                                                                 
    #Params.append(Free("Gs_m_Gd", 0.6604, limits=(0.6, 0.75)))                                                                                                                   
    Params.append(Free("Dm",17.757, limits = (17., 18.5)))     
     
Ntot = 0
Ntot_sig = 0


if CARTESIAN == 0:
    for i in range(1,7):
        ibin = str(i)
        Params.append(Free("Fs_" + ibin, 0., limits=(0.,1.)))
    for i in range(1,7):
        ibin = str(i)
        if i<4:
            Params.append(Free("ds_m_dpe_" + ibin, pi/4., limits=(-10.,10.)))
                #Params.append(Free("ds_m_dpe_" + ibin, pi/4., limits=(-3.*pi,pi)))
        else:
            Params.append(Free("ds_m_dpe_" + ibin, -pi/4., limits=(-10.,10.)))
else:
    for i in range(1,7):
        ibin = str(i)
        if i<4:
            Params.append(Free("xs_" + ibin, -0.2, limits=(-1.,1.)))
            Params.append(Free("ys_" + ibin, -0.2, limits=(-1.,1.)))
        else:
            Params.append(Free("xs_" + ibin, 0., limits=(-1.,1.)))
            Params.append(Free("ys_" + ibin, 0.2, limits=(-1.,1.)))
    
for i in range(1,7):
    ibin = str(i)
    Params.append(Parameter("CSP_f0_" + ibin, CSP[i]))
    Params.append(Parameter("CSP_spl_" + ibin, CSP_syst[i]))   

if(POLDEP == 0):    
    Params.append(Parameter("delta_phis_S_0",0))                                                                  
    Params.append(Parameter("delta_phis_pa_0",0.))                                                                     
    Params.append(Parameter("delta_phis_pe_0",0.))                                                                                                                               
    Params.append(Parameter("lambda_S_0_abs",1.))                                                                                                                   
    Params.append(Parameter("lambda_pa_0_abs",1.))                                                                                                                      
    Params.append(Parameter("lambda_pe_0_abs",1.))    
            
#Tagging parameters
Params.append(Parameter("p2_OS", 0))
Params.append(Parameter("dp2_OS", 0))
#Free for quadratic tagging systematics
#Params.append(Free("p2_OS", 0))
#Params.append(Free("dp2_OS", 0))
        
Params.append(Parameter("eta_bar_OS", eta_bar_OS_cv))#Always fixed                                                                                                                                                 
Params.append(Parameter("p0_OS", p0_OS_cv,limits = (0.,1.),  constant = FIXTAG_RUNII))
Params.append(Parameter("dp0_OS", dp0_OS_cv, limits = (-0.1,0.1),  constant = FIXTAG_RUNII))
Params.append(Parameter("p1_OS", p1_OS_cv,limits = (0.5,1.5),  constant = FIXTAG_RUNII))
Params.append(Parameter("dp1_OS", dp1_OS_cv,limits = (-0.1,0.1),  constant = FIXTAG_RUNII))

Params.append(Parameter("eta_bar_SSK", eta_bar_SSK_cv))#Always fixed                                                                                                                                               
Params.append(Parameter("p0_SSK", p0_SSK_cv , limits = (0.,2.),  constant = FIXTAG_RUNII))#(.2,.6)))                                                                                                          
#Params.append(Parameter("p0_SSK", p0_SSK_cv, constant = FIXTAG_RUNII))#(.2,.6)))
Params.append(Parameter("dp0_SSK", dp0_SSK_cv,limits = (-0.1,0.1),  constant = FIXTAG_RUNII))
#Params.append(Parameter("dp0_SSK", dp0_SSK_cv, constant = FIXTAG_RUNII))
Params.append(Parameter("p1_SSK", p1_SSK_cv, limits = (0.,2.),  constant = FIXTAG_RUNII))#(.7,1.3)))
#Params.append(Parameter("p1_SSK", p1_SSK_cv, constant = FIXTAG_RUNII))#(.7,1.3)))
Params.append(Parameter("dp1_SSK", dp1_SSK_cv, limits = (-0.1,0.1),  constant = FIXTAG_RUNII))
#Params.append(Parameter("dp1_SSK", dp1_SSK_cv, constant = FIXTAG_RUNII))
        
Params.append(Parameter("eta_bar_OS_RunI", eta_bar_OS_RunI_cv))#Always fixed                                                                                                                                                 
Params.append(Parameter("p0_OS_RunI", p0_OS_RunI_cv,limits = (0.,1.),  constant = FIXTAG_RUNI))
Params.append(Parameter("dp0_OS_RunI", dp0_OS_RunI_cv, limits = (-0.1,0.1),  constant = FIXTAG_RUNI))
Params.append(Parameter("p1_OS_RunI", p1_OS_RunI_cv,limits = (0.5,1.5),  constant = FIXTAG_RUNI))
Params.append(Parameter("dp1_OS_RunI", dp1_OS_RunI_cv,limits = (-0.2,0.2),  constant = FIXTAG_RUNI))

Params.append(Parameter("eta_bar_SSK_RunI", eta_bar_SSK_RunI_cv))#Always fixed                                                                                                                                               
Params.append(Parameter("p0_SSK_RunI", p0_SSK_RunI_cv , limits = (0.,2.),  constant = FIXTAG_RUNI))#(.2,.6)))
Params.append(Parameter("dp0_SSK_RunI", dp0_SSK_RunI_cv,limits = (-0.1,0.1),  constant = FIXTAG_RUNI))
Params.append(Parameter("p1_SSK_RunI", p1_SSK_RunI_cv, limits = (0.,2.),  constant = FIXTAG_RUNI))#(.7,1.3)))
Params.append(Parameter("dp1_SSK_RunI", dp1_SSK_RunI_cv, limits = (-0.2,0.2),  constant = FIXTAG_RUNI))

#Time resolution parameters
Params.append(Parameter("sigma_t_a", sigma_t_a))
Params.append(Parameter("sigma_t_b", sigma_t_b))
Params.append(Parameter("sigma_t_c", sigma_t_c))
  
#Mean offset parameters (all zero in nominal fit)
Params.append(Parameter("sigma_t_mu_a", sigma_t_mu_a))
Params.append(Parameter("sigma_t_mu_b", sigma_t_mu_b))
Params.append(Parameter("sigma_t_mu_c", sigma_t_mu_c))

#For Csp factor systematics
Params.append(Parameter("sigma_0", 0.))

#for seed in xrange(366,1000):
for seed in xrange(1):
    cats = []    
    for year in years:    
        for trig in trig_cats:
            for i in range(1,7):
                ibin = str(i)
                cat_name = "Cat_" + str(year) + "_" + str(trig) + "_" + ibin
                
                #These use Run I taggers: using fitParameters_201{5,6}_180615.py
                #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_" + trig + "_" + ibin +".ext"
                if("5" in year or "6" in year):
                    #These use Run I taggers: using fitParameters_201{5,6}_RunI_tagging.py
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunI_taggers_" + trig + "_" + ibin +".ext"
                    #These use Run II taggers
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_taggers_" + trig + "_" + ibin +".ext"
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_tagging_20190123_" + trig + "_" + ibin +".ext"
                    filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_20190225_" + trig + "_" + ibin +".ext"
                    #POLARITY = 1
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_tagging_" + trig + "_POLARITY_1_" + ibin +".ext"
                    #POLARITY = -1
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_tagging_" + trig + "_POLARITY_-1_" + ibin +".ext"
                    #nPV=1
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_tagging_" + trig + "_nPV_1_" + ibin +".ext"
                    #nPV=2
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_tagging_" + trig + "_nPV_2_" + ibin +".ext"
                    #nPV>3
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_tagging_" + trig + "_nPV_3plus_" + ibin +".ext"
                    #SSK
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_tagging_" + trig + "_SSK_" + ibin +".ext"
                    #OS
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_tagging_" + trig + "_OS_" + ibin +".ext"
                    #SSK&OS
                    #filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_RunII_tagging_" + trig + "_SSKandOS_" + ibin +".ext"
                else: 
                    filename = "/scratch28/veronika/BsJpsiKK/data" + str(year) + "ForFit_" + trig + "_" + ibin +".ext"
                ## Define category for i-th bin
                thiscat = Cat(cat_name, filename, getN = True)
                
                sweights = np.float64(thiscat.data.get()[:,9])
                #scale = sum(sweights)*1./sum(sweights*sweights)
                thiscat.sumW_raw = sum(sweights)
                scale = thiscat.sumW_raw/sum(sweights*sweights)
                thiscat.weights = scale*gpuarray.to_gpu(np.float64(sweights))

                thiscat.tristan = gpuarray.to_gpu(np.float64(Tristan[year][trig]))
                thiscat.knots = gpuarray.to_gpu(np.float64(Knots[year][trig]))
                thiscat.spline_coeffs = gpuarray.to_gpu(np.float64(SplineMatrix[year][trig]))
                thiscat.low_time_acc_bins_ul = gpuarray.to_gpu(np.float64(HistUL[year][trig]))
                thiscat.low_time_acc = gpuarray.to_gpu(np.float64(HistAcc[year][trig]))
                #print thiscat.spline_coeffs
                
                thiscat.wNorm = np.float64(gpuarray.sum(thiscat.weights).get())
                
                print filename
                print "sum(sw)", i, sum(sweights)
                print "scale", i, scale
                print "Nevts", i, thiscat.Nevts
                print "wNorm", i, thiscat.wNorm
                Ntot+=thiscat.Nevts
                Ntot_sig+=thiscat.wNorm

                thiscat.bin = i
                thiscat.ibin = str(i)
                thiscat.block = (BLOCK_SIZE,1,1)
                thiscat.grid = getGrid(thiscat.Nevts, BLOCK_SIZE)
                cats.append(thiscat) 

    print "Ntot", Ntot 
    print "Nsig", Ntot_sig

    start = timer()
    manager = Model(Params, cats, weighted = 1, blind = 0, poldep = POLDEP, cartesian = CARTESIAN)
    manager.createFit()

    #manager.setPars2fitSummary(data_syst)
    #manager.run_with_vals()
    #EXIT

    manager.fit.set_strategy(1)
    manager.fit.tol = 0.5
    manager.fit.migrad()
    while manager.fit.get_fmin()["is_valid"] == False:
        manager.fit.migrad()
    try: manager.fit.hesse()
    except RuntimeError:
        manager.fit.migrad()
        manager.fit.hesse()
    #manager.camaron("phis_0")
    #manager.migros(["phis_0"])
    #manager.fit.minos("phis_0", 50000)
    #manager.fit.minos(maxcall = 10000)
    manager.fitSummary().corr = manager.fit.matrix(correlation=True)
    #print manager.fitSummary().C.tolist()
    #print manager.fitSummary().corr.tolist()
    #print manager.fit.latex_matrix().__str__()
        
    #########################
    #Asymmetric errors with MIGROS
    #migros = manager.migros()
    #nominal_asym_cv = {}
    #nominal_asym_err_down = {}
    #nominal_asym_err_up = {}
    #for m in migros:
        #name = m["name"]
        #nominal_asym_cv[name] = m[name]['min']
        #nominal_asym_err_down[name] = m[name]['lower']
        #nominal_asym_err_up[name] = m[name]['upper']

    #print nominal_asym_cv
    #print nominal_asym_err_down
    #print nominal_asym_err_up
    #########################

if CARTESIAN:
    x,y,xerr,yerr,x_p_d0,y_p_d0,xerr_p_d0,yerr_p_d0 = manager.ArgandPlot()
    #print "x =", x
    #print "y =", y
    print "x =", x_p_d0
    print "y =", y_p_d0
    print "x_err =", xerr_p_d0
    print "y_err =", yerr_p_d0
#print manager.fitSummary().values["Dm"]
print "Elapsed time:", timer() - start
for i in range(len(manager.fit.get_param_states())):
    print manager.fit.get_param_states()[i].name, "\t\t=\t", manager.fit.get_param_states()[i].value, "\t\t+/-\t", manager.fit.get_param_states()[i].error
#print manager.fit.np_matrix(correlation=True)

if POLDEP:
    manager.saveFitSummary("syst/"+systname+"_pol_dep")
else:
    manager.saveFitSummary("syst/"+systname)


def set_pars_to_fit_result(result=manager.fitSummary().values):
    for i in range(len(Params)):  
        Params[i].setVal(result[Params[i].name])
 
def before_scan(par_name, value):
    for i in range(len(Params)):  
        if Params[i].name == par_name:
            Params[i].constant = True
            Params[i].setVal(value)
            
def after_scan(par_name, value):
    for i in range(len(Params)):  
        if Params[i].name == par_name:
            Params[i].constant = False
            Params[i].setVal(value)
            
def par_scan(par_start, par_range, par_name, start_val, par_bins, filename, bl_off = 0):    
    scan = []
    x = []

    for j in range(par_bins):
        
        x.append(par_start + j*par_range/par_bins)      
        #x.append(par_start + bl_off + j*par_range/par_bins)

        before_scan(par_name, x[j])
        manager.createFit()
        try:
            manager.fit.migrad()
        except np.linalg.linalg.LinAlgError as err:
            if 'Singular matrix' in str(err):
                continue
            else: 
                raise
        scan.append(manager.fit.get_fmin()['fval'])
        result_vals = manager.fitSummary().values
        #set_pars_to_fit_result(result_vals)
        set_pars_to_fit_result()
        cPickle.dump([x,scan],file(filename,'w'))
        
    after_scan(par_name, start_val) 
    
    return x, scan  
def find_minimum(x, y):
    
    y_min = y[0]
    i_min = 0
    for i in range(1,len(y)):
        #print i, i_min
        #print y[i], y_min
        if y_min > y[i]:
            y_min = y[i]
            i_min = i
            
    return x[i_min], y[i_min], i_min

def find_one_sigma_interval(x, y):
    x_start, y_start, i_start = find_minimum(x, y)
    dL_left = 0
    dL_right = 0
    i_right = i_start
    i_left = i_start
        
    #print x_start, y_start, i_start
            
    while dL_left < 1.:
        i_left -= 1
        if(i_left) <= 0:
            break
        dL_left = y[i_left] - y_start
        
    while dL_right < 1.:
        i_right += 1
        if(i_right) > len(y)-1:
            break
        dL_right = y[i_right] - y_start
            
    print x_start, "-", x_start-x[i_left], "+", x[i_right]-x_start
    print y_start , "-", dL_left, "+", dL_right 
 
def interpolate_one_sigma_interval(x, y):
    x_min, y_min, imin = find_minimum(x,y)
    f_l = interpolate.interp1d(y[:imin], x[:imin])
    f_u = interpolate.interp1d(y[imin:], x[imin:])
    print x_min, f_l(y_min+1.) - x_min , "+", f_u(y_min+1.) - x_min
    #return f_l(y_min+1.), f_u(y_min+1.)

#EXIT
bins = 100
#x, y = par_scan(-0.22, 0.28,    "phis_0",       0., bins, "./scans/phis_0_RunII.ext")
#x, y = par_scan(0.052, 0.05,    "DG",           0., bins, "./scans/DG_RunII.ext")
#x, y = par_scan(-0.012, 0.016,  "Gs_m_Gd",      0., bins, "./scans/Gs-Gd_RunII.ext")
#x, y = par_scan(0.94, 0.12,     "lambda_0_abs", 0., bins, "./scans/lambda_0_abs_RunII.ext") 
#x, y = par_scan(17.5, 0.4,      "Dm",           0., bins, "./scans/Dm_RunII.ext")
#x, y = par_scan(0.51, 0.02,     "fL",           0., bins, "./scans/fL_RunII.ext")
#x, y = par_scan(0.23, 0.03,     "fpe",          0., bins, "./scans/fpe_RunII.ext")
#x, y = par_scan(2.85, 0.55,     "dpa",          0., bins, "./scans/dpa_RunII.ext")
#x, y = par_scan(2.2, 0.9,       "dpe",          0., bins, "./scans/dpe_RunII.ext")
#x, y = par_scan(0.35, 0.3,      "Fs_1",          0., bins, "./scans/Fs1_RunII.ext")
#x, y = par_scan(0.015, 0.06,    "Fs_2",          0., bins, "./scans/Fs2_RunII.ext")
#x, y = par_scan(0., 0.017,      "Fs_3",          0., bins, "./scans/Fs3_RunII.ext")
#x, y = par_scan(0., 0.03,       "Fs_4",          0., bins, "./scans/Fs4_RunII.ext")
#x, y = par_scan(0.02, 0.10,     "Fs_5",          0., bins, "./scans/Fs5_RunII.ext")
#x, y = par_scan(0.08, 0.14,     "Fs_6",          0., bins, "./scans/Fs6_RunII.ext")
#x, y = par_scan(0.5, 2.5,        "ds_m_dpe_1",   0., bins, "./scans/ds_m_dpe_1_RunII.ext")
#x, y = par_scan(0.68, 1.8,      "ds_m_dpe_2",   0., bins, "./scans/ds_m_dpe_2_RunII.ext")
#x, y = par_scan(0.2, 2.4,       "ds_m_dpe_3",   0., bins, "./scans/ds_m_dpe_3_RunII.ext")
#x, y = par_scan(-2., 2.5,       "ds_m_dpe_4",   0., bins, "./scans/ds_m_dpe_4_RunII.ext")
#x, y = par_scan(-1., 0.8,       "ds_m_dpe_5",   0., bins, "./scans/ds_m_dpe_5_RunII.ext")
#x, y = par_scan(-1.8, 1.1,       "ds_m_dpe_6",   0., bins, "./scans/ds_m_dpe_6_RunII.ext")
#EXIT
#x, y = par_scan(-0.15, 0.2, "phis_0", 0., 300)
#cPickle.dump([x,y],file("./scans/phis_0_RunI.ext",'w'))
#x, y = par_scan(-0.01, 0.012, "Gs_m_Gd", 0., 30)
#cPickle.dump([x,y],file("./scans/Gs-Gd_RunII.ext",'w'))
#EXIT
print "Limits from scan"
#find_one_sigma_interval(x,y)
print "Interpolated limits from scan"
#interpolate_one_sigma_interval(x, y)
#-0.06 - 0.0513333333333 + 0.0506666666667
#846170.486273 - 1.00409760606 + 1.00283658551
#-0.06 -0.0512283683033 + 0.0505938076558

manager.setPars2bfp()
labels0 = manager.Params.keys()# + ["chi2"]
labels = []
from RTuple import RTuple

for shit in labels0:
    labels.append(shit + "_gen/F")
    labels.append(shit + "_polind_val/F")
    labels.append(shit + "_poldep_val/F")
    labels.append(shit + "_polind_HesseErr/F")
    labels.append(shit + "_poldep_HesseErr/F")
labels += ["chi2_polind/D", "chi2_poldep/D"]
tup = RTuple("./toymc/BabaTestToys" + str(rnd.random()), labels)

def do(N):
    for i in range(N):
        print i*100./N
        manager.setPars2bfp()
        gens = manager.fit.values
        toycats = manager.generate_cats()
        mntoys = Model(Params, toycats, weighted = 0, blind = 1)
        mntoys.createFit()
        mntoys.fit.migrad()
        try:
            mntoys.fit.hesse()
        except RuntimeError:
            mntoys.fit.migrad()
            mntoys.fit.hesse()
        polindvals = mntoys.fit.values
        polinderrs = mntoys.fit.errors
        chi21 = mntoys.fit.get_fmin()["fval"]
        mntoys.setPolDep(True)
        mntoys.createFit()
        mntoys.fit.migrad()
        try:
            mntoys.fit.hesse()
        except RunTimeError:
            mntoys.fit.migrad()
            mntoys.fit.hesse()

        poldepvals = mntoys.fit.values
        poldeperrs = mntoys.fit.errors
        
        for shit in labels0:
            tup.fillItem(shit + "_gen", gens[shit])
            tup.fillItem(shit + "_polind_val", polindvals[shit])
            tup.fillItem(shit + "_polind_HesseErr", polinderrs[shit])
            tup.fillItem(shit + "_poldep_val", poldepvals[shit])
            tup.fillItem(shit + "_poldep_HesseErr", poldeperrs[shit])
        tup.fillItem("chi2_polind", chi21)
        tup.fillItem("chi2_poldep", mntoys.fit.get_fmin()["fval"])
        tup.fill()
        for cat in toycats:
            cat.data = []
            cat.tristan = []
            cat.Probs = []
            cat.weights = []
            cat.knots = []
            
    tup.close()
 
def do_toymc_data(N):
    labels = ["cosThetaKRef/F", "cosThetaLRef/F", "phiHelRef/F", "time/F", "sigmat/F", "tag_OS/F", "tag_SS/F", "omega_OS/F", "omega_SS/F", "hltB/F", "year/F", "MKK_bin/F"]
    tup = RTuple("./toymc/BabaToysKatya", labels)
    for i in range(N):
        print i*100./N
        manager.setPars2bfp()
        toycats = manager.generate_cats()
        
        for cat in toycats:
            data = cat.data.get()
            ibin = cat.ibin
            
            cosThetaKRef = np.float64(data[:,0])
            cosThetaLRef = np.float64(data[:,1])
            phiHelRef = np.float64(data[:,2])
            time = np.float64(data[:,3])
            tag_OS = np.float64(data[:,5])
            tag_SS = np.float64(data[:,6])
            omega_OS = np.float64(data[:,7])
            omega_SS = np.float64(data[:,8])
            hltB = np.float64(data[:,11])
            year = np.float64(data[:,12])
            
            print len(year)
            print ibin
               
            for j in xrange(len(year)):
                tup.fillItem("cosThetaKRef", cosThetaKRef[j])
                tup.fillItem("cosThetaLRef", cosThetaLRef[j])
                tup.fillItem("phiHelRef", phiHelRef[j])
                tup.fillItem("time", time[j])
                tup.fillItem("sigmat", 0.04554)
                tup.fillItem("tag_OS", tag_OS[j])
                tup.fillItem("tag_SS", tag_SS[j])
                tup.fillItem("omega_OS", omega_OS[j])
                tup.fillItem("omega_SS", omega_SS[j])
                tup.fillItem("hltB", hltB[j])
                tup.fillItem("year", year[j])
                tup.fillItem("MKK_bin", float(ibin))
                tup.fill()
        
        for cat in toycats:
            cat.data = []
            cat.tristan = []
            cat.Probs = []
            cat.weights = []
            cat.knots = []
            
    tup.close() 
 
do_toymc_data(100) 
EXIT
 
def compare(col):
    shit = toycats[20].data.get()
    shit2 = cats[20].data.get()
    htoy,x = np.histogram(shit[:,col], bins = 100)
    hdat,xp = np.histogram(shit2[:,col], weights = cats[20].weights.get(), bins = 100)
    hdat*= np.sum(htoy)/np.sum(hdat)
    x = (xp[:-1] + xp[1:]) / 2
    
    plt.bar(x, htoy, width = np.diff(xp))
    plt.bar(x,hdat, width = np.diff(xp), color = ["none"], edgecolor = ["red"])  
    
start = timer()
do(500)
#toycats = manager.generate_cats()
#compare(0)
#plt.show()  
print timer() - start

EXIT

result_vals = manager.fitSummary().values

while manager.fit.get_fmin()["is_valid"] == False:# 2e-04:
    set_pars_to_fit_result(result_vals)
    manager.fit.migrad()
manager.fit.hesse()
EXIT
        
while manager.fit.get_fmin()["is_valid"] == False:# 2e-04:
    if Params[i].name == "fL": Params[i].setVal(manipulate_parameter(initial_fL, 0.001))
    if Params[i].name == "fpe": Params[i].setVal(manipulate_parameter(initial_pe, 0.001))
    manager.fit.migrad()
BREAK
manager.fit.minos(maxcall = 10000)

print timer() - start
for i in range(len(manager.fit.get_param_states())):
    print manager.fit.get_param_states()[i].name, "\t\t=\t", manager.fit.get_param_states()[i].value, "\t\t+/-\t", manager.fit.get_param_states()[i].error
EXIT
