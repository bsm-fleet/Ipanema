#include <stdio.h>
//#include <math.h>
// #include <thrust/complex.h>
#include <pycuda-complex.hpp>

#define errf_const 1.12837916709551
#define xLim 5.33
#define yLim 4.29 

#define eta_bar_OS 0.384
#define eta_bar_SSK 0.42
#define p0_OS 0.389
#define p0_SSK 0.4314
#define dp0_hf_OS 0.00435
#define dp0_hf_SSK 0.
#define p1_OS 0.9093
#define p1_SSK 0.8874
#define dp1_hf_OS -0.00315
#define dp1_hf_SSK 0.
#define sigma_t_a_RunII -6.
#define sigma_t_b_RunII 1.539
#define sigma_t_c_RunII -0.004133
#define f_sigma_t 0.244
#define r_offset_pr 1.4207
#define r_offset_sc 0.3778
#define r_slope_pr -2.85
#define r_slope_sc -1.55
#define sigma_t_bar 0.0350 
#define t_ll 0.2

#define sigma_t_threshold 20.
#define time_acc_bins 40


 
__device__ double normweights[10] = {1., 1.0322, 1.0311, -0.00032, 0.00338, -0.0009, 1.01090, 0.00058, 0.00034, -0.0010};
  
__device__ double low_time_acc_bins_ll[time_acc_bins] = {0.2, 0.23797471677166174, 0.27693584616386924, 0.31693600305260117, 0.35803212761855635, 0.40028597282970174, 0.4437646625969446, 0.48854133326607896, 0.5346958738379816, 0.5823157837313284, 0.6314971712226232, 0.6823459211913355, 0.7349790678339851, 0.7895264170955251, 0.8461324753957553, 0.9049587567724324, 0.966186561187383, 1.0300203443952365, 1.0966918372716514, 1.1664651239654924, 1.2396429598202376, 1.3165747110027106, 1.3976664425174057, 1.483393892317744, 1.5743193827952986, 1.6711141967271264, 1.7745886837810803, 1.8857335419899877, 2.0057776537213834, 2.1362711425364616, 2.279208120728858, 2.43721431642301, 2.613845648787718, 2.814087169543123, 3.045239101577547, 3.3186177201267117, 3.653177485580561, 4.084441360908032, 4.692120690735037, 5.730287538377153};

__device__ double low_time_acc_bins_ul[time_acc_bins] = 
{0.23797471677166174, 0.27693584616386924, 0.31693600305260117, 0.35803212761855635, 0.40028597282970174, 0.4437646625969446, 0.48854133326607896, 0.5346958738379816, 0.5823157837313284, 0.6314971712226232, 0.6823459211913355, 0.7349790678339851, 0.7895264170955251, 0.8461324753957553, 0.9049587567724324, 0.966186561187383, 1.0300203443952365, 1.0966918372716514, 1.1664651239654924, 1.2396429598202376, 1.3165747110027106, 1.3976664425174057, 1.483393892317744, 1.5743193827952986, 1.6711141967271264, 1.7745886837810803, 1.8857335419899877, 2.0057776537213834, 2.1362711425364616, 2.279208120728858, 2.43721431642301, 2.613845648787718, 2.814087169543123, 3.045239101577547, 3.3186177201267117, 3.653177485580561, 4.084441360908032, 4.692120690735037, 5.730287538377153, 15.0};

__device__ double low_time_acc_all[time_acc_bins] = {0.2634384036064148, 0.3077799677848816, 0.3243389427661896, 0.3367297947406769, 0.3454984724521637, 0.351309597492218, 0.3549683690071106, 0.35744380950927734, 0.35949477553367615, 0.36121028661727905, 0.3626016676425934, 0.36368557810783386, 0.3644851744174957, 0.3650318384170532, 0.36536726355552673, 0.36554619669914246, 0.365639865398407, 0.365723580121994, 0.36581581830978394, 0.36591747403144836, 0.3660294711589813, 0.3661530315876007, 0.3662893772125244, 0.3664401173591614, 0.36660704016685486, 0.3667922616004944, 0.3669983744621277, 0.3672284483909607, 0.36748626828193665, 0.3677765130996704, 0.36810508370399475, 0.36847949028015137, 0.3689095377922058, 0.3694082498550415, 0.36999329924583435, 0.37068918347358704, 0.37152960896492004, 0.3725546896457672, 0.373737096786499, 0.3596302568912506};

__device__ double get_ta (double t,double G,double DG) { return exp(-G*t)*cosh(DG*t/2); }
__device__ double get_tb (double t,double G,double DG) { return exp(-G*t)*sinh(DG*t/2); }
__device__ double get_tc (double t,double G,double DM) { return exp(-G*t)*cos(DM*t); }
__device__ double get_td (double t,double G,double DM) { return exp(-G*t)*sin(DM*t); }

__device__ double get_int_ta(double t_0, double t_1, double G,double DG) 
{ return (2*(DG*sinh(.5*DG*t_0) + 2*G*cosh(.5*DG*t_0))*exp(G*t_1)
- 2*(DG*sinh(.5*DG*t_1) + 2*G*cosh(.5*DG*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(-pow(DG, 2) + 4 *pow(G, 2));
}
__device__ double get_int_tb(double t_0, double t_1,double G,double DG) 
{
return (2*(DG*cosh(.5*DG*t_0) + 2*G*sinh(.5*DG*t_0))*exp(G*t_1) 
- 2*(DG*cosh(.5*DG*t_1) + 2*G*sinh(.5*DG*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(-pow(DG, 2) + 4*pow(G, 2));
}
__device__ double get_int_tc(double t_0, double t_1,double G,double DM)
{ return ((-DM*sin(DM*t_0) + G*cos(DM*t_0))*exp(G*t_1) + (DM*sin(DM*t_1) 
- G*cos(DM*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(pow(DM, 2) + pow(G, 2));
}

__device__ double get_int_td(double t_0, double t_1,double G,double DM)
{
return ((DM*cos(DM*t_0) + G*sin(DM*t_0))*exp(G*t_1) - (DM*cos(DM*t_1) 
+ G*sin(DM*t_1))*exp(G*t_0))*exp(-G*(t_0 + t_1))/(pow(DM, 2) + pow(G, 2));
}

__device__ double get_time_acc(double time)
{    
    int i;
    for(i = 0; i < time_acc_bins; i++)
    {
        if(time < low_time_acc_bins_ul[i])
            break;
    }
    
    double acc;    
    acc = low_time_acc_all[i];
    
    return acc;
}

__device__ double omega(double eta, int isOS)
{
    double om;
    
    if(isOS == 1)
        om = (p0_OS + dp0_hf_OS) + (p1_OS + dp1_hf_OS)*(eta - eta_bar_OS);
    else
        om = (p0_SSK + dp0_hf_SSK) + (p1_SSK + dp1_hf_SSK)*(eta - eta_bar_SSK);
    
    return om;    
}

__device__ double omega_bar(double eta, int isOS)
{
    double om_bar;
    
    if(isOS == 1)
        om_bar = (p0_OS - dp0_hf_OS) + (p1_OS - dp1_hf_OS)*(eta - eta_bar_OS);
    else
        om_bar = (p0_SSK - dp0_hf_SSK) + (p1_SSK - dp1_hf_SSK)*(eta - eta_bar_SSK);
    
    return om_bar;  
}

__device__ pycuda::complex<double> z(double t, double sigma_t, double G, double Dm)
{
    pycuda::complex<double> I(0,1);
    
    return (-I*(t-sigma_t*sigma_t*G) - Dm*sigma_t*sigma_t)/(sigma_t*sqrt(2.));
}

__device__ double delta_RunII(double sigma_t)
{
    return sigma_t_a_RunII*sigma_t*sigma_t + sigma_t_b_RunII*sigma_t + sigma_t_c_RunII;
}

__device__ pycuda::complex<double> faddeeva(pycuda::complex<double> z)//, double t) 
{
   double in_real = pycuda::real(z);
   double in_imag = pycuda::imag(z);
   int n, nc, nu;
   double h, q, Saux, Sx, Sy, Tn, Tx, Ty, Wx, Wy, xh, xl, x, yh, y;
   double Rx [33];
   double Ry [33];

   x = fabs(in_real);
   y = fabs(in_imag);

   if (y < yLim && x < xLim) {
      q = (1.0 - y / yLim) * sqrt(1.0 - (x / xLim) * (x / xLim));
      h  = 1.0 / (3.2 * q);
      nc = 7 + int(23.0 * q);
//       xl = pow(h, double(1 - nc));
      double h_inv = 1./h;
      xl = h_inv;
      for(int i = 1; i < nc-1; i++)
          xl *= h_inv;
      
      xh = y + 0.5 / h;
      yh = x;
      nu = 10 + int(21.0 * q);
      Rx[nu] = 0.;
      Ry[nu] = 0.;
      for (n = nu; n > 0; n--){
         Tx = xh + n * Rx[n];
         Ty = yh - n * Ry[n];
         Tn = Tx*Tx + Ty*Ty;
         Rx[n-1] = 0.5 * Tx / Tn;
         Ry[n-1] = 0.5 * Ty / Tn;
         }
      Sx = 0.;
      Sy = 0.;
      for (n = nc; n>0; n--){
         Saux = Sx + xl;
         Sx = Rx[n-1] * Saux - Ry[n-1] * Sy;
         Sy = Rx[n-1] * Sy + Ry[n-1] * Saux;
         xl = h * xl;
      };
      Wx = errf_const * Sx;
      Wy = errf_const * Sy;
   }
   else {
      xh = y;
      yh = x;
      Rx[0] = 0.;
      Ry[0] = 0.;
      for (n = 9; n>0; n--){
         Tx = xh + n * Rx[0];
         Ty = yh - n * Ry[0];
         Tn = Tx * Tx + Ty * Ty;
         Rx[0] = 0.5 * Tx / Tn;
         Ry[0] = 0.5 * Ty / Tn;
      };
      Wx = errf_const * Rx[0];
      Wy = errf_const * Ry[0];
   }

   if (y == 0.) {
      Wx = exp(-x * x);
   }
   if (in_imag < 0.) {

      double exp_x2_y2 = exp(y * y - x * x);
      Wx =   2.0 * exp_x2_y2 * cos(2.0 * x * y) - Wx;
      Wy = - 2.0 * exp_x2_y2 * sin(2.0 * x * y) - Wy;
      if (in_real > 0.) {
         Wy = -Wy;
      }
   }
   else if (in_real < 0.) {
      Wy = -Wy;
   }

   return pycuda::complex<double>(Wx,Wy);
}

__device__ double conv_exp_single(double t, double G, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    if(t>sigma_t_threshold*sigma_t){
        double f = 2.*(sqrt(0.5*M_PI)); 
                   
        return f*exp(-G*t+0.5*G*G*sigma_t_sq);
    }
    else
        return sqrt(0.5*M_PI)*exp(-G*t+0.5*G*sigma_t_sq)*(1.+erf((t-G*sigma_t_sq)/(sigma_t*sqrt(2.)))); 
}

__device__ double conv_exp_cos_single(double t, double G, double Dm, double sigma_t) 
{   
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>sigma_t_threshold*sigma_t){        
        double f  = 2.*(sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*cos(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::real(faddeeva(z));
    }
}

__device__ double conv_exp_sin_single(double t, double G, double Dm, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>sigma_t_threshold*sigma_t){
        double f  = 2.*(sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*sin(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return -sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::imag(faddeeva(z));
    }
}

__device__ double conv_cosh_single(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp_single(t,G_m_hDG,sigma_t)+conv_exp_single(t,G_p_hDG,sigma_t));
}

__device__ double conv_sinh_single(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp_single(t,G_m_hDG,sigma_t)-conv_exp_single(t,G_p_hDG,sigma_t));
}


__device__ double Nk(double A_0_abs, 
                     double A_S_abs,
                     double A_pa_abs, 
                     double A_pe_abs,
                     double CSP,
                     int k)
{
    double nk;
    switch(k) {
        case 1:  nk = A_0_abs*A_0_abs;
                 break;
        case 2:  nk = A_pa_abs*A_pa_abs;
                 break;
        case 3:  nk = A_pe_abs*A_pe_abs;
                 break;
        case 4:  nk = A_pe_abs*A_pa_abs;
                 break;
        case 5:  nk = A_0_abs*A_pa_abs;
                 break;
        case 6:  nk = A_0_abs*A_pe_abs;
                 break;
        case 7:  nk = A_S_abs*A_S_abs;
                 break;
        case 8:  nk = CSP*A_S_abs*A_pa_abs;
                 break;
        case 9:  nk = CSP*A_S_abs*A_pe_abs;
                 break;
        case 10: nk = CSP*A_S_abs*A_0_abs;
                 break;
        default: printf("Wrong k index in nk, please check code %d\\n", k);
                 return 0.;
    }
    return nk;
}

__device__ double fk(double helcosthetaK, 
                     double helcosthetaL, 
                     double helphi,
                     int k)
{
    double helsinthetaK = sqrt(1. - helcosthetaK*helcosthetaK);
    double helsinthetaL = sqrt(1. - helcosthetaL*helcosthetaL);
//     helphi -= M_PI;
    double helsinphi = sin(-helphi);
    double helcosphi = cos(-helphi);
    
    double fk;
    switch(k) {
        case 1:  fk = helcosthetaK*helcosthetaK*helsinthetaL*helsinthetaL;
                 break;
        case 2:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helcosphi*helcosphi*helsinthetaL*helsinthetaL);
                 break;
        case 3:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helsinphi*helsinphi*helsinthetaL*helsinthetaL);
                 break;
        case 4:  fk = helsinthetaK*helsinthetaK*helsinthetaL*helsinthetaL*helsinphi*helcosphi;
                 break;
        case 5:  fk = sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helcosphi;
                 break;
        case 6:  fk = -sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helsinphi;
                 break;
        case 7:  fk = helsinthetaL*helsinthetaL/3.;
                 break;
        case 8:  fk = 2.*helsinthetaK*helsinthetaL*helcosthetaL*helcosphi/sqrt(6.);
                 break;
        case 9:  fk = -2.*helsinthetaK*helsinthetaL*helcosthetaL*helsinphi/sqrt(6.);
                 break;
        case 10: fk = 2.*helcosthetaK*helsinthetaL*helsinthetaL/sqrt(3.);
                 break;
        default: printf("Wrong k index in fk, please check code %d\\n", k);
                 return 0.;
    }
    return fk;
}

__device__ double ak(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double ak;
    switch(k) {
        case 1:  ak = 0.5*(1.+lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ak = 0.5*(1.+lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ak = 0.5*(1.+lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ak = 0.5*(sin(delta_pe-delta_pa) - lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ak = 0.5*(cos(delta_0-delta_pa) + lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ak = -0.5*(sin(delta_0-delta_pe) - lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ak = 0.5*(1.+lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ak = 0.5*(cos(delta_S-delta_pa) - lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ak = -0.5*(sin(delta_S-delta_pe) + lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ak = 0.5*(cos(delta_S-delta_0) - lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ak, please check code %d\\n", k);
                 return 0.;
    }
    return ak;
}

__device__ double bk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double bk;
    switch(k) {
        case 1:  bk = -lambda_0_abs*cos(phis_0);
                 break;
        case 2:  bk = -lambda_pa_abs*cos(phis_pa);
                 break;
        case 3:  bk = lambda_pe_abs*cos(phis_pe);
                 break;
        case 4:  bk = 0.5*(lambda_pe_abs*sin(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*sin(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  bk = -0.5*(lambda_0_abs*cos(delta_0-delta_pa-phis_0) + lambda_pa_abs*cos(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  bk = 0.5*(lambda_0_abs*sin(delta_0-delta_pe-phis_0) + lambda_pe_abs*sin(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  bk = lambda_S_abs*cos(phis_S);
                 break;
        case 8:  bk = 0.5*(lambda_S_abs*cos(delta_S-delta_pa-phis_S) - lambda_pa_abs*cos(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  bk = -0.5*(lambda_S_abs*sin(delta_S-delta_pe-phis_S) - lambda_pe_abs*sin(delta_pe-delta_S-phis_pe));
                 break;
        case 10: bk = 0.5*(lambda_S_abs*cos(delta_S-delta_0-phis_S) - lambda_0_abs*cos(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in bk, please check code %d\\n", k);
                 return 0.;
    }
    return bk;
}
    
__device__ double ck(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double ck;
    switch(k) {
        case 1:  ck = 0.5*(1.-lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ck = 0.5*(1.-lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ck = 0.5*(1.-lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ck = 0.5*(sin(delta_pe-delta_pa) + lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ck = 0.5*(cos(delta_0-delta_pa) - lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ck = -0.5*(sin(delta_0-delta_pe) + lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ck = 0.5*(1.-lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ck = 0.5*(cos(delta_S-delta_pa) + lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ck = -0.5*(sin(delta_S-delta_pe) - lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ck = 0.5*(cos(delta_S-delta_0) + lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ck, please check code %d\\n", k);
                 return 0.;
    }
    return ck;
}

__device__ double dk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double dk;
    switch(k) {
        case 1:  dk = lambda_0_abs*sin(phis_0);
                 break;
        case 2:  dk = lambda_pa_abs*sin(phis_pa);
                 break;
        case 3:  dk = -lambda_pe_abs*sin(phis_pe);
                 break;
        case 4:  dk = -0.5*(lambda_pe_abs*cos(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*cos(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  dk = -0.5*(lambda_0_abs*sin(delta_0-delta_pa-phis_0) + lambda_pa_abs*sin(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  dk = -0.5*(lambda_0_abs*cos(delta_0-delta_pe-phis_0) + lambda_pe_abs*cos(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  dk = -lambda_S_abs*sin(phis_S);
                 break;
        case 8:  dk = 0.5*(lambda_S_abs*sin(delta_S-delta_pa-phis_S) - lambda_pa_abs*sin(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  dk = -0.5*(-lambda_S_abs*cos(delta_S-delta_pe-phis_S) + lambda_pe_abs*cos(delta_pe-delta_S-phis_pe));
                 break;
        case 10: dk = 0.5*(lambda_S_abs*sin(delta_S-delta_0-phis_S) - lambda_0_abs*sin(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in dk, please check code %d\\n", k);
    
             return 0.;
    }
    return dk;
}

__device__ void integral4pitime(double integral[2], double vNk[10], double vak[10],double vbk[10],double vck[10],double vdk[10], 
                                double G, double DG, double DM, double delta_t)
{
  //double Nk, ak, bk, ck, dk;
  
  double low_time_acc;
  double t_1;
  double conv_sin_term_t_0, conv_cos_term_t_0, conv_sinh_term_t_0, conv_cosh_term_t_0;
  double conv_sin_term_t_1, conv_cos_term_t_1, conv_sinh_term_t_1, conv_cosh_term_t_1;
  double int_ta, int_tb, int_tc, int_td;
  //t_0 = t_ll;
  
  conv_sin_term_t_0  = conv_exp_sin_single(t_ll, G, DM, delta_t);
  conv_cos_term_t_0  = conv_exp_cos_single(t_ll, G, DM, delta_t);
  conv_sinh_term_t_0 = conv_sinh_single(t_ll, G, DG, delta_t);
  conv_cosh_term_t_0 = conv_cosh_single(t_ll, G, DG, delta_t);
  
  integral[0] = 0.;
  integral[1] = 0.;
  double den1 = -DG*DG + 4*G*G;
  double den2 = DM*DM + G*G;
  
  for(int i=0; i<time_acc_bins; i++)
  {
    t_1 = low_time_acc_bins_ul[i];
    
    if(t_1 < t_ll)
        continue;
        
    conv_sin_term_t_1  = conv_exp_sin_single(t_1, G, DM, delta_t);
    conv_cos_term_t_1  = conv_exp_cos_single(t_1, G, DM, delta_t);
    conv_sinh_term_t_1 = conv_sinh_single(t_1, G, DG, delta_t);
    conv_cosh_term_t_1 = conv_cosh_single(t_1, G, DG, delta_t);
    
    low_time_acc = low_time_acc_all[i];
        
    int_ta = 2*(DG*conv_sinh_term_t_0 + 2*G*conv_cosh_term_t_0 - DG*conv_sinh_term_t_1 - 2*G*conv_cosh_term_t_1)/den1;
    int_tb = 2*(DG*conv_cosh_term_t_0 + 2*G*conv_sinh_term_t_0 - DG*conv_cosh_term_t_1 - 2*G*conv_sinh_term_t_1)/den1;
    int_tc = (-DM*conv_sin_term_t_0 + G*conv_cos_term_t_0 + DM*conv_sin_term_t_1 - G*conv_cos_term_t_1)/den2;
    int_td = (DM*conv_cos_term_t_0 + G*conv_sin_term_t_0 - DM*conv_cos_term_t_1 - G*conv_sin_term_t_1)/den2;
    for(int k=0; k<10; k++)
    {
      
      integral[0] += low_time_acc*vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb + vck[k]*int_tc + vdk[k]*int_td);
      integral[1] += low_time_acc*vNk[k]*normweights[k]*(vak[k]*int_ta + vbk[k]*int_tb - vck[k]*int_tc - vdk[k]*int_td); 
      
    }
        
    conv_sin_term_t_0  = conv_sin_term_t_1;
    conv_cos_term_t_0  = conv_cos_term_t_1;
    conv_sinh_term_t_0 = conv_sinh_term_t_1;
    conv_cosh_term_t_0 = conv_cosh_term_t_1;
    }       
} 


__global__ void DiffRate(double *data, 
                         double *out, 
                         double CSP,
                         double A_0_abs,
                         double A_S_abs,
                         double A_pa_abs,
                         double A_pe_abs,
                         double phis_0,
                         double phis_S,
                         double phis_pa,
                         double phis_pe,
                         double delta_S,
//                          double delta_S_m_pe,
                         double delta_pa,
                         double delta_pe,
                         double l_0_abs,
                         double l_S_abs,
                         double l_pa_abs,
                         double l_pe_abs,
                         double G, 
                         double DG, 
                         double DM, 
                         int Nevt)
{
int row = threadIdx.x + blockDim.x * blockIdx.x; //ntuple entry

if (row >= Nevt) { 
    return;
}

int i0 = row*10;// general rule for cuda matrices : index = col + row*N; as it is now, N = 10 (cthk,cthl,cphi,t,sigma_t,q_OS,q_SSK,eta_OS,eta_SSK,sweights)
int idx = 0 + i0; 
int idy = 1 + i0;
int idz = 2 + i0;
int idt = 3 + i0;
int idsigma_t = 4 + i0;
int idq_OS = 5 + i0;
int idq_SSK = 6 + i0;
int ideta_OS = 7 + i0;
int ideta_SSK = 8 + i0;
//int sweights = 9 + i0;

double helcosthetaK = data[idx];
double helcosthetaL = data[idy];
double helphi = data[idz];
double t = data[idt];
double sigma_t = data[idsigma_t];
double q_OS = data[idq_OS];
double q_SSK = data[idq_SSK];
double eta_OS = data[ideta_OS];
double eta_SSK = data[ideta_SSK];

double delta_0 = 0.;

double pdf_B = 0.;
double pdf_Bbar = 0.;  

double delta_t =  delta_RunII(sigma_t);

double omega_OS = omega(eta_OS, 1);
double omega_bar_OS = omega_bar(eta_OS, 1);
double omega_SSK = omega(eta_SSK, 0);
double omega_bar_SSK = omega_bar(eta_SSK, 0);

double ta = conv_cosh_single(t, G, DG, delta_t);
double tb = conv_sinh_single(t, G, DG, delta_t);
double tc  = conv_exp_cos_single(t, G, DM, delta_t);
double td  = conv_exp_sin_single(t, G, DM, delta_t);

double vNk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vak[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vbk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vck[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
double vdk[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};

for(int k = 1; k <= 10; k++)
{
    double Nk_term = Nk(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,CSP,k);
    double fk_term = fk(helcosthetaK,helcosthetaL,helphi,k); 
   
    double ak_term = ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);    
    double bk_term = bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    double ck_term = ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    double dk_term = dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,l_0_abs,l_S_abs,l_pa_abs,l_pe_abs,k);
    
    double hk_B    = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb + ck_term*tc + dk_term*td);
    double hk_Bbar = 3./(4.*M_PI)*(ak_term*ta + bk_term*tb - ck_term*tc - dk_term*td);
    
    pdf_B += Nk_term*hk_B*fk_term;            
    pdf_Bbar += Nk_term*hk_Bbar*fk_term;

    vNk[k-1] = 1.*Nk_term;
    vak[k-1] = 1.*ak_term;
    vbk[k-1] = 1.*bk_term;
    vck[k-1] = 1.*ck_term;
    vdk[k-1] = 1.*dk_term;
}
    
double integral[2] = {0.,0.};
integral4pitime(integral, vNk,vak,vbk,vck,vdk, G, DG,DM, delta_t);

double int_B = integral[0];
double int_Bbar = integral[1];

double time_acc = get_time_acc(t);

double num = time_acc*((1.+ q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK)) *pdf_B 
                    + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*pdf_Bbar);


double den =  ((1.+q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))*int_B 
             + (1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))*int_Bbar);

out[row] = num/den;
}       


 
