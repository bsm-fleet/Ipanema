/***************************************************************************** 
 * Project: RooFit                                                           * 
 *                                                                           * 
 * This code was autogenerated by RooClassFactory                            * 
 *****************************************************************************/ 

// Your description goes here... 

#include "Riostream.h" 

#include "B.h" 
#include "RooAbsReal.h" 
#include "RooAbsCategory.h" 
#include <math.h> 
#include "TMath.h" 

ClassImp(B) 

 B::B(const char *name, const char *title, 
                        RooAbsReal& _helcosthetaK,
                        RooAbsReal& _helcosthetaL,
                        RooAbsReal& _helphi,
                        RooAbsReal& _t,
                        RooAbsReal& _A_pa_mod,
                        RooAbsReal& _A_pe_mod,
                        RooAbsReal& _A_0_mod,
                        RooAbsReal& _delta_pa,
                        RooAbsReal& _delta_pe,
                        RooAbsReal& _phi_pa,
                        RooAbsReal& _phi_0,
                        RooAbsReal& _G,
                        RooAbsReal& _DG,
                        RooAbsReal& _DM,
                        RooAbsReal& _1.0) :
   RooAbsPdf(name,title), 
   helcosthetaK("helcosthetaK","helcosthetaK",this,_helcosthetaK),
   helcosthetaL("helcosthetaL","helcosthetaL",this,_helcosthetaL),
   helphi("helphi","helphi",this,_helphi),
   t("t","t",this,_t),
   A_pa_mod("A_pa_mod","A_pa_mod",this,_A_pa_mod),
   A_pe_mod("A_pe_mod","A_pe_mod",this,_A_pe_mod),
   A_0_mod("A_0_mod","A_0_mod",this,_A_0_mod),
   delta_pa("delta_pa","delta_pa",this,_delta_pa),
   delta_pe("delta_pe","delta_pe",this,_delta_pe),
   phi_pa("phi_pa","phi_pa",this,_phi_pa),
   phi_0("phi_0","phi_0",this,_phi_0),
   G("G","G",this,_G),
   DG("DG","DG",this,_DG),
   DM("DM","DM",this,_DM),
   1.0("1.0","1.0",this,_1.0)
 { 
 } 


 B::B(const B& other, const char* name) :  
   RooAbsPdf(other,name), 
   helcosthetaK("helcosthetaK",this,other.helcosthetaK),
   helcosthetaL("helcosthetaL",this,other.helcosthetaL),
   helphi("helphi",this,other.helphi),
   t("t",this,other.t),
   A_pa_mod("A_pa_mod",this,other.A_pa_mod),
   A_pe_mod("A_pe_mod",this,other.A_pe_mod),
   A_0_mod("A_0_mod",this,other.A_0_mod),
   delta_pa("delta_pa",this,other.delta_pa),
   delta_pe("delta_pe",this,other.delta_pe),
   phi_pa("phi_pa",this,other.phi_pa),
   phi_0("phi_0",this,other.phi_0),
   G("G",this,other.G),
   DG("DG",this,other.DG),
   DM("DM",this,other.DM),
   1.0("1.0",this,other.1.0)
 { 
 } 



 Double_t B::evaluate() const 
 { 
   // ENTER EXPRESSION IN TERMS OF VARIABLE ARGUMENTS HERE 
   return (3.0L/4.0L)*sqrt(2)*helcosthetaK*helcosthetaL*exp_G_t*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(A_0_mod*A_pa_mod*cdmt*(cos(delta_pa) - cos(delta_pa + phi_0 - phi_pa)) + A_0_mod*A_pa_mod*cosh_term*(cos(delta_pa) + cos(delta_pa + phi_0 - phi_pa)) + A_0_mod*A_pa_mod*sdmt*(sin(delta_pa + phi_0) - sin(delta_pa - phi_pa)) - A_0_mod*A_pa_mod*sinh_term*(cos(delta_pa + phi_0) + cos(delta_pa - phi_pa)))*cos(helphi)/M_PI + (3.0L/4.0L)*sqrt(2)*helcosthetaK*helcosthetaL*exp_G_t*sqrt(-x2 + 1)*sqrt(-y2 + 1)*(A_0_mod*A_pe_mod*cdmt*(sin(delta_pe) + sin(delta_pe + phi_0 - phi_pe)) + A_0_mod*A_pe_mod*cosh_term*(sin(delta_pe) - sin(delta_pe + phi_0 - phi_pe)) - A_0_mod*A_pe_mod*sdmt*(cos(delta_pe + phi_0) + cos(delta_pe - phi_pe)) + A_0_mod*A_pe_mod*sinh_term*(-sin(delta_pe + phi_0) + sin(delta_pe - phi_pe)))*sin(helphi)/M_PI + (3.0L/4.0L)*exp_G_t*x2*(-y2 + 1)*(2*pow(A_0_mod, 2)*cosh_term - pow(A_0_mod, 2)*sdmt*(sin(phi_0)*cos(2*phi_0) - sin(phi_0) - sin(2*phi_0)*cos(phi_0)) - pow(A_0_mod, 2)*sinh_term*(sin(phi_0)*sin(2*phi_0) + cos(phi_0)*cos(2*phi_0) + cos(phi_0)))/M_PI + (3.0L/4.0L)*exp_G_t*(-x2 + 1)*(-y2 + 1)*(A_pa_mod*A_pe_mod*cdmt*(sin(delta_pa - delta_pe) + sin(delta_pa - delta_pe - phi_pa + phi_pe)) + A_pa_mod*A_pe_mod*cosh_term*(sin(delta_pa - delta_pe) - sin(delta_pa - delta_pe - phi_pa + phi_pe)) + A_pa_mod*A_pe_mod*sdmt*(cos(-delta_pa + delta_pe + phi_pa) + cos(delta_pa - delta_pe + phi_pe)) + A_pa_mod*A_pe_mod*sinh_term*(sin(-delta_pa + delta_pe + phi_pa) + sin(delta_pa - delta_pe + phi_pe)))*sin(helphi)*cos(helphi)/M_PI + (3.0L/8.0L)*exp_G_t*(-x2 + 1)*(y2 + (-y2 + 1)*pow(sin(helphi), 2))*(2*pow(A_pa_mod, 2)*cosh_term - pow(A_pa_mod, 2)*sdmt*(sin(phi_pa)*cos(2*phi_pa) - sin(phi_pa) - sin(2*phi_pa)*cos(phi_pa)) - pow(A_pa_mod, 2)*sinh_term*(sin(phi_pa)*sin(2*phi_pa) + cos(phi_pa)*cos(2*phi_pa) + cos(phi_pa)))/M_PI + (3.0L/8.0L)*exp_G_t*(-x2 + 1)*(-(-y2 + 1)*pow(sin(helphi), 2) + 1)*(2*pow(A_pe_mod, 2)*cosh_term - 2*pow(A_pe_mod, 2)*sdmt*sin(phi_pe) + 2*pow(A_pe_mod, 2)*sinh_term*cos(phi_pe))/M_PI ; 
 } 



 Int_t B::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const  
 { 
   // LIST HERE OVER WHICH VARIABLES ANALYTICAL INTEGRATION IS SUPPORTED, 
   // ASSIGN A NUMERIC CODE FOR EACH SUPPORTED (SET OF) PARAMETERS 
   // THE EXAMPLE BELOW ASSIGNS CODE 1 TO INTEGRATION OVER VARIABLE X
   // YOU CAN ALSO IMPLEMENT MORE THAN ONE ANALYTICAL INTEGRAL BY REPEATING THE matchArgs 
   // EXPRESSION MULTIPLE TIMES

   // if (matchArgs(allVars,analVars,x)) return 1 ; 
   return 0 ; 
 } 



 Double_t B::analyticalIntegral(Int_t code, const char* rangeName) const  
 { 
   // RETURN ANALYTICAL INTEGRAL DEFINED BY RETURN CODE ASSIGNED BY getAnalyticalIntegral
   // THE MEMBER FUNCTION x.min(rangeName) AND x.max(rangeName) WILL RETURN THE INTEGRATION
   // BOUNDARIES FOR EACH OBSERVABLE x

   // assert(code==1) ; 
   // return (x.max(rangeName)-x.min(rangeName)) ; 
   return 0 ; 
 } 



