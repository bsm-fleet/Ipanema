/*****************************************************************************
 * Project: RooFit                                                           *
 *                                                                           *
  * This code was autogenerated by RooClassFactory                            * 
 *****************************************************************************/

#ifndef B
#define B

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooCategoryProxy.h"
#include "RooAbsReal.h"
#include "RooAbsCategory.h"
 
class B : public RooAbsPdf {
public:
  B() {} ; 
  B(const char *name, const char *title,
	      RooAbsReal& _helcosthetaK,
	      RooAbsReal& _helcosthetaL,
	      RooAbsReal& _helphi,
	      RooAbsReal& _t,
	      RooAbsReal& _A_pa_mod,
	      RooAbsReal& _A_pe_mod,
	      RooAbsReal& _A_0_mod,
	      RooAbsReal& _delta_pa,
	      RooAbsReal& _delta_pe,
	      RooAbsReal& _phi_pa,
	      RooAbsReal& _phi_0,
	      RooAbsReal& _G,
	      RooAbsReal& _DG,
	      RooAbsReal& _DM,
	      RooAbsReal& _1.0);
  B(const B& other, const char* name=0) ;
  virtual TObject* clone(const char* newname) const { return new B(*this,newname); }
  inline virtual ~B() { }

  Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

protected:

  RooRealProxy helcosthetaK ;
  RooRealProxy helcosthetaL ;
  RooRealProxy helphi ;
  RooRealProxy t ;
  RooRealProxy A_pa_mod ;
  RooRealProxy A_pe_mod ;
  RooRealProxy A_0_mod ;
  RooRealProxy delta_pa ;
  RooRealProxy delta_pe ;
  RooRealProxy phi_pa ;
  RooRealProxy phi_0 ;
  RooRealProxy G ;
  RooRealProxy DG ;
  RooRealProxy DM ;
  RooRealProxy 1.0 ;
  
  Double_t evaluate() const ;

private:

  ClassDef(B,1) // Your description goes here...
};
 
#endif
