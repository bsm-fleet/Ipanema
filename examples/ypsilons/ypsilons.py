# -*- coding: utf-8 -*-
'''
Fit of A1 -> mu+ mu- in bins of the transverse momentum and pseudorapidity

@author Diego Martínez Santos
@author Miguel Ramos Pernas
@date   2017-03-17
'''
from tools import initialize
initialize()

# Cuda imports
import pycuda.cumath
import pycuda.driver as cudriver
import pycuda.gpuarray as gpuarray

# Usual imports
from math import sqrt
import numpy as np
import cPickle

# Custom imports
from timeit import default_timer as timer
from tools import plt, poisson_FCinterval
from ModelBricks import Parameter, Free, Cat
from model import YpsilonsFitModel

# Number of events
nevts = int(1e10)

# Year and data type
sample = 'Data'
year   = 2011

# Data files
rampup_params = cPickle.load(file('rampup_params.cpickle'))

# Number of bins in transverse momentum and pseudo-rapidity
ptbins  = range(2)
etabins = range(2)

# Parameters of the fit to the A1
Params = [
  Free('lm', 0.47, limits = (0., 0.9)),
  Free('lb', -2.0, limits = (-4, -0.1)),
  Free('a1', 1.26, limits = (0.5, 5.)),
  Free('n1', 1.28, limits = (0.5, 5.)),
  Parameter('n2', 1.),
  Parameter('a2', 200.),
  Free('beta', -0.001, limits = (-1.e-02, 0.)),
  Free('scale', 1.e-04, limits = (0, 0.005)),
  Parameter('A1_m', 9460.30),
  ]

# Loop over bins to generate the categories for the fit
cats, nevts_s, results, rampups = [], {}, {}, {}
for ptbin in ptbins:
  for etabin in etabins:

    ''' Get data '''
    #idata = data[('{}{}'.format(sample, year), ptbin, etabin)]

    ''' Create category '''
    cat_name = 'pt' + str(ptbin) + '_eta' + str(etabin) + '_' + str(year)

    #thiscat  = Cat(cat_name, idata[1], getN = True)

    thiscat = Cat(cat_name)
    cats.append(thiscat)
    cname = thiscat.name
    nevts_s['pt' + str(ptbin) +'_eta'+str(etabin) + '_' + str(year)] = nevts

    ''' Add parameters of this bin '''
    bin_pars = [
      Free(cname + '_sf',1., limits = (0.1,3.0)),
      Free(cname + '_N1',.5*nevts, limits = (.05*nevts,1.1*nevts)),
      Free(cname + '_N2',.2*nevts, limits = (.01*nevts,.6*nevts)),
      Free(cname + '_N3',.1*nevts, limits = (.01*nevts,.4*nevts)),
      Parameter(cname + '_Ns',0.),
      Free(cname + '_Nb',.5*nevts, limits = (.0*nevts,1.1*nevts)),
      Free(cname + '_k1',-1.e-04, limits = (-1e-03, -1e-06)),
      Free(cname + '_k10',0.5, limits = (0.1, 1.0)),
      Free(cname + '_k11',-0.1, limits = (-1.0, -0.01)),
      Parameter(cname + '_k12',0.0),
      ]
    Params += bin_pars
    
    rampups[cname] = rampup_params[('Data{}'.format(year), ptbin, etabin)]

# Binning scheme to do the integral
binning = gpuarray.to_gpu(np.float64(np.linspace(5.5e3, 15e3, 512*32)))

# Create model and data
manager = YpsilonsFitModel(cats, Params, binning, rampups)
genPars = {}
for p in Params:
  genPars[p.name] = p.val
manager.generateToy(genPars)

# Do the fit
start = timer()
manager.createFit()
manager.fit.migrad()
manager.fit.hesse()
print timer() - start

# Make one plot for each category
for cat in cats:
  manager.plot(cat, rebinning=256, outfile='{}{}.pdf'.format('Example3_', cat.name), interval=poisson_FCinterval)
plt.show()
