# Cuda imports
from pycuda.compiler import SourceModule
import pycuda.cumath
import pycuda.driver as cudriver
import pycuda.gpuarray as gpuarray

# Custom imports
from params import means as mPDG, P as Ypars
from CLcalculator_beta1 import catTS
from ModelBricks import Parameter, ParamBox, cuRead
from tools import GaussSmear1, plt, plot1D
from toygen import poissonLL_mask

# Usual imports
#from bisect import *
import cPickle
from iminuit import Minuit
import numpy as np
from timeit import default_timer as timere
from toygen import generateBinned
import os
# Get Ipatia function from source
mod = cuRead(os.environ["IPAPATH"] + "/cuda/psIpatia.cu")
ipa = mod.get_function("Ipatia")

class YpsilonsFitModel(ParamBox):
    '''
    Main model
    '''
    def __init__(self, cats, pars, bins_gpu, rampups):
        '''
        Constructor

        param cats: List of categories, one for each pt and eta bin
        param pars: List of parameters
        param bins_gpu: GPU bins to do the normalization
        param rampups: Information
        '''
        ParamBox.__init__(self, pars, cats)
        if isinstance(bins_gpu, gpuarray.GPUArray ): self.bins = bins_gpu
        elif isinstance(bins_gpu, np.ndarray): self.bins = gpuarray.to_gpu(bins_gpu)
        else : self.bins = gpuarray.to_gpu(np.float64(bins_gpu) )
        
        self.bins_cx = self.bins.astype(np.complex128) 
        self.func = {}
        self.SmearFunc = {}
        self.Ypars = Ypars
        self.func["A1"] = self.bins.copy()
        for key in self.Ypars.keys(): self.func[key] = self.bins.copy()
         
        self.setRampup(rampups)
        self.Nbins = self.bins.size
        self.min = float(pycuda.gpuarray.min(self.bins).get())
        self.max = float(pycuda.gpuarray.max(self.bins).get())
        self.dx = (self.max - self.min )*1./self.Nbins
        self.setDummyMask()
        
    def setDummyMask(self):
        '''
        Create a mask so all the bins are used
        '''
        mass = self.bins.get()
        ones = np.ones(len(mass))
        self.mask = gpuarray.to_gpu(ones)
        
    def setRampup(self, rampupVals):
        '''
        Define the smearing function for each signal peak (Upsilon_i + A1) given the values of
        slope and origin for each category.

        param rampupVals: Dictionary with the slope and origin for each category
        '''
        self.rampup = rampupVals
        for cat in self.cats:
            m, b = self.rampup[cat.name]
            self.SmearFunc[cat.name] = {}
            for i in range( 1, 4 ):
                key = "Y" + str(i)
                self.SmearFunc[cat.name][key] = GaussSmear1(self.bins_cx, m*mPDG[key] + b)
            self.SmearFunc[cat.name]['A1'] = GaussSmear1(self.bins_cx, m*self.Params["A1_m"].fit_init + b)

    def setAmass(self,mu):
        '''
        Set a new mass hypothesis for the A1. The resolution changes according to it.

        param mu: Mass hypothesis
        '''
        self.Params["A1_m"].setVal(mu)
        self.Params["A1_m"].constant = True
        mass = self.bins.get()
        ones = np.ones(len(mass))
        Mmin = mu - 100.
        Mmax = mu + 100.
        mask = np.concatenate((ones[mass<Mmin], ones[(mass>=Mmin) & (mass<Mmax)]*0, ones[mass>=Mmax]))
        self.mask = gpuarray.to_gpu(mask)
        for cat in self.cats:
            m, b= self.rampup[cat.name]
            self.SmearFunc[cat.name]['A1'] = GaussSmear1(self.bins_cx, m*mu + b)
            

    def peak(self, catname, name, mu, sigma, l , beta, a, n, aa, nn):
        '''
        Create a new peak given the Ipatia parameters. The output pdf corresponds to the convolution
        of the Ipatia with the smearing function.

        param catname: Name of the category
        param name: Name of the particle to be fitted
        param [mu, sigma, l, beta, a, n, aa, nn]: Ipatia parameters
        return: Convoluted signal peak pdf
        '''
        ipa(self.bins, self.func[name], np.float64(mu),np.float64(sigma),np.float64(l), np.float64(beta), np.float64(a), np.float64(n), np.float64(aa), np.float64(nn), block = (512,1,1), grid = (self.Nbins/512,1))
        return self.SmearFunc[catname][name].Smear(self.func[name])
    
   
    def Pdf(self, catname, A1_m, shared_pars, bkg_pars, fit_yields):
        '''
        Cooks the expectation arrays given the models

        param catname: Category name
        param A1_m: A1 mass hypothesis
        param shared_pars: Values of the parameters shared by the Upsilon resonances
        param bkg_pars: Parameters of the combinatorial background
        param fit_yields: Yields for the fit
        '''
        scale, sf, lm, lb, beta, a, n, aa, nn = shared_pars

        double_conv = lambda x: np.float64(x)
        
        k1, k10, k11, k12 = map(double_conv, bkg_pars)
        
        ns, nb, n1, n2, n3 = map(double_conv, fit_yields)

        l = lm*sf + lb

        ''' Extract Upsilon mass and resolution '''
        smear_func = self.SmearFunc[catname]
        upsilons   = ['Y1', 'Y2', 'Y3']
        
        Y1_sig, Y2_sig, Y3_sig, A1_sig = map(lambda y: sf*smear_func[y].sigma, upsilons + ['A1'])
        
        Y1_mu, Y2_mu, Y3_mu = map(lambda y: mPDG[y]*(1 + scale), upsilons)

        A1_mu = A1_m*(1 + scale)

        ''' Not normalized '''
        Y1 = self.peak(catname, 'Y1', Y1_mu, Y1_sig, l, beta, a, n, aa, nn)
        Y2 = self.peak(catname, 'Y2', Y2_mu, Y2_sig, l, beta, a, n, aa, nn)
        Y3 = self.peak(catname, 'Y3', Y3_mu, Y3_sig, l, beta, a, n, aa, nn)

        ''' Normalized A1 '''
        A1 = self.peak(catname, "A1", A1_mu, A1_sig, l, beta, a, n, aa, nn) 

        ''' Normalized background '''
        bkg = self.bkg(k1, k10, k11, k12)
        
        ''' Contributions '''
        A1_cont  = ns*A1/np.sum(A1.get())
        bkg_cont = nb*(bkg/np.sum(bkg.get()))
        Y1_cont  = n1*Y1/np.sum(Y1.get())
        Y2_cont  = n2*Y2/np.sum(Y2.get())
        Y3_cont  = n3*Y3/np.sum(Y3.get())
        
        return bkg_cont + Y1_cont + Y2_cont + Y3_cont + A1_cont


    def bkg(self, k1, k10, k11, k12):
        '''
        Background pdf given its parameters
        '''
        expf1 = pycuda.cumath.exp(k1*self.bins)        
        mpone = (self.bins - self.min)/(self.max - self.min)*2. - 1
        pol = k10 + k11*mpone + k12*0.5*(3*mpone*mpone - 1.) #3rd order Legendre polynomial
        return pol*expf1
    
       
    def catFCN(self, cat, A1_m, shared_pars, bkg_pars, fit_yields):
        '''
        Definition of the FCN for each category

        See self.Pdf
        '''
        pdf  = self.Pdf(cat.name, A1_m, shared_pars, bkg_pars, fit_yields)
        LL   = poissonLL_mask(cat.data, pdf, self.mask)
        chi2 = -2*LL
        return chi2

   
    def __call__(self, *args):
        '''
        Function to be used as the FCN used by Minuit to do the minimization

        param args: List of input arguments
        '''
        chi2 = np.float64(0.)
        N = self.dc
        beta = np.float64(args[N["beta"]])
        landam = np.float64(args[N["lm"]])
        landab = np.float64(args[N["lb"]])
        scale = np.float64(args[N["scale"]])
        
        a1 = np.float64(args[N["a1"]])
        n1 = np.float64(args[N["n1"]])
        a2 = np.float64(args[N["a2"]])
        n2 = np.float64(args[N["n2"]])
        A1_m = np.float64(args[N["A1_m"]])
        
        for cat in self.cats:
            k = cat.name
            N1 = np.float64(args[N[ k + "_N1"]])
            N2 = np.float64(args[N[ k + "_N2"]])
            N3 = np.float64(args[N[ k + "_N3"]])
            Nb = np.float64(args[N[ k + "_Nb"]])
            Ns = np.float64(args[N[ k + "_Ns"]])

            k1 = np.float64(args[N[ k + "_k1"]])
            k10 = np.float64(args[N[ k + "_k10"]])
            k11 = np.float64(args[N[ k + "_k11"]])
            k12 = np.float64(args[N[ k + "_k12"]])
            sf = np.float64(args[N[k + "_sf"]])
            
            shared_pars = scale, sf, landam, landab, beta, a1, n1, a2, n2
            bkg_pars    = k1, k10, k11, k12
            fit_yields  = Ns, Nb, N1, N2, N3

            chi2 += self.catFCN(cat, A1_m, shared_pars, bkg_pars, fit_yields)
        
        return chi2

    def catPdf(self, cat, values):
        '''
        Build the pdf for the given category

        param cat: Category
        param values: Dictionary with the values for the floating parameters in the pdf
        return: Whole pdf for the given parameters
        '''
        beta = np.float64(values["beta"])
        landam = np.float64(values["lm"])
        landab = np.float64(values["lb"])
        scale = np.float64(values["scale"])
        
        a1 = np.float64(values["a1"])
        n1 = np.float64(values["n1"])
        a2 = np.float64(values["a2"])
        n2 = np.float64(values["n2"])
        A1_m = np.float64(values["A1_m"])
        
        k = cat.name
        N1 = np.float64(values[ k + "_N1"])
        N2 = np.float64(values[ k + "_N2"])
        N3 = np.float64(values[ k + "_N3"])
        Nb = np.float64(values[ k + "_Nb"])
        Ns = np.float64(values[ k + "_Ns"])

        k1 = np.float64(values[ k + "_k1"])
        k10 = np.float64(values[ k + "_k10"])
        k11 = np.float64(values[ k + "_k11"])
        k12 = np.float64(values[ k + "_k12"])
        sf = np.float64(values[ k + "_sf" ])

        shared_pars = scale, sf, landam, landab, beta, a1, n1, a2, n2
        bkg_pars    = k1, k10, k11, k12
        fit_yields  = Ns, Nb, N1, N2, N3

        return self.Pdf(k, A1_m, shared_pars, bkg_pars, fit_yields)
    
    def getBestFitPdf(self, cat):
        '''
        Return the pdf for the given category evaluated using the fit values

        param cat: Category
        return: Pdf for the input category evaluated using the fit values
        '''
        return self.catPdf(cat, self.fit.values)

    def plot(self, cat, outfile='test_fit.pdf', mask = True, plotf=plot1D, **kwargs):
        '''
        Plot the given category and save it on an output file

        param cat: Category
        param outfile: Output file name
        param mask: Apply the mask
        param plotf: Plot function
        param kwargs: Dictionary passed to the function defined as "plotf"
        '''
        mass_center = self.bins.get()
        if mask:
            data = (cat.data*self.mask).get()
            pdf = (self.getBestFitPdf(cat)*self.mask).get()
        else:
            data = cat.data.get()
            pdf = self.getBestFitPdf(cat).get()
       
        fig , pltfit, pltpulls = plotf (data, pdf, mass_center, **kwargs)
        pltfit.set_ylabel('Candidates')
        pltpulls.set_ylabel('Significance')
        pltpulls.set_xlabel('$M_{\mu^+\mu^-}$ ( MeV/c$^2 )$')
        fig.tight_layout()
        fig.savefig(outfile)


    def loadFitSummary(self, name):
        '''
        Load the fit result from a file
        
        param name: File name
        '''
        self.summary = cPickle.load(file(name))
    
        
    def generateToy(self, params):
        '''
        Generate data given the current pdf of the model

        param params: Parameters of the pdf
        '''
        template = gpuarray.to_gpu(np.uint32(self.Nbins*[0]))
        for cat in self.cats:
            cat_data = template.copy()
            generateBinned(cat_data, self.catPdf(cat, params))
            cat.setData(cat_data, getN = True)
    
    def CLs_pe(self, Ns, signal_fractions, toys = 1e04, Npmax = 2e06, reboot = 0):
        '''
        Generate toys to store the information to calculate the CLs

        param Ns: Number of observed signal events
        param signal_fractions: List of fractions (fr) so the number of generated events is fr*Ns
        param toys: Number of toys
        param Npmax: Number of points
        param reboot: Attach a new set of TS values (to prevent from running out of space)
        '''
        start = timer()
        if toys > Npmax:
            print "Not fully implemented yet for such large number of toys bcs of GPU memory "\
                "usage. As a workaround, you can call CLs_pe consecutively to accumulate toys"
        self.summary.generate(min(toys,Npmax))
        if not "tB" in dir(self) or not "tS" in dir(self) or reboot:
            self.tB, self.tS = [], []
        else:
            print "Detected previous pe's (aka toys). Will merge the current set with them. "\
                "If you don't want this, rerun CLs_pe with reboot = 1"
        null = {}
        test = {}
        point = self.summary.values.copy()
        datasets = {}
        null_toy = {}
        test_toy = {}
        toy_template =  gpuarray.to_gpu(np.uint32(self.Nbins*[0]))
        for cat in self.cats:
            name = cat.name
            point[name + "_Ns"] = Ns*signal_fractions[name]
            test[name] = self.catPdf(cat, point)
            point[name + "_Ns"] = 0.
            null[name] = self.catPdf(cat, point)
            datasets[name] = cat.data
            null_toy[name] = toy_template.copy()
            test_toy[name] = toy_template.copy()
            
        self.ts0 = catTS(datasets, null, test)
        print "Preliminaries:", timer() - start
        start = timer()

        for i in xrange(int(toys)):
            point.update( self.summary.pickGenerated(i))
            for cat in self.cats:
                name = cat.name
                point[name + "_Ns"] = Ns*signal_fractions[name]
                generateBinned(test_toy[name], self.catPdf(cat, point) )
                point[name + "_Ns"] = 0.
                generateBinned(null_toy[name], self.catPdf(cat, point))
            self.tB.append(catTS(null_toy, null, test))
            self.tS.append(catTS(test_toy, null, test))
        print "Toy loop and TS:", timer() - start
        self.tB.sort(), self.tS.sort()

    def CLs( self ):
        '''
        Calculate CLs given the background, signal and observation hypotheses

        return: CLs
        '''
        return getCLs(self.ts0, self.tS, self.tB)
