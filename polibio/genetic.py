import numpy as np
import os
#from tools import initialize
#initialize(1)
import pycuda.gpuarray as gpuarray
#import toyrand
from pycuda.compiler import SourceModule
X = SourceModule(file(os.environ["IPAPATH"] + "/cuda/genetic.c","r").read(), no_extern_c = True)

cx_mutate = X.get_function("cx_mutate")
cx_select = X.get_function("cx_select")
re_mutate = X.get_function("re_mutate")
re_select = X.get_function("re_select")

#rand = toyrand.XORWOWRandomNumberGenerator()
