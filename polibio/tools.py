from reikna import cluda
from reikna.fft import FFT, FFTShift
import atexit
import pycuda.cumath
import pycuda.driver as cuda
import pycuda.gpuarray as gpuarray
from pycuda.tools import clear_context_caches
import numpy as np
import matplotlib.pyplot as plt
from poisson_intervals import *

# Global variable for the cuda context and device
__context__ = None
__device__  = None
__thread__  = None


def initialize( idev = None, interactive = True ):
    '''
    Initialize CUDA. This function substitutes "make_default_context"
    function from PyCuda. It allows to correctly select the device to
    work. This function is meant to be called just once at the
    beginning of a script. Any other call will be ignored.

    :param idev: device to work with. If "None", and "interactive = False" \
    use the first found.
    :type idev: int
    :param interactive: determine whether to ask for a device or \
    to select the first device found
    :returns: CUDA context made from the device
    :rtype: pycuda.driver.Context
    '''
    global __context__, __device__, __thread__

    if __context__ is not None:
        # "initialize" has already been called
        return
    
    cuda.init()
    
    ndev = cuda.Device.count()
    
    if ndev == 0:
        raise LookupError('No devices have been found')
    
    # Default device if anything fails
    defdev = 0
    
    if idev != None:
        # Use the specified device
        if idev > ndev:
            print 'WARNING: Specified a device number ({}) greater '\
                'than the maximum number of devices ({}); '\
                'set to {}'.format(idev, ndev, defdev)
            idev = defdev
            
    elif not interactive:
        # Get the default device
        idev = defdev
        
    else:
        # Ask the user to select a device
        print 'Found {} available devices:'.format(ndev)
        for i in xrange(ndev):
            print '- {} [{}]'.format(cuda.Device(i).name(), i)
            
        idev = -1
        while int(idev) not in xrange(ndev):
            idev = raw_input('Select a device (default {}): '.format(defdev))
            if idev.strip() == '':
                # Set to default value
                idev = defdev
    
    device = cuda.Device(int(idev))
    print '-- Using device "{}" [{}]'.format(device.name(), idev)
    api = cluda.cuda_api()    
    
    __device__  = device
    __context__ = device.make_context()
    __thread__  = api.Thread(__context__)

    atexit.register(_finish_up)


def _finish_up():
    '''
    Instructions to finalize the CUDA context. This function has
    been copied from pycuda.autoinit.
    '''
    global __context__
    __context__.pop()
    __context__ = None
    
    clear_context_caches()


class ConvolverFFT:
    def __init__(self, x):
        self.tmp = x.copy()
        self.out = x.copy()
        
        self.FFT =  FFT(x)
        self.FFTShift = FFTShift(x)
        self.fft = self.FFT.compile(__thread__)
        self.fftshift = self.FFTShift.compile(__thread__)
        
    def Convolve(self, a,b):
        Fa = a.copy()
        Fb = b.copy()
        self.fft(Fa,a)
        self.fft(Fb, b)
        
        self.fft(self.tmp, Fa*Fb, inverse = True)
        self.fftshift(self.out,self.tmp)
        return self.out.copy()
    

class SmearerFFT(ConvolverFFT):
    def __init__(self, smf):
        ConvolverFFT.__init__(self, smf)
        self.smf = smf
        self.Fb = self.tmp.copy()
        self.fft(self.Fb,self.tmp)
        
    def Smear(self, a):
        cineroso = a.astype(np.complex128)
        Fa = cineroso.copy()
        self.fft(Fa,cineroso)
        
        self.fft(self.tmp, Fa*self.Fb, inverse = True)
        self.fftshift(self.out,self.tmp)
        return self.out.real.copy()
    
        
class GaussSmear1(SmearerFFT):
    def __init__(self, bins_gpu, sigma):
        M = gpuarray.take(bins_gpu,gpuarray.to_gpu(np.int32([int(bins_gpu.size*0.5)]))).get()[0]
        d = bins_gpu - M
        a = np.float64(0.5/(sigma*sigma))
        self.sigma = sigma
        f = pycuda.cumath.exp(-a*d*d)
        SmearerFFT.__init__(self, f/np.sum(f.get()))


def getSumLL_large(cat): return gpuarray.sum(pycuda.cumath.log(getattr(cat,"Probs")))
def getSumLL_short(cat): return np.sum(pycuda.cumath.log(getattr(cat,"Probs")).get())
def getSumLL_w_large(cat): return gpuarray.sum(pycuda.cumath.log(getattr(cat,"Probs")) * cat.weights )
def getSumLL_w_short(cat): return np.sum(( pycuda.cumath.log(getattr(cat,"Probs")) * cat.weights ).get())

def get_pulls(data,pdf, interval):
    errl = []
    errh = []
    for k in data:
        l, h = interval(k)
        errl.append(l)
        errh.append(h)
    errl = np.array(errl)
    errh = np.array(errh)
    deltas = data-pdf
    return np.array([(deltas[i]/errl[i] if deltas[i]>0  else deltas[i]/errh[i]) for i in range(np.size(deltas))]), errl, errh

def plot1D(data, pdf, bins, rebinning = 32,yscale = '', interval = poisson_Linterval):
    assert np.size(bins)%rebinning==0
    bins_rebin = np.sum(bins[i::rebinning] for i in range(rebinning))/rebinning #averaging
    pdf_rebin = np.sum(pdf[i::rebinning] for i in range(rebinning)) #summing
    data_rebin = np.sum(data[i::rebinning] for i in range(rebinning)) #summing
    pulls,errl, errh = get_pulls(data_rebin, pdf_rebin, interval)

     ## Now plot the result
    fig, (pltfit, pltpulls) = plt.subplots(2,1,figsize=(10, 7), sharex=True, gridspec_kw = {'height_ratios':[5, 1]})

    pltfit.errorbar(bins_rebin, data_rebin, yerr=[errl, errh], fmt='.', color='k') 
    pltfit.plot(bins, pdf*rebinning, '-', color='blue', linewidth=1)

    if yscale=='log': pltfit.set_yscale('log')
    pltpulls.errorbar(bins_rebin, pulls, yerr=[np.ones(len(errl)), np.ones(len(errh))], fmt='.', color='k') 
    pltpulls.plot(bins_rebin, pulls, marker='.', linestyle='-', color='k')

    pltpulls.set_ylim(-5, 5)
    pltpulls.set_yticks([-5, -3, 0, +3, +5])
    pltpulls.yaxis.grid(True, linestyle='--', which='major', color='grey')
    pltpulls.set_xlim(bins[0], bins[-1])
    #pltpulls.set_xlabel('$m(\mu\mu)$ MeV/$c^2$', fontsize=20)

    return fig, pltfit, pltpulls

def contour(X,Y,Z, levels=[1.15,2.3,5.99],colors=('#AC004C', '#4CAC00', '#004CAC'),labels =  ["68% CL", "95% CL", "99.9% CL"], subst = True,filename = ""):
    fig = plt.figure(figsize=(7,5))

    minimum = np.min(Z)
    if subst: Zp = Z - minimum
    else: Zp = Z
    cs =plt.contour(X,Y,Zp,levels,colors)

    
    cs.collections[0].set_dashes([(0, (2.0, 2.0))])
    for i in range(len(labels)):
        cs.collections[i].set_label(labels[i])
    plt.show()
    if filename: plt.savefig("filename")
    return cs
